<?php


namespace App\Http\Controllers\Utilizador;
use App\Http\Controllers\Controller;
use App\Dish;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sucesso=0;
       
        if (Auth::user())
         return view('UI_Cliente.areaCliente')
            ->with(compact('sucesso'));

   else return redirect('/login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        if (Auth::user())
        return view('UI_Cliente.areaCliente');
       else return redirect('/login');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user())
        return view('UI_Cliente.areaCliente');
       else return redirect('/login');
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $user->name=$request->get('name');
        $user->phone=$request->get('phone');
        $user->save(); 
        $sucesso=1;
        
        return view('UI_Cliente.areaCliente')
        ->with(compact('user','sucesso'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect('/users');
    }

    public function AlterarPwd (Request $request)
    {
        $user = User::find(Auth::user()->id);
       
                $current_password = $user->password;  
                $newpw = $request['newpwd'];
                $confirmnew=$request['confirmpwd'];
                
                if(Hash::check($request['oldpwd'], $current_password)) {   
                                
                    if($newpw == $confirmnew) { 
        
                        $user->password = Hash::make($newpw);
                        $user->save(); 
                        $sucesso=1;
                        
                    }else{
                        $user_id = Auth::User()->id;                       
                        $user = User::find($user_id);
                        // PASSWORD ANTIGA ERRADA!
                        $sucesso = -1;
                        
                            return view('UI_Cliente.areaCliente')
                            ->with(compact('user','sucesso'));
                    }
                }
                $sucesso=1;
                 
                    return view('UI_Cliente.areaCliente')
                    ->with(compact('user','sucesso'));
    }
}
