<?php

namespace App\Http\Controllers\Utilizador;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Reserve;
use Auth;
class ReserveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()){
            $sucesso=0;
            return view('UI_Cliente.reservas',compact('sucesso'));
        }
        

   else return redirect('/login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            
         'date' => 'required',
         'hour' => 'required',
         'nr_persons' => 'required',
        ]);

        $reserve = new Reserve();
        $reserve->user_id =  Auth::user()->id;
        $reserve->nr_persons = $request->get('nr_persons');
        $reserve->status = 0;
        $reserve->datehour = $request->get('date') . " " . $request->get('hour');
        $reserve->save();
        $sucesso=1;

        return view('UI_Cliente.reservas',compact('sucesso'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reserve= Reserve::find($id);
        return $reserve;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reserve= Reserve::find($id);
        return $reserve;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reserve= Reserve::find($id);
        $reserve->nr_persons = $request->get('nr_persons');
        $reserve->datehour = $request->get('datehour');
        $reserve->status = 0;
        $reserve->save();

        return $reserve;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       //Obter reserva
       $reserve = Reserve::find($id);
       //Eliminar reserva
       $reserve->delete();
       $reserves = Reserve::where('user_id','=',Auth::user()->id)->get();
       $sucesso=1;
       //Retorno
       return view('UI_Cliente.verreserva',compact('reserves','sucesso'));
    }

    public function reservesbyuser(){

        $reserves = Reserve::where('user_id','=',Auth::user()->id)->get();
        $sucesso=0;
        return view('UI_Cliente.verreserva',compact('reserves','sucesso'));
    }
}
