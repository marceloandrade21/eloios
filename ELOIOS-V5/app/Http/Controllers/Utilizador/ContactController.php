<?php
namespace App\Http\Controllers\Utilizador;
use App\Http\Controllers\Controller;

use Auth;
use App\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sucesso = 0 ;
    
        return view('UI_Cliente.contacts',compact('sucesso'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            
         'name' => 'required|string|max:255',
         'email' => 'required|string|email|max:255|',
         'phone' => 'required|string',
         'message' => 'required|string|max:255'
     ]);


       $message = new Contact();
       $message->name= $request->get('name');
       $message->email= $request->get('email');
        $message->phone=$request->get('phone');
       $message->message= $request->get('message');
     
       $message->save();
      $sucesso=1;
   
      return view('UI_Cliente.contacts',compact('sucesso')); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        //
    }
}
