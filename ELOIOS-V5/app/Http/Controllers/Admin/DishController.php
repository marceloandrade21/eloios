<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Dish;
use Illuminate\Http\Request;
use App\Product;
use App\Type_Dish;

class DishController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Obter todos os pratos
        $dishes = Dish::all();
        //Retorno
        return $dishes;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Criar Produto
        $product = new Product();
        $product->name = $request->get('name');
        $product->menu = 1;
        $product->save();
        //Obter o tipo de prato
        $dishType = Type_Dish::find($request->get('dishType'));
        //Criar prato com o id do produto e o tipo de prato
        $dish = new Dish();
        $dish->product_id = $product->id;
        $dish->type_dish_id = $dishType->id;
        $dish->name = $product->name;
        $dish->save();
        $insDish = 1;    
        //Retorno
        return redirect('/admin/produtos#menu');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Obter prato
        $dish = Dish::find($id);
        //Retorno
        return redirect('/admin/produtos#menu');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Obter prato
        $dish = Dish::find($id);
        //Retorno
        return redirect('/admin/produtos#menu');
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Obter prato
        $dish = Drink::find($id);
        //Criar Produto
        $product = Product::where('id', '=', $dish->product_id);
        $product->name = $request->get('name');
        $product->status = $request->get('status');
        //Adicionar imagem
        if($file){ //-> VERIFICA SE EXISTE FICHEIRO
            if($file->clientExtension() == 'jpeg' || $file->clientExtension() == 'png'){ //-> VERIFICA A EXTENSÃO DO FICHEIRO
                $filename = $file->getClientOriginalName();
                $product->img = $filename;
                Storage::disk('local')->put($filename, File::get($file));
            }else{
                $extError = 1;
                //Obter todas as bebidas
                $dishes = Dish::all();              
                return [$extError, $dishes];
            }
        }
        $product->save();
        //Alterar prato     
        $dish->quantityavailable = $request->get('quantityavailable');
        $dish->quantitymin = $request->get('quantitymin');
        $dish->save();
        $upDish = 1;    
        //Obter todas as bebidas
        $dishes = Dish::all();
        //Retorno
        return redirect('/admin/produtos#menu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Obter prato
        $dish = Dish::find($id);
        //Obter produto
        $product = Product::where('id', '=', $dish->product_id);
        //Eliminar prato
        $dish->delete();
        //Eliminar produto
        $product->delete();
        //Obter todos os pratos
        $dishes = Dish::all();
        $delDish = 1;
        //Retorno
        return redirect('/admin/produtos#menu');
    }
    public function AlterarMenu($id)
    {
        $product = Product::find($id);
        if($product->menu ==1){
            $product->menu = 0;
        }else{
            $product->menu=1;
        }
        $product->save();
        return redirect('/admin/produtos#menu');
    }
}
