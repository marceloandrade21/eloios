<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Gate;
use App\Dessert;
use App\Product;
use Illuminate\Http\Request;

class DessertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Obter sobremesas
        $dessert = Dessert::all();
        //Retorno
        return $desert;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Criar Produto
         //Criar Produto
         $product = new Product();
         $product->name = $request->get('name');
         $product->menu = 0;
        // //Adicionar imagem
        // if($file){ //-> VERIFICA SE EXISTE FICHEIRO
        //     if($file->clientExtension() == 'jpeg' || $file->clientExtension() == 'png'){ //-> VERIFICA A EXTENSÃO DO FICHEIRO
        //         $filename = $file->getClientOriginalName();
        //         $product->img = $filename;
        //         Storage::disk('local')->put($filename, File::get($file));
        //     }else{
        //         $extError = 1;
        //         //Obter todas as sobremesas
        //         $desserts = Desserte::all();              
        //         return [$extError, $desserts];
        //     }
        // }else{
        //     $filename = 'default.jpeg';
        //     $product->img = $filename;
        // }
        $product->save();        
        //Criar digestivo com o id do produto e o tipo de bebida
        $dessert = new Dessert();
        $dessert->name = $request->get('name');
        $dessert->product_id = $product->id;
        $dessert->quantityavailable = $request->get('qtd_existente');
        $dessert->quantitymin = $request->get('qtd_min');
        $dessert->save();
        $insDessert = 1;    
        //Obter todas as digestivos
        $desserts = Dessert::all();
        //Retorno
        return redirect('/admin/produtos#sobremesa');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Obter sobremesa
        $dessert = Dessert::find($id);
        //Retorno
        return redirect('/admin/produtos#sobremesa');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Obter sobremesa
        $dessert = Dessert::find($id);
        //Retorno
        return redirect('/admin/produtos#sobremesa');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = $request->get('id');
        //Obter diverso
        $dessert = Dessert::find($id);
        //Obter produto
        $product = Product::where('id', '=', $dessert->id)->first();
        
        $product->name = $product->name;
        $product->menu = 0;
        // //Adicionar imagem
        // if($file){ //-> VERIFICA SE EXISTE FICHEIRO
        //     if($file->clientExtension() == 'jpeg' || $file->clientExtension() == 'png'){ //-> VERIFICA A EXTENSÃO DO FICHEIRO
        //         $filename = $file->getClientOriginalName();
        //         $product->img = $filename;
        //         Storage::disk('local')->put($filename, File::get($file));
        //     }else{
        //         $extError = 1;
        //         //Obter todos os diversos
        //         $desserts = Dessert::all();              
        //         return [$extError, $desserts];
        //     }
        // }
        $product->save();
        $dessert->quantityavailable += $request->get('qtd');
       
        $dessert->quantitymin = $dessert->quantitymin ;
        $dessert->save();
         
        $upDessert=1;
        //Obter todos os diversos
        $desserts = Dessert::all();
        //Retorno
        return redirect('/admin/produtos#sobremesa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Obter sobremesa
        $dessert = Dessert::find($id);
        //Obter produto
        $product = Product::where('id', '=', $dessert->product_id);

        //Eliminar sobremesa
        $dessert->delete();
        //Eliminar produto
        $product->delete();
        //Obter todas as sobremesas
        $desserts = Dessert::all();
        $delDessert = 1;
        //Retorno
        return redirect('/admin/produtos#sobremesa');
    }
}
