<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Product;
use App\Various;
use App\Drink;
use App\Dessert;
use App\Type_Drink;
Use App\Dish;
use App\Type_Dish;
use App\Appetizer;
use Illuminate\Http\Request;
use Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user() && Auth::user()->utype_id == 1){
        $diversos = Various::all();
        $entradas = Appetizer::all();
        $sobremesas=Dessert::all();
        $tipos= Type_Drink::all();
        $produtos = Product::all();
        $pratos = Dish::all();
        $tipospratos = Type_Dish::all();
        $drinks = Drink::paginate(5);
        
        return view('UI_Admin.gerirProdutos')
         ->with(compact('produtos','drinks','tipos','sobremesas','diversos','pratos','tipospratos','entradas'));
        }else return redirect('/login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $product = new Product();
        $product->name = $request->get('name');
        $product->status = $request->get('status');
        // //Adicionar imagem
        // if($file){ //-> VERIFICA SE EXISTE FICHEIRO
        //     if($file->clientExtension() == 'jpeg' || $file->clientExtension() == 'png'){ //-> VERIFICA A EXTENSÃO DO FICHEIRO
        //         $filename = $file->getClientOriginalName();
        //         $product->img = $filename;
        //         Storage::disk('local')->put($filename, File::get($file));
        //     }else{
        //         $extError = 1;
        //         //Obter todas as bebidas
        //         $products = Dish::all();              
        //         return [$extError, $products];
        //     }
        // }else{
        //     $filename = 'default.jpeg';
        //     $product->img = $filename;
        // }
        $product->save();
        $products = Product::all();
        $insProd = 1;   
        return [$insProd, $products];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $product = Product::find($id);
        return $product;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $product = Product::find($id);
        return $product;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $product =  Product::find($id);
        $product->name = $request->get('name');
        $product->status = $request->get('status');
        //Adicionar imagem
        if($file){ //-> VERIFICA SE EXISTE FICHEIRO
            if($file->clientExtension() == 'jpeg' || $file->clientExtension() == 'png'){ //-> VERIFICA A EXTENSÃO DO FICHEIRO
                $filename = $file->getClientOriginalName();
                $product->img = $filename;
                Storage::disk('local')->put($filename, File::get($file));
            }else{
                $extError = 1;
                //Obter todas as bebidas
                $products = Dish::all();              
                return [$extError, $products];
            }
        }else{
            $filename = 'default.jpeg';
            $product->img = $filename;
        }
        $product->save();
        $products = Product::all();
        $insProd = 1;   
        return [$insProd, $products];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        //Obter produto
        $product = Product::where('id', '=', $id);
        //Eliminar produto
        $product->delete();
        //Obter todos os pratos
        $products = Dish::all();
        $delPro = 1;
        //Retorno
        return [$delPro, $products];
    }
}
