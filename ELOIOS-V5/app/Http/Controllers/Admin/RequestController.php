<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Request as Pedido;
use App\Product_Request;
use Illuminate\Http\Request;

class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pedidosNoFuncionario = Pedido::where('status','=',1)->get();
        $pedidosNaCozinha = Pedido::where('status','=',0)->get();
        $pedidosFechados= Pedido::where('status','=',null)->get();
        $sucessodel=0;
        return view('UI_Admin.GerirPedidos',compact('pedidosNoFuncionario','pedidosNaCozinha','pedidosFechados','sucessodel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $pedido= Pedido::find($id);
       $product_requests = Product_Request::where('request_id','=',$id)->get();
       return view('UI_Admin.PedidoShow',compact('pedido','product_requests'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pedido = Pedido::find($id);
        $pedido->status=1;
        $pedido->save();
        $linhaspedido = Product_Request::where('request_id','=',$pedido->id)->get();
        foreach($linhaspedido as $linhapedido){
            $linhapedido->delete();
        }
        $pedido->delete();
        $pedidosNoFuncionario = Pedido::where('status','=',1)->get();
        $pedidosNaCozinha = Pedido::where('status','=',0)->get();
        $pedidosFechados= Pedido::where('status','=',null)->get();
        $sucessodel=1;
        return view('UI_Admin.GerirPedidos',compact('pedidosNoFuncionario','pedidosNaCozinha','pedidosFechados','sucessodel'));
    }
}
