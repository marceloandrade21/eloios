<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Reserve;
use Illuminate\Http\Request;
use Auth;

class ReserveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user() && Auth::user()->utype_id == 1){
        //Obter todas as reservas
        $reserves = Reserve::all();
        //Retorno
        $sucessodel=0;
        $sucessoconf=0;
        return view('UI_Admin.gerirReservas',compact('reserves','sucessodel','sucessoconf'));
        }else return redirect('/login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reserve  $reserve
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Obter reserva
        $reserve = Reserve::find($id);
        //Retorno
        return $reserve;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reserve  $reserve
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Obter reserva
        $reserve = Reserve::find($id);
        //Retorno
        return $reserve;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reserve  $reserve
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Obter reserva
        $reserve = Reserve::find($id);
        $reserve->status=1;
        $reserve->save();
        $upReserve = 1;
        //Obter todas as reservas
        $reserves = Reserve::all();
        //Retorno
        return [$upReserve, $reserves];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reserve  $reserve
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {     
        //Obter reserva
        $reserve = Reserve::find($id);
        //Eliminar reserva
        $reserve->delete();
        $reserves = Reserve::all();
        $sucesso=1;
        //Retorno
        return view('UI_Admin.verreserva',compact('reserves','sucesso'));
    }

    public function confirmar($id)
    {
         //Obter reserva
         
         $reserve = Reserve::find($id);
         $reserve->status=1;
         $reserve->save();
         $reserves = Reserve::all();
         $sucessodel=0;
         $sucessoconf=1;
        // $email = $reserve->user->email;
        //  \Mail::send('Emails.email', [], function ($message) use ($email)
        //  {
        //      $message->to($reserve->user->email);
        //  });
         //Retorno
         return view('UI_Admin.gerirReservas',compact('reserves','sucessodel','sucessoconf'));
    }
}
