<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Product;
use App\Various;
use App\Drink;
use App\Dessert;
use App\Type_Drink;
Use App\Dish;
use App\Type_Dish;
use App\Lista;
use Illuminate\Http\Request;
use Auth;


class ListaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     if (Auth::user() && Auth::user()->utype_id == 1){
        $sucesso=0;
        $diversos = Various::all();
        $sobremesas=Dessert::all();
        $drinks = Drink::all();
        $lista=[];
        return view('UI_Admin.listaCompras',compact('drinks','sobremesas','diversos','produtos','lista','sucesso'));
    }else return redirect ('/login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $lista = new Lista();
        $lista->name = $request->get('name');
        $lista-> save();
        $lista= Lista::all();
        $sucesso=1;
        $diversos = Various::all();
        $sobremesas=Dessert::all();
        $drinks = Drink::all();
        return view('UI_Admin.listaCompras',compact('drinks','sobremesas','diversos','lista','sucesso'));
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lista  $lista
     * @return \Illuminate\Http\Response
     */
    public function show(Lista $lista)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lista  $lista
     * @return \Illuminate\Http\Response
     */
    public function edit(Lista $lista)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lista  $lista
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lista $lista)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lista  $lista
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lista $lista)
    {
        //
    }
}
