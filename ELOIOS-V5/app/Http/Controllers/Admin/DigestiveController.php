<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Digestive;
use App\Product;
use Illuminate\Http\Request;

class DigestiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Obter todos os digestivos
        $digestives = Digestive::all();
        //Retorno
        return $digestives;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Criar Produto
        $product = new Product();
        $product->name = $request->get('name');
        $product->status = $request->get('status');
        //Adicionar imagem
        if($file){ //-> VERIFICA SE EXISTE FICHEIRO
            if($file->clientExtension() == 'jpeg' || $file->clientExtension() == 'png'){ //-> VERIFICA A EXTENSÃO DO FICHEIRO
                $filename = $file->getClientOriginalName();
                $product->img = $filename;
                Storage::disk('local')->put($filename, File::get($file));
            }else{
                $extError = 1;
                //Obter todas as bebidas
                $digestives = Digestive::all();              
                return [$extError, $digestives];
            }
        }else{
            $filename = 'default.jpeg';
            $product->img = $filename;
        }
        $product->save();        
        //Criar digestivo com o id do produto e o tipo de bebida
        $digestive = new Digestive();
        $digestive->product_id = $product->id;
        $digestive->save();
        $insDigestive = 1;    
        //Obter todas as digestivos
        $digestives = Digestive::all();
        //Retorno
        return [$insDigestive, $digestives];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Obter digestivo  
        $digestive = Digestive::find($id);
        //Retorno
        return $digestive;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Obter digestivo  
        $digestive = Digestive::find('id');
        //Retorno
        return $digestive;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Obter digestivo
        $digestive = Digestive::find($id);
        //Obter produto
        $product = Product::where('id', '=', $digestive->id)->first();
        $product->name = $request->get('name');
        $product->status = $request->get('status');
        //Adicionar imagem
        if($file){ //-> VERIFICA SE EXISTE FICHEIRO
            if($file->clientExtension() == 'jpeg' || $file->clientExtension() == 'png'){ //-> VERIFICA A EXTENSÃO DO FICHEIRO
                $filename = $file->getClientOriginalName();
                $product->img = $filename;
                Storage::disk('local')->put($filename, File::get($file));
            }else{
                $extError = 1;
                //Obter todas as bebidas
                $digestives = Digestive::all();              
                return [$extError, $digestives];
            }
        }
        $product->save();
        $upDigestive=1;
        //Obter todas os digestivos
        $digestives = Digestive::all();
        //Retorno
        return [$upDigestive, $digestives];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Obter digestivo
        $digestive = Digestive::find($id);
        //Obter Produto
        $product = Product::where('id', '=', $digestive->product_id);
        //Eliminar digestivo
        $digestive->delete();
        //Eliminar produto
        $product->delete();
        $delDigestive = 1;
        //Obter todos os digestivos
        $digestives = Digestive::all();
        //Retorno
        return [$delDigestive, $digestives];
    }
}
