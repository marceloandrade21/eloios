<?php


namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Auth;
use Illuminate\Http\Request;
use App\Contact;


class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sucesso=0;
        $reclamacoes= Contact::paginate(5);
        if (Auth::user() && Auth::user()->utype_id == 1) 
        return view('UI_Admin.reclamacoes')
            ->with(compact('reclamacoes','sucesso'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      
        $reclamacao = Contact::find($id);
       
        $reclamacao->delete();
        $reclamacoes= Contact::paginate(5);
        $sucesso=1;

      
        return view('UI_Admin.reclamacoes')
        ->with(compact('reclamacoes','sucesso'));
    }

    public function Status($id)
    {     
        $reclamacao = Contact::find($id);
        if($reclamacao->status==1){
        $reclamacao->status = 0;
        }else{
           $reclamacao->status = 1;
        }
        $reclamacao->save();
        return redirect('/admin/reclamacoes');
    }

}
