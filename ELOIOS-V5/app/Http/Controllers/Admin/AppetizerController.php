<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Appetizer;
use App\Product;
use Illuminate\Http\Request;

class AppetizerController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Obter todos entradas
        $appetizers = Appetizer::all();
        //Retorno
        return $appetizers;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        //Criar produto
        $product = new Product();
        $product->name = $request->get('name');
        $product->menu = 0;
        // //Adicionar imagem
        // if($file){ //-> VERIFICA SE EXISTE FICHEIRO
        //     if($file->clientExtension() == 'jpeg' || $file->clientExtension() == 'png'){ //-> VERIFICA A EXTENSÃO DO FICHEIRO
        //         $filename = $file->getClientOriginalName();
        //         $product->img = $filename;
        //         Storage::disk('local')->put($filename, File::get($file));
        //     }else{
        //         $extError = 1;
        //         //Obter todas as bebidas
        //         $drinks = Drink::all();              
        //         return [$extError, $drinks];
        //     }
        // }else{
        //     $filename = 'default.jpeg';
        //     $product->img = $filename;
        // }
        $product->save();
        //Criar entradas
        $appetizer = new Appetizer();
        $appetizer->product_id = $product->id;
        $appetizer->name = $request->get('name');
        $appetizer->save();
        $insAppe=1;
        //Obter todas as entradas
        $appetizers = Appetizer::all();
        //Retorno
        return redirect('/admin/produtos#entrada');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appetizer  $appetizer
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Obter entrada
        $appetizer = Appetizer::find($id);
        //Retorno
        return $appetizer;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appetizer  $appetizer
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Obter entrada
        $appetizer = Appetizer::find($id);
        //Obter produto
        $product = Product::where('id', '=', $appetizer->id)->first();
        //Retorno
        return [$appetizer, $product];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Appetizer  $appetizer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Obter produto
        $appetizer = Appetizer::find($id);
        $product = Product::where('id', '=', $appetizer->id)->first();
        $product->name = $request->get('name');
        $product->status = $request->get('status');
      
        $product->save();
        $upAppe=1;
        //Obter todas as entradas
        $appetizers = Appetizer::all();
        //Retorno
        return redirect('/admin/produtos#entrada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appetizer  $appetizer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $appetizer = Appetizer::find($id);
        $product = Product::where('id','=',$appetizer->product_id)->first();
        $appetizer->delete();
        $product->delete();
        $delAppe=1;
        return redirect('/admin/produtos#entrada');
    }
}
