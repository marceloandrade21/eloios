<?php


namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Type_Drink;
use Illuminate\Http\Request;
class DrinkTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $drinkstype = Type_Drink::all();
        return $drinkstype;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $drinktype = new Type_Drink();
        $drinktype->name = $request->get('name');
        $drinktype->save();
        $sucesso= 1;

        $drinkstype = Type_Drink::all();

        return view('UI_Admin.Admin')->with(compact('sucesso'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $drinktype = Type_Drink::find($id);
        return $drinktype;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $drinktype = Type_Drink::find($id);
        return $drinktype;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $drinktype = Type_Drink::find($id);
        $drinkstype->name = $request->get('name');
        $updDt =1;
        $drinkstype = Type_Drink::all();
        return [$updDt,$drinkstype];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $drinktype = Type_Drink::find($id);
        $drinktype->delete();
        $delDt = 1;
        $drinkstype = Type_Drink::all();
        return [$delDt,$drinkstype];
    }
}
