<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Gate;
use Auth;
use Illuminate\Http\Request;
use App\User;
use App\Type_User;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user() && Auth::user()->utype_id == 1)
        {
            $users = User::where('utype_id','=',2)->paginate(5);
            
            return view('UI_Admin.gerirUsers')
            ->with(compact('users'));
        }
            
        
    }

    // RETORNA JSON DE FUNCIONÁRIOS
    public function ListaFuncionarios()
    {
        //Obter todos os funcionarios
        $funcionarios = User::where('utype_id','=',3)->paginate(5);
        //Retorno
        
        return view('UI_Admin.gerirFuncionarios')
        ->with(compact('funcionarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            
         'name' => 'required|string|max:255',
         'email' => 'required|string|email|max:255|unique:users',
         'password' => 'required|string|min:6|confirmed',
         'phone' => 'required|string|min:9|max:13',
     ]);
        //Criar utilizador funcionario
        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = $request->get('password');
        $user->phone=$request->get('phone');
        $user->utype_id = 3; // SÓ PARA FUNCIONÁRIOS
        $user->status=1;
        $user->save();
        
        $insFunc = 1;
        //Obter todos os funcionarios
        $funcionarios = User::where('utype_id','=',3)->get();
        //Retorno
        return view('UI_Admin.gerirFuncionarios')
        ->with(compact('$insUser', '$funcionarios'));
       
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Obter utilizador
        $user = User::find($id);
        //Retorno
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Obter user
        $user = User::find($id);
        //Apagar user
        $user->delete();
      
        //Obter todos os users
        $users = User::all();
        return redirect('/admin/utilizadores');
    }
     // BLOQUEIA OU DESBLOQUEIA UTILIZADORES 
     public function bloquear($id)
     {     
         $user = User::find($id);
         if($user->status==1){
         $user->status = 0;
         }else{
            $user->status = 1;
         }
         $user->save();
         return redirect('/admin/utilizadores');
     }
}
