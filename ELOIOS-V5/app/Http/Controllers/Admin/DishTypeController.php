<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Type_Dish;
use Illuminate\Http\Request;

class DishTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dishestype = Type_Dish::all();
        return $dishestype;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $dishtype = new Type_Dish();
        $dishtype->name = $request->get('name');
        $sucesso= 1;
        $dishtype->save();
        $tipos = Type_Dish::all();

        return view('UI_Admin.Admin')->with(compact('sucesso','tipos'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dishtype = Type_Dish::find($id);
        return $dishtype;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dishtype = Type_Dish::find($id);
        return $dishtype;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dishtype = Type_Dish::find($id);
        $dishtype->name = $request->get('name');
        $updDt =1;
        $dishestype = Type_Dish::all();
        return [$updDt,$dishestype];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dishtype = Type_Dish::find($id);
        $dishtype->delete();
        $sucesso = 1;
        $tipos = Type_Dish::all();
        return view('UI_Admin.Admin')->with(compact('sucesso','tipos'));
    }
}
