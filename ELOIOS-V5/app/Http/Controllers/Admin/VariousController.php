<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Various;
use App\Product;
use Illuminate\Http\Request;

class VariousController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Obter todos os Diversos
        $various = Various::all();
        //Retorno
        return $various;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Criar Produto
        $product = new Product();
        $product->name = $request->get('name');
        $product->menu = 0;
        $product->save();        
        //Criar digestivo com o id do produto e o tipo de bebida
        $varied = new Various();
        $varied->product_id = $product->id;
        $varied->name = $product->name;
        $varied->quantityavailable = $request->get('quantityavailable');
        $varied->quantitymin = $request->get('quantitymin');
        $varied->save();
        $insVaried = 1;    
        //Obter todas as digestivos
        $various = Various::all();
        //Retorno
        return redirect('/admin/produtos#diverso');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Obter diverso
        $varied = Various::find($id);
        //Retorno
        return redirect('/admin/produtos#diverso');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Obter diverso
        $varied = Various::find($id);
        //Retorno
        return redirect('/admin/produtos#diverso');
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        $id = $request->get('id');
        //Obter diverso
        $varied = Various::find($id);
        //Obter produto
        $product = Product::where('id', '=', $varied->id)->first();
        $product->name = $product->name;
        $product->menu = 0;
        // //Adicionar imagem
        // if($file){ //-> VERIFICA SE EXISTE FICHEIRO
        //     if($file->clientExtension() == 'jpeg' || $file->clientExtension() == 'png'){ //-> VERIFICA A EXTENSÃO DO FICHEIRO
        //         $filename = $file->getClientOriginalName();
        //         $product->img = $filename;
        //         Storage::disk('local')->put($filename, File::get($file));
        //     }else{
        //         $extError = 1;
        //         //Obter todos os diversos
        //         $various = Various::all();              
        //         return [$extError, $various];
        //     }
        // }
        $product->save();
        $varied->quantityavailable += $request->get('qtd');
        $varied->quantitymin = $varied->quantitymin;
        $varied->save();
        $upVaried=1;
        //Obter todos os diversos
        $various = Various::all();
        //Retorno
        return redirect('/admin/produtos#diverso');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Obter diverso
        $varied = Various::find($id);
        //Obter produto
        $product = Product::where('id', '=', $varied->product_id);
        //Eliminar diverso
        $varied->delete();
        //Eliminar produto
        $product->delete();
        return redirect('/admin/produtos#diverso');
        
    }


    public function rmStock(Request $request)
    {      
      
        $id = $request->get('id');
        //Obter diverso
        $varied = Various::find($id);
        //Obter produto
        $product = Product::where('id', '=', $varied->id)->first();
        $product->name = $product->name;
        $product->menu = 0;
        // //Adicionar imagem
        // if($file){ //-> VERIFICA SE EXISTE FICHEIRO
        //     if($file->clientExtension() == 'jpeg' || $file->clientExtension() == 'png'){ //-> VERIFICA A EXTENSÃO DO FICHEIRO
        //         $filename = $file->getClientOriginalName();
        //         $product->img = $filename;
        //         Storage::disk('local')->put($filename, File::get($file));
        //     }else{
        //         $extError = 1;
        //         //Obter todos os diversos
        //         $various = Various::all();              
        //         return [$extError, $various];
        //     }
        // }
        $product->save();
        $varied->quantityavailable -= $request->get('qtd');
        $varied->quantitymin = $varied->quantitymin;
        $varied->save();
        $upVaried=1;
        //Obter todos os diversos
        $various = Various::all();
        //Retorno
        return redirect('/admin/produtos#diverso');
        
    }

}
