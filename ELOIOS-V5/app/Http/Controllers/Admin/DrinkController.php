<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Drink;
use Illuminate\Http\Request;
use App\Type_Drink;
use App\Product;
class DrinkController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //Criar Produto
        $product = new Product();
        $product->name = $request->get('name');
        $product->menu = 0;
        
        // //Adicionar imagem
        // if($file){ //-> VERIFICA SE EXISTE FICHEIRO
        //     if($file->clientExtension() == 'jpeg' || $file->clientExtension() == 'png'){ //-> VERIFICA A EXTENSÃO DO FICHEIRO
        //         $filename = $file->getClientOriginalName();
        //         $product->img = $filename;
        //         Storage::disk('local')->put($filename, File::get($file));
        //     }else{
        //         $extError = 1;
        //         //Obter todas as bebidas
        //         $drinks = Drink::all();              
        //         return [$extError, $drinks];
        //     }
        // }else{
        //     $filename = 'default.jpeg';
        //     $product->img = $filename;
        // }
        $product->save();
        
        //Obter o tipo de bebida
        $drinkType = Type_Drink::find($request->get('drinkType'));
        //Criar Bebida com o id do produto e o tipo de bebida
        $drink = new Drink();
        $drink->name = $request->get('name');
        $drink->product_id = $product->id;
        $drink->type_drink_id = $drinkType ->id;
        $drink->quantityavailable = $request->get('qtd_existente');
        $drink->quantitymin = $request->get('qtd_min');
        $drink->save();
        $insDrink = 1;    
        //Obter todas as bebidas
        $drinks = Drink::all();
        //Retorno
        $tipos= Type_Drink::all();
        $produtos = Product::all();
        $drinks = Drink::all();
        return redirect('/admin/produtos#bebida');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Drink  $drink
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Obter bebida
        $drink = Drink::find($id);
        //Retorno
        return redirect('/admin/produtos#bebida');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Drink  $drink
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Obter bebida
        $drink = Drink::find($id);
        //Obter produto
        $product = Product::where('id', '=', $drink->id)->first();
        //Retorno
        return redirect('/admin/produtos#bebida');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Drink  $drink
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = $request->get('id');

        //Obter a bebida
        $drink = Drink::find($id);
        //Obter o produto
        $product = Product::where('id', '=', $drink->product_id)->first();
        $product->name = $product->name;
        $product->menu = 0;
        // //Adicionar imagem
        // if($file){ //-> VERIFICA SE EXISTE FICHEIRO
        //     if($file->clientExtension() == 'jpeg' || $file->clientExtension() == 'png'){ //-> VERIFICA A EXTENSÃO DO FICHEIRO
        //         $filename = $file->getClientOriginalName();
        //         $product->img = $filename;
        //         Storage::disk('local')->put($filename, File::get($file));
        //     }else{
        //         $extError = 1;
        //         //Obter todas as bebidas
        //         $drinks = Drink::all();              
        //         return [$extError, $drinks];
        //     }
        // }
        $product->save();
        //Obter o tipo de bebida
        $drinkType = Type_Drink::find( $drink->type_drink->id);
        //Criar Bebida com o id do produto e o tipo de bebida
        $drink->quantityavailable += $request->get('qtd');
        $drink->quantitymin =  $drink->quantitymin;
       
        $drink->save();
        $upDrink = 1;    
        //Obter todas as bebidas
        $drinks = Drink::all();
        //Retorno
        return redirect('/admin/produtos#bebida');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Drink  $drink
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Obter bebida
        $drink = Drink::find($id);
        //Obter produto
        $product = Product::where('id', '=', $drink->product_id)->first();
        //Eliminar bebida
        $drink->delete();
        $product->delete();
        //Obter todas as bebidas
        $drinks = Drink::all();
        $delDrink = 1;
        //Retorno
        $produtos = Product::all();
        $drinks = Drink::all();
       
        $tipos= Type_Drink::all();
        $produtos = Product::all();
        $drinks = Drink::all();
        return redirect('/admin/produtos#bebida');
    }
}
