<?php

namespace App\Http\Controllers\Funcionario;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Drink;
use App\Type_Drink;

class DrinkController extends Controller
{
    public function DrinkByDrinkType($id){
        //Obter o tipo de bebida
        $drinktype = Type_Drink::find($id);
        //Obter todas as bebidas daquele tipo
        $drinks = Drink::where('type_drink_id', '=', $id)->first();
        //Retorno
        return $drinks;
    }
}
