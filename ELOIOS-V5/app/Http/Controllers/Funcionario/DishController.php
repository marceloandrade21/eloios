<?php

namespace App\Http\Controllers\Funcionario;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Dish;
use App\Type_Dish;

class DishController extends Controller
{
    public function DishByDishType($id){
        //Obter o tipo de prato
        $dishtype = Type_Dish::find($id);
        //Obter todos os pratos daquele tipo
        $dishes = Dish::where('type_dish_id', '=', $id)->first();
        //Retorno
        return $dishes;
    }
}
