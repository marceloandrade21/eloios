<?php

namespace App\Http\Controllers\Funcionario;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Digestive;

class DigestiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Obter todos os digestivos
        $digestives = Digestive::all();
        //Retorno
        return $digestives;
    }
}
