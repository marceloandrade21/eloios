<?php

namespace App\Http\Controllers\Funcionario;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Dessert;

class DessertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Obter todas as sobremesas
        $desserts = Dessert::all();
        //Retorno
        return $desserts;
    }
}
