<?php

namespace App\Http\Controllers\Funcionario;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Product;
use App\Appetizer;
use App\Dessert;
use App\Digestive;
use App\Dish;
use App\DishType;
use App\Various;
use App\Drink;
use App\DrinkType;
use App\Request;
use App\Product_Request;

class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Obter todos os pedidos
        $requests = Request::where('status', '!=', null)->get();
        //Retorno
        return $request;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Obter pedido
        $request = Request::find($id);
        //Obter linhas de pedido
        $requestlines = Product_Request::where('request_id', '=', $id)->get();
        //Retorno
        return [$request, $requestlines];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Obter pedido
        $request = Request::find($id);
        //Obter linhas de pedido
        $requestlines = Product_Request::where('request_id', '=', $id)->get();
        //Retorno
        return [$request, $requestlines];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    //MUDA O ESTADO DO PEDIDO DE NÃO PAGO PARA PAGO
    public function IsPaid($id)
    {
        //Obter pedido
        $request = Request::find($id);
        //Mudar o estado do pedido
        $request->status = null;
        $request->save();
        //Retorno
        $isPaid = 1;
        return $isPaid;
    }

    //ELIMINA UMA LINHA AO PEDIDO
    public function DeleteLine($id)
    {   
        //Obter linha do pedido
        $requestline = Product_Request::find($id);
        //Obter pedido 
        $request = Request::find($requestline->request_id);     
        //Eliminar linha do pedido
        $shopcartline->delete();
        //Retorno
        $delRequestLine = 1;
        return [$delRequestLine, $request];
    }

    //FINALIZA O PEDIDO
    public function ConfirmRequest($id)
    {
        $request = Request::find($id);
        $requestlines = Product_Request::where('request_id', '=', $request->id)->get();

        //Por cada linha de pedido
        foreach($requestlines as $requestline){
            //Obter produto da linha
            $product = Product::find($requestline->product_id)->first();
            //
            $dessert = Dessert::where('product_id', '=', $product->id)->first();
            //
            $varied = Various::where('product_id', '=', $product->id)->first();
            //
            $drink = Drink::where('product_id', '=', $product->id)->first();
            //Verifica se o produto é sobremesa
            if($dessert){
                $isOk = $this->stockManager($requestline->quantity, $dessert->quantityavailable);
                if($isOk != 1){
                    return [$isOk, $request, $requestlines];
                }
            }
            //Verifica se o produto é diverso
            if($varied){
                $isOk = $this->stockManager($requestline->quantity, $varied->quantityavailable);
                if($isOk != 1){
                    return [$isOk, $request, $requestlines];
                }
            }
            //Verifica se o produto é bebida
            if($drink){
                $isOk = $this->stockManager($requestline->quantity, $drink->quantityavailable);
                if($isOk != 1){
                    return [$isOk, $request, $requestlines];
                }
            }            
        }
        $isOk = 1;
        return $isOk;
    }

    //GERE O STOCK
    function stockManager($lineQuant, $prodQuant){
        $isOk;
        if($lineQuant->quantity <= $prodQuant->quantityavailable){
            $quantity = $prodQuant->quantityavailable - $lineQuant->quantity;
            $prodQuant->quantityavailable = $quantity;
            $prodQuant->save();
            $isOk = 1;
        }else{
            $isOk = 0;            
        }
        return $isOk;
    }
}