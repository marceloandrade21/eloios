<?php

namespace App\Http\Controllers\Funcionario;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Obter todos os produtos
        $products = Product::all();
        //Retorno
        return $products;
    }
}
