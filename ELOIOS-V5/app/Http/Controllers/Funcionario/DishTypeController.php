<?php

namespace App\Http\Controllers\Funcionario;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Type_Dish;

class DishTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Obter todos os tipos de prato
        $dishtypes = Type_Dish::all();
        //Retorno
        return $dishtypes;
    }
}
