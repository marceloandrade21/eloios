<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type_Dish extends Model
{
    protected $table = "dish_types";
    public function dishes()
    {
        return $this->hasMany(Dish::class);
    }
}
