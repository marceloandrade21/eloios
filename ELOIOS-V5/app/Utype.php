<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Utype extends Model
{
    protected $table="utypes";

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
