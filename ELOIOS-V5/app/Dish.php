<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    public function type_dish()
    {
        return $this->belongsTo(Type_Dish::class);
    }
    public function product(){
        return $this->belongsTo(Product::class);
    }
}
