@extends('UI_Admin.base')
 
 @section('title')
Gerir Funcionarios
 @endsection

 @section('css')
 
  
 @endsection
 
 @section('content')

 <div class="col-lg-12 col-md12">
        <h2>Todos os Funcionarios</h2>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        
                                        <th>Nome</th>
                                        <th>E-mail</th>
										<th>Telemovel</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                              
                             
                                @forelse($funcionarios as $funcionario)
                               
                                    <tr class="info">
                                      
                                        <td>{{ $funcionario->name }}</td>
                                        <td>{{ $funcionario->email}}</td>
                                        <td>{{ $funcionario->phone}}</td>
                                         <td class="direita">
                                                <form method="post" action="/admin/utilizadores/{{ $funcionario->id}}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    
                                                    <button type="submit" class="btn btn-danger" title="Eliminar">
                                                        <i class="fa fa-trash" ></i>
                                                    </button>
                                                </form> 
                                            </td>
                                    </tr>
                                
                                 @empty
                              <div class="alert alert-danger alert-dismissable centrado">
                                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
                                 NÃO EXISTEM FUNCIONÁRIOS PARA APRESENTAR
                                </div>
      
                               @endforelse
                                </tbody>
                               
                            </table>
                         
                        </div>
 
 </div>
</br>
</br>
</br>

   <!-- BOTÕES ADICIONAR E REMOVER Funcionário -->
   <div class="text-center">
        <button type="button" class="btn btn-success btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal_funcionario">
            <i class="fa fa-plus-circle i"></i>Adicionar Funcionário
        </button>
       
    </div>

<!-- Modal INSERIR FUNCIONARIO -->
<form class="form-horizontal" method="POST" action="/admin/utilizadores">
                            {{ csrf_field() }}
    <div class="modal fade" id="myModal_funcionario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_funcionario">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel_funcionario">  <i class="fa fa-user i "></i> INSERIR FUNCIONÁRIO</h4>
                </div>

                <!-- CORPO DO MODAL -->
                
                <div class="modal-body centrado">
                                    <div class="row centrado" >
                                        <div class="col-lg-5 col-md-5">
                                            <div class="form-group">
                                                <label>Nome do Funcionário</label>
                                                <input type="text" class="form-control txt" name="name" required Placholder="Nome">
                                                <p class="help-block">Ex: Joaquim</p>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-5 col-md-5">
                                            <div class="form-group" >
                                                <label>E-mail do Fucnionário </label>

                                                <input type="text" class="form-control txt" name="email" required Placeholder="E-mail">
                                                <p class="help-block">Ex:joaquim@gmail.com</p>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row centrado" >
                                        <div class="col-lg-5 col-md-5">
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input id="password" class="form-control txt" type="password"  name="password" required PlaceHolder="Password">
                                                <p class="help-block">Ex: qwe+123 </p>
                                            </div>
                                        </div>
                                        <div class="col-lg-5 col-md-5">
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input id="password-confirm" type="password" class="form-control txt" name="password_confirmation" required PlaceHolder="Confirma a Password">
                                                <p class="help-block">Ex: qwe+123 </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row centrado" >
                                        <div class="col-lg-6 col-md-6" >
                                            <div class="form-group">
                                                <label>Telemovel</label>
                                                <input id="password" type="text" class="form-control " name="phone" required PlaceHolder="Telemovel">
                                                <p class="help-block">Ex:91xxxxxxx</p>
                                            </div>
                                        </div>
                                    </div>

                                    
                </div>
                <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        <input type="submit" value="Criar Funcionário" class="btn btn-primary" runat="server"></input>
                </div>
            </div>
        </div>
    </div>
</form>
    



    <script src='https://code.jquery.com/jquery-2.2.4.js'></script>
    <script>
        $('.main-menu li.active-link').removeClass('active-link');
        $('#funcionarios').addClass("active-link");
        document.getElementById("introducao").innerHTML = "FAÇA A GESTÃO DE TODOS OS FUNCIONARIOS";
        document.getElementById("introducao2").innerHTML = " AQUI PODER VISUALIZAR ADICIONAR BLOQUEAR E ELIMINAR FUNCIONÁRIOS";

    </script>

	@endsection