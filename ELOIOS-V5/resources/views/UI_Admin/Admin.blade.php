@extends('UI_Admin.base')
 
 @section('title')
ADMIN
 @endsection

 @section('css')

 @endsection

 @section('content')
  
  @if($sucesso==1)
        <div class="alert alert-success alert-dismissable" id="success-alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Successo!</strong> Adicionada nova Categoria de Pratos
        </div>
    @endif
    </br>


<table class="table">
                                <thead>
                                    <tr >
                                        <th>Categorias de Prato</th>
                                        {{--  <th>Categorias de Bebidas</th>  --}}
                                    </tr>
                                </thead>
                                <tbody>
                                   @forelse($tipos as $tipo)
                                    <tr>
                                    <td>{{$tipo->name}}</td>
                                            
                                    <td class="direita">
                                                <form method="post" action="/admin/tipopratos/{{$tipo->id}}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    
                                                    <button type="submit" class="btn btn-danger" title="Eliminar">
                                                        <i class="fa fa-trash" ></i>
                                                    </button>
                                                </form> 
                                            </td>
                                     </tr>
                               
                                 @empty
                              <div class="alert alert-danger alert-dismissable centrado">
                                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
                                 NÃO EXISTEM RESERVAS PARA APRESENTAR
                                </div>
      
                               @endforelse
                             
                                </tbody>
                            </table>
                                    <div class="text-center">
                              
                                            <button type="button" class="btn btn-success direita  btn-lg" data-toggle="modal" data-target="#myModal_funcionario">
                                                 <i class="fa fa-plus-circle i"></i>Adicionar Categoria de Prato
                                            </button>
                                             {{--  <button type="button" class="btn btn-success direita" data-toggle="modal" data-target="#myModal_be">
                                        <i class="fa fa-plus-circle i"></i>Adicionar Categoria de Bebida
                                    </button>
                                               --}}
                                    </div>

<!-- Modal INSERIR  TIPO DE PRATO -->
<form class="form-horizontal" method="POST" action="/admin/tipopratos">
                            {{ csrf_field() }}
    <div class="modal fade" id="myModal_funcionario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_funcionario">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel_funcionario">  <i class="fa fa-glass i "></i> INSERIR CATEGORIA</h4>
                </div>

                <!-- CORPO DO MODAL -->
                
                <div class="modal-body centrado">
                                    <div class="row centrado" >
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label>Nome da Categoria</label>
                                                </br>
                                                </br>
                                                <input type="text" class="form-control txt" name="name" required Placholder="Nome">
                                                <p class="help-block">Ex: Vegetariano</p>
                                            </div>
                                        </div>
                                        
                                
                </div>
                <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        <input type="submit" value="Criar Categoria" class="btn btn-primary" runat="server"></input>
                </div>
            </div>
        </div>
    </div>
</form>
                         <!-- Modal INSERIR TIPO D EBEBIDA -->
{{--  <form class="form-horizontal" method="POST" action="">
                            {{ csrf_field() }}
    <div class="modal fade" id="myModal_be" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_be">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel_funcionario">  <i class="fa fa-glass i "></i> INSERIR CATEGORIA</h4>
                </div>
                <!-- CORPO DO MODAL --> 
                <div class="modal-body centrado">
                                    <div class="row centrado" >
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label>Nome da Categoria</label>
                                                </br>
                                                </br>
                                                <input type="text" class="form-control txt" name="name" required Placholder="Nome">
                                                <p class="help-block">Ex: Vegetariano</p>
                                            </div>
                                        </div>
                                        
                                
                </div>
                <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                        <input type="submit" value="Criar Categoria" class="btn btn-primary" runat="server"></input>
                </div>
            </div>
        </div>
    </div>
</form>  --}}
                             

    <script src='https://code.jquery.com/jquery-2.2.4.js'></script>

<script>
         
$("#success-alert").fadeTo(2000, 1000).slideUp(1000, function(){
    $("#success-alert").alert('close');
});
        $('.main-menu li.active-link').removeClass('active-link');
        $('#reservas').addClass("active-link");
        document.getElementById("introducao").innerHTML = "ADMINISTRAÇÃO";
        document.getElementById("introducao2").innerHTML = "AQUI PODE ADICIONAR TIPOS DE PRATO";

</script>
 
 @endsection