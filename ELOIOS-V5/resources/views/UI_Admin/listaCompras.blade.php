@extends('UI_Admin.base') 
@section('title') Lista de Compras 
@endsection

@section('script') 
<script src='https://code.jquery.com/jquery-2.2.4.js'></script>
   
@endsection

 @section('content')

	<h2>Lista de Compras</h2>
    </br>
    <div class="col-sm-12">
                @if($sucesso==1)
                        <div class="alert alert-success alert-dismissable" id="success-alert" style="margin-top:30px">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>Produto Adicionado com Sucesso!</strong>
                        </div>
                @endif
    </div>
    <div id="tabela">
        <table class="table" id="mytable">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Quantidade de Stock</th>
                    <th>Quantidade Mínima</th>
                </tr>
            </thead>
            <tbody id="lista">
                @foreach($drinks as $produto)
                <tr class="warning">
                    @if($produto->quantityavailable<=$produto->quantitymin)
                        <td>{{ $produto->name }}</td>
                        <td>{{ $produto->quantityavailable }}</td>
                        <td>{{ $produto->quantitymin }}</td>
                    @endif
                </tr>
                @endforeach
                @foreach($sobremesas as $produto)
                <tr class="warning">
                    @if($produto->quantityavailable<=$produto->quantitymin)
                        <td>{{ $produto->name }}</td>
                        <td>{{ $produto->quantityavailable }}</td>
                        <td>{{ $produto->quantitymin }}</td>
                        @endif
                </tr>
                @endforeach
                @foreach($diversos as $produto)
                <tr class="warning">
                    @if($produto->quantityavailable<=$produto->quantitymin)
                        <td>{{ $produto->name }}</td>
                        <td>{{ $produto->quantityavailable }}</td>
                        <td>{{ $produto->quantitymin }}</td>
                        @endif
                </tr>
                @endforeach	
            
                    
                
                
                
            </tbody>
        </table>
    </div>

	
    <div class="row centrado"style="margin-bottom:10 px;" >

      <!-- BOTÕES ADICIONAR E REMOVER Funcionário -->
        <div class="text-center">
            <button type="button" class="btn btn-success btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal_funcionario">
                <i class="fa fa-plus-circle i"></i>Adicionar Produto à lista
            </button></br></br>
            <button type="button" class="btn btn-primary btn btn-primary btn-lg" name="imprimir" value="Imprimir" onclick="imp()"> <i class="fa fa-print" aria-hidden="true"></i> Imprimir Lista </button>
      </br>
       </br>
        </div>

        <!-- Modal INSERIR FUNCIONARIO -->
        {{--  <form class="form-horizontal" method="POST" action="/admin/lista">
                                    {{ csrf_field() }}  --}}
            <div class="modal fade" id="myModal_funcionario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_funcionario">
                <div class="modal-dialog " role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center" id="myModalLabel_funcionario">  <i class="fa fa-user i "></i> INSERIR PRODUTO</h4>
                        </div>

                        <!-- CORPO DO MODAL -->
                        
                        <div class="modal-body centrado">
                                            <div class="row centrado" >
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="form-group">
                                                        <label>Nome do Produto</label>
                                                        <input type="text" class="form-control txt"id="valor" required Placholder="Nome">
                                                        <p class="help-block">Ex: Esfregão</p>
                                                    </div>
                                                </div>
                                                            
                                            
                        </div>
                        <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                <input type="submit" value="Adicionar à lista" id="botao" class="btn btn-primary" onclick="funcao()" data-dismiss="modal"></input>
                        </div>
                    </div>
                </div>
            </div>
        {{--  </form>  --}}
    

	</div>







<script>
         
        document.getElementById("introducao").innerHTML = "LISTA DE COMPRAS";
        document.getElementById("introducao2").innerHTML = "Aqui pode visualizar todos os artigos que necessita assim como adicionar produtos à lista";

$("#success-alert").fadeTo(3000, 500).slideUp(500, function(){
    $("#success-alert").alert('close');
     
});

    
$( "#botao" ).click(function() {
	$('#mytable > tbody:last-child').append('<tr><td>' + $( "#valor" ).val() +'</td><tr>');
});


 function imp(){
    var printContents = document.getElementById("tabela").innerHTML;
     var originalContents = document.body.innerHTML;
   
     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
 }


</script>

@endsection