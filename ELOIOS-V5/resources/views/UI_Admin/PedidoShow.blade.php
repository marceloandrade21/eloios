   @extends('UI_Admin.base')
 
 @section('title')
 Pedido {{$pedido->id}}
 @endsection
 
 @section('content')
  <h3 class="text-center"><strong>Linhas do Pedido</strong></h3>
  <table class="table">
                                <thead>
                                    <tr >
                                        <th>ID</th>
                                        <th>Produto</th>
                                        <th>Quantidade</th>
                                    </tr>
                                </thead>
                                <tbody>
                                      @forelse($product_requests as $line)
                                       <tr class="info">
                                      <td>{{$line->id}}</td>
                                      <td>{{$line->product->name}}</td>
                                      <td>{{$line->quantity}}</td>
                                      </tr>
                                        
                                     
                                 @empty
                              <div class="alert alert-danger alert-dismissable centrado">
                                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
                                 NÃO EXISTEM ITEMS PARA APRESENTAR
                                </div>
      
                               @endforelse
                                </tbody>
                            </table>
<script src='https://code.jquery.com/jquery-2.2.4.js' ></script>
<script>
    $('.main-menu li.active-link').removeClass('active-link');
    $('#pedidos').addClass("active-link");
    document.getElementById("introducao").innerHTML = "Pedido Nr {{$pedido->id}} - Mesa {{$pedido->table->name}}";

</script>

@endsection