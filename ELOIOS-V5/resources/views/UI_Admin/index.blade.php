@extends('UI_Admin.base')
 
 @section('title')
 Index
 @endsection
 
 @section('content')
    <!-- /. ROW  ICONS 1 LINHA -->
    <div class="row text-center pad-top index">

        <!-- VERIFICAR RESERVAS -->
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
            <div class="div-square">
                <a href="/admin/reservas">
                    <i class="fa fa-calendar fa-5x"></i>
                    <h4>Reservas</h4>
                </a>
            </div>

        </div>

        <!-- GERIR UTILIZADORES -->
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
            <div class="div-square">
                <a href="/admin/utilizadores">
                    <i class="fa fa-users fa-5x"></i>
                    <h4>utilizadores</h4>
                </a>
            </div>


        </div>

        <!-- GERIR FUNCIONARIO -->
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
            <div class="div-square">
                <a href="/admin/funcionarios">
                    <i class="fa fa-user fa-5x"></i>
                    <h4>Funcionários</h4>
                </a>
            </div>


        </div>

        <!-- GERIR PRODUTOS -->
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
            <div class="div-square">
               <a href="/admin/produtos">
                    <i class="fa fa-glass fa-5x"></i>
                    <h4>Produtos</h4>
                </a>
            </div>


        </div>

        <!-- ELIMINAR PEDIDO -->
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
            <div class="div-square">
                <a href="/admin/pedidos">
                    <i class="fa fa-cutlery fa-5x"></i>
                    <h4>Pedidos</h4>
                </a>
            </div>

        </div>

        <!-- EMITIR LISTA DE COMPRAS -->
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
            <div class="div-square">
                <a href="/admin/lista">
                    <i class="fa fa-clipboard fa-5x"></i>
                    <h4>Lista de Compras</h4>
                </a>
            </div>


        </div>

    </div>

    <!-- /. ROW  ICOONS 2 LINHA -->
    <div class="row text-center pad-top index">

        <!-- RECLAMA��ES/SUGEST�ES -->
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
            <div class="div-square">
                <a href="/admin/reclamacoes">
                    <i class="fa fa-envelope-o fa-5x"></i>
                    <h4>Reclamções</h4>
                </a>
            </div>
        </div>

        <!--ADMINISTRA��O-->
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
            <div class="div-square">
                <a href="/admin/admin">
                    <i class="fa fa-cogs fa-5x"></i>
                    <h4>Admin </h4>
                </a>
            </div>
        </div>



    </div>

   
			

	

    <script src='https://code.jquery.com/jquery-2.2.4.js'></script>
    <script>
        $('.main-menu li.active-link').removeClass('active-link');
        $('#indexs').addClass("active-link");
        document.getElementById("introducao").innerHTML = "BEM-VINDO ADMINISTRADOR!";
        document.getElementById("introducao2").innerHTML = "FAÇA TODA GESTÃO DE ELOI'OS";

    </script>

    @endsection