@extends('UI_Admin.base') @section('title') Reclamações @endsection @section('css') @endsection @section('content')
<div class="col-lg-12 col-md12">
<div class="col-sm-12">
            @if($sucesso==1)
                    <div class="alert alert-success alert-dismissable" id="success-alert" style="margin-top:30px">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>Sugestão eliminada com sucesso!</strong> 
                    </div>
            @endif
       </div>
	<h2>Todos as Sugestões/Reclamações</h2>

	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>

					<th>Nome</th>
					<th>E-mail</th>
					<th>Telemovel</th>
					<th>Estado</th>

				</tr>
			</thead>
			<tbody>
				@forelse($reclamacoes as $reclamacao) @if($reclamacao->status==1)
				<tr class="info">
					@else
					<tr class="danger">
						@endif

						<td>{{ $reclamacao->name }}</td>
						<td>{{ $reclamacao->email}}</td>
						<td>{{ $reclamacao->phone}}</td>
						@if($reclamacao->status==1)

						<td> <a href="/admin/reclamacoes/estado/{{$reclamacao->id}}"><i class="fa fa-check centrado i"  title="Carregue para Marcar como Lida"></i> Lida</a></td>

						@else
						<td><a href="/admin/reclamacoes/estado/{{$reclamacao->id}}"><i class="fa fa-times centrado i" title="Carregue para Marcar como não lida"></i>Por Ler</a></td>
						@endif

						<td class="direita">
							<div class="inline">

								{{ csrf_field() }}
								<button type="submit" class="btn btn-default i" title="Ver Mensagem" data-toggle="modal" data-target="#myModal_reclamacao"
								 onclick="openModal({{ $reclamacao }})">
                                    <i class="fa fa-search" ></i>
                                 </button>

							</div>
							<div class="inline">
								<form method="post" action="/admin/reclamacoes/{{ $reclamacao->id}}">
									{{ csrf_field() }} {{ method_field('DELETE') }}

									<button type="submit" class="btn btn-danger" title="Eliminar">
                                                                    <i class="fa fa-trash" ></i>
                                                                </button>
								</form>
							</div>
						</td>
					</tr>


					@empty
					<div class="alert alert-danger alert-dismissable centrado">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> NÃO EXISTEM RECALAMAÇÕES/SUGESTÕES
						PARA APRESENTAR
					</div>

					@endforelse
			</tbody>

		</table>


		<div style="text-align:center;"> {{$reclamacoes->links()}} </div>
	</div>

</div>

<div class="  modal fade" id="myModal_reclamacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_reclamacao">
	<div class="row modal-dialog" role="document">
		<div class="panel panel-primary">
			<div class="panel-heading text-center">
				<label id="txtName" class="upper"></label>
				<button class="close" data-dismiss="modal" aria-label="Close"><span style="color:white;" aria-hidden="true">&times;</span></button>
			</div>

			<div class="panel-body">
				<label>Mensagem:</label>
				<p id="txtMessage" class="upper text-center"></p>
			</div>
			<div class="panel-footer text-center">
				<div class="row">
					<div class=" col-xs-6">
						<p>Email:</p>
						<label id="txtEmail"></label>
					</div>
					<div class=" col-xs-6">
						<p>Telefone:</p>
						<label id="txtPhone"></label>
					</div>
				</div>
			</div>

		</div>

	</div>




	<script src='https://code.jquery.com/jquery-2.2.4.js'></script>
	<script>
	$("#success-alert").fadeTo(2000, 1000).slideUp(1000, function(){
    $("#success-alert").alert('close');
});
		$('.main-menu li.active-link').removeClass('active-link');
        $('#utilizadores').addClass("active-link");
        document.getElementById("introducao").innerHTML = "VEJA TODAS AS SUGESTÕES POR PARTE DOS UTILIZADORES";

        function openModal(obj){
            document.getElementById("txtName").innerHTML =obj.name;
            document.getElementById("txtMessage").innerHTML =obj.message;
            document.getElementById("txtEmail").innerHTML =obj.email;
            document.getElementById("txtPhone").innerHTML =obj.phone;
             if(obj.status==1)
             {
                $('#se').addClass("hidden"); 
                
             }

        }

	</script>






	@endsection