@extends('UI_Admin.base')
 
 @section('title')
Gerir Utilizadores
 @endsection

 @section('css')

  
 @endsection
 
 @section('content')

 <div class="col-lg-12 col-md12">
        <h2>Todos os Utilizadores</h2>
        
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                       
                                        <th>Nome</th>
                                        <th>E-mail</th>
										<th>Telemovel</th>
                                        <th>Estado</th>
                                        
                                    </tr>
                                </thead>
                                    <tbody>
                                        @forelse($users as $user)
                                         @if($user->status==1)
                                        <tr class="info">
                                        @else 
                                         <tr class="danger">
                                         @endif
                                           
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email}}</td>
                                            <td>{{ $user->phone}}</td>
                                                @if($user->status==1)
                                            <td> <a href="/admin/utilizadores/ban/{{$user->id}}"><i class="fa fa-check centrado" title="Carregue para Bloquear"></i>  </a></td>
                                            
                                                @else 
                                            <td><a href="/admin/utilizadores/ban/{{$user->id}}"> <i class="fa fa-ban  centrado" title="Carregue para Desbloquear"> </i></a></td>
                                                @endif
                                            <td class="direita">
                                                <form method="post" action="/admin/utilizadores/{{ $user->id}}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    
                                                    <button type="submit" class="btn btn-danger" title="Eliminar">
                                                        <i class="fa fa-trash" ></i>
                                                    </button>
                                                </form> 
                                            </td>
                                        </tr>
                                        @empty
                                            <div class="alert alert-danger alert-dismissable centrado">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
                                                NÃO EXISTEM UTILIZADORES  PARA APRESENTAR
                                            </div>
                    
                                         @endforelse
                                    </tbody>
                                                
                            </table>
                        <div style="text-align:center;">  {{$users->links()}} </div>
                        </div>
                      
 </div>


  

    



    <script src='https://code.jquery.com/jquery-2.2.4.js'></script>
    <script>
        $('.main-menu li.active-link').removeClass('active-link');
        $('#utilizadores').addClass("active-link");
        document.getElementById("introducao").innerHTML = "FAÇAA A GESTÃO DE TODOS OS UTILIZADORES";
        document.getElementById("introducao2").innerHTML = " AQUI PODER VISUALIZAR / BLOQUEAR / ELIMINAR UTILIZADORES";

    </script>

	@endsection