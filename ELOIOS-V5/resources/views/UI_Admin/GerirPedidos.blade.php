@extends('UI_Admin.base')
 
 @section('title')
 Gerir Pedidos
 @endsection
 
 @section('content')
  <ul class="nav nav-tabs">
    <li class="active"><a href="#abertos" data-toggle="tab">Pedidos Abertos</a></li>
    <li><a href="#cozinha" data-toggle="tab">Pedidos na Cozinha</a></li>
    <li><a href="#fechados" data-toggle="tab">Pedidos Fechados</a></li>
  </ul>
<div class="tab-content">
  <div id="abertos" class="tab-pane fade active in">
    <h3 class="text-center"><strong>Todas os Pedidos Abertos</strong></h3>
     @if($sucessodel==1)
        <div class="alert alert-success alert-dismissable" id="success-alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Successo!</strong> Pedido eliminado com sucesso.
        </div>
    @endif
     <!-- CONTEUDO -->
                            <table class="table">
                                <thead>
                                    <tr >
                                        <th>Nr Pedido</th>
                                       <th>Mesa</th>
                                       <th>Funcionário</th>
                                       <th>Data e Hora</th>
                                       <th>Detalhes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                      @forelse($pedidosNoFuncionario as $pedido)
                                    <tr class="info">    
                                        <td>{{ $pedido->id }}</td>
                                        <td>{{$pedido->table->name}}</td>
                                        <td>{{ $pedido->user->name }}</td>
                                        <td>{{$pedido->datehour}}</td>
                                        <td><a href="/admin/pedidos/{{$pedido->id}}">Detalhes do Pedido</a></td>
                                         <td class="direita">
                                                <form method="post" action="/admin/pedidos/{{$pedido->id}}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    
                                                    <button type="submit" class="btn btn-danger" title="Eliminar">
                                                        <i class="fa fa-trash" ></i>
                                                    </button>
                                                </form> 
                                            </td>
                                    </tr>
                                
                                 @empty
                              <div class="alert alert-danger alert-dismissable centrado">
                                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
                                 NÃO EXISTEM PEDIDOS PARA APRESENTAR
                                </div>
      
                               @endforelse
                                </tbody>
                            </table>
  </div>

  <div id="cozinha" class="tab-pane fade">
    <h3 class="text-center"><strong>Todos os Pedidos na Cozinha</strong></h3>
    <table class="table">
                                <thead>
                                    <tr >
                                        <th>Nr Pedido</th>
                                       <th>Mesa</th>
                                       <th>Funcionário</th>
                                       <th>Data e Hora</th>
                                       <th>Detalhes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                      @forelse($pedidosNaCozinha as $pedido)
                                    <tr class="info">    
                                        <td>{{ $pedido->id }}</td>
                                        <td>{{$pedido->table->name}}</td>
                                        <td>{{ $pedido->user->name }}</td>
                                        <td>{{$pedido->datehour}}</td>
                                        <td><a href="/admin/pedidos/{{$pedido->id}}">Detalhes do Pedido</a></td>
                                         <td class="direita">
                                                <form method="post" action="/admin/pedidos/{{$pedido->id}}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    
                                                    <button type="submit" class="btn btn-danger" title="Eliminar">
                                                        <i class="fa fa-trash" ></i>
                                                    </button>
                                                </form> 
                                            </td>
                                    </tr>
                                
                                 @empty
                              <div class="alert alert-danger alert-dismissable centrado">
                                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
                                 NÃO EXISTEM PEDIDOS PARA APRESENTAR
                                </div>
      
                               @endforelse
                                </tbody>
                            </table>
  </div>

  <div id="fechados" class="tab-pane fade">
    <h3 class="text-center"><strong>Todos os Pedidos Fechados</strong></h3>
  <table class="table">
                                <thead>
                                    <tr >
                                        <th>Nr Pedido</th>
                                       <th>Mesa</th>
                                       <th>Funcionário</th>
                                       <th>Data e Hora</th>
                                       <th>Detalhes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                      @forelse($pedidosFechados as $pedido)
                                    <tr class="info">    
                                        <td>{{ $pedido->id }}</td>
                                        <td>{{$pedido->table->name}}</td>
                                        <td>{{ $pedido->user->name }}</td>
                                        <td>{{$pedido->datehour}}</td>
                                        <td><a href="/admin/pedidos/{{$pedido->id}}">Detalhes do Pedido</a></td>
                                         <td class="direita">
                                                <form method="post" action="/admin/pedidos/{{$pedido->id}}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    
                                                    <button type="submit" class="btn btn-danger" title="Eliminar">
                                                        <i class="fa fa-trash" ></i>
                                                    </button>
                                                </form> 
                                            </td>
                                    </tr>
                                
                                 @empty
                              <div class="alert alert-danger alert-dismissable centrado">
                                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
                                 NÃO EXISTEM PEDIDOS PARA APRESENTAR
                                </div>
      
                               @endforelse
                                </tbody>
                            </table>
  </div>
</div>
         
    <script src='https://code.jquery.com/jquery-2.2.4.js' ></script>
    <script>
        $('.main-menu li.active-link').removeClass('active-link');
        $('#pedidos').addClass("active-link");
        document.getElementById("introducao").innerHTML = "FAÇA A GESTÃO DE TODOS OS PEDIDOS";
 $("#success-alert").fadeTo(2000, 1000).slideUp(1000, function(){
    $("#success-alert").alert('close');
});
    </script>

	@endsection