<!DOCTYPE html>
<html lang="pt">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/icon.ico">


    <title>@yield('title')</title>

    <!-- Bootstrap Core CSS -->
    <link href="/ui_admin_css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/ui_admin_css/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/ui_admin_css/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/ui_admin_css/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

   
@yield('script')

@yield('css')

<style>

.color{background-color:"#333333"}
    .i {
            margin-right: 10px;
        }
.upper{text-transform: uppercase;}   
.inline{display:inline-block}
.index{margin-top:30px;} 
.direita{text-align:right;
		margin-right:5x;}

       .visibility{visibility: visible; } 

        .centrado{
            text-align:center;
            font-size:15px;
        }

        .bordas{
            border-radius: 5px;
        }

</style>
</head>


<body>

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
   
            <ul class="nav navbar-top-links navbar-right">
             
                @if (Auth::user())
            
                                    <a href="/" class="navbar-brand i"  data-toggle="dropdown" role="button" aria-expanded="false"> 
                                       <i class="fa fa-user i"></i>    {{ Auth::user()->name }} <span class="caret"></span>
                                       </a>
       
                                       <ul class="dropdown-menu" role="menu">
                                           <li>
                                               <a href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                                   Terminar Sessão
                                               </a>
       
                                               <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                   {{ csrf_field() }}
                                               </form>
                                           </li>
                                       </ul>
                            
                @endif 
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                    </button>  
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                      

                        <li id="indexs">
                            <a href="/home"><i class="fa fa-desktop i "></i>Iníco<span class="badge"></span></a>
                        </li>
                        <li id="reservas">
                            <a href="/admin/reservas"><i class="fa fa-calendar i"></i>Reservas <span class="badge"></span></a>
                        </li>

                        <li id="utilizadores">
                            <a href="/admin/utilizadores"><i class="fa fa-users i"></i>Utilizadores <span class="badge"></span></a>
                        </li>

                        <li id="funcionarios">
                            <a href="/admin/funcionarios"><i class="fa fa-user i"></i>Funcionários <span class="badge"></span></a>
                        </li>

                        <li id="produtos">
                            <a href="/admin/produtos"><i class="fa fa-glass i"></i>Produtos <span class="badge"></span></a>
                        </li>

                        <li id="pedidos">
                            <a href="/admin/pedidos"><i class="fa fa-cutlery i"></i>Pedidos <span class="badge"></span></a>
                        </li>

                        <li id="listaCompras">
                            <a href="/admin/lista"><i class="fa fa-clipboard i"></i>Lista de Compras<span class="badge"></span></a>
                        </li>

                        <li id="reclamacoes">
                            <a href="/admin/reclamacoes"><i class="fa  fa-envelope i"></i>Reclamações/Sugestões <span class="badge"></span></a>
                        </li>
                        <li id="site">
                            <a href="/"><i class="fa  fa-globe i"></i>Acesso ao Site <span class="badge"></span></a>
                        </li>

                    </ul>
                       
                              
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div id="page-inner" >
                <div class="row">
                    <div class="col-lg-12">
                        <h2>PAINEL ADMINISTRATIVO </h2>
							
					</div>

                </div>
                <!-- /. ROW  -->
                <hr />
                <div class="row">

                    <div class="col-lg-12 ">
                        <div class="alert alert-info">

                            <h4><strong id="introducao"></strong>
                                <Label Style="margin-left: 20px;" ID="introducao2" runat="server" Text=""></Label>
                            </h4>
                        </div>
                        <hr>
						   
                    </div>
                </div>
		    </div>
           
            @yield('content')  

        </div>


<!-- jQuery -->
    <script src="/ui_admin_css/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/ui_admin_css/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/ui_admin_css/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="/ui_admin_css/dist/js/sb-admin-2.js"></script>

    

</body>
</html>