@extends('UI_Admin.base')
 
 @section('title')
Gerir Produtos
 @endsection

 @section('css')

 @endsection

 @section('content')

    <!-- MenuStrip de familias de produtos -->
    <ul class="nav nav-tabs">
        <li class="active" id="bebida"><a href="#bebidas" data-toggle="tab">BEBIDAS</a>
        </li>
        <li id="entrada"><a href="#entradas" data-toggle="tab">ENTRADAS</a>
        </li>
        <li id="menu"><a href="#menus" data-toggle="tab">MENUS</a>
        </li>
        <li id="sobremesa"><a href="#sobremesas" data-toggle="tab">SOBREMESAS</a>
        </li>
        <li id="diverso"><a href="#diversos" data-toggle="tab">DIVERSOS</a>
        </li>
    </ul>


    <!-- Contudo das Familias -->
    <div class="tab-content">
        <!-- BEBIDAS -->
        <div class="tab-pane fade active in" id="bebidas">
            @include('UI_Admin.Produtos.bebidas')  
        </div>

        <!--PRATOS -->
        <div class="tab-pane fade" id="menus">
             @include('UI_Admin.Produtos.menus')  
        </div>
       
        <!--ENTRADAS -->
        <div class="tab-pane fade" id="entradas">
             @include('UI_Admin.Produtos.entradas')  
           

        </div>


        <!--SOBREMESAS-->
        <div class="tab-pane fade" id="sobremesas">
         @include('UI_Admin.Produtos.sobremesas') 

        </div>

        <!-- DIVERSOS -->
        <div class="tab-pane fade" id="diversos">
          @include('UI_Admin.Produtos.diversos') 

        </div>


    </div>

    <script src='https://code.jquery.com/jquery-2.2.4.js'></script>
    <script>
       
       
        $('#produtos').addClass("active-link");
        document.getElementById("introducao").innerHTML = "GERIR PRODUTOS";
        document.getElementById("introducao2").innerHTML = "AQUI PODE VISUALIZAR TODOS OS PRODUTOS ASSIM COMO ADICIONAR E REMOVE-LOS";

       // function clicado() { $('adicionar_bebida').removeClass('hidden'); };
        var hash = window.location.href.substring(window.location.href.indexOf("#")+1);
        if(hash!=window.location.href){
         $('#bebida').removeClass('active');
         $('#bebidas').removeClass('active');
         var str1= '#';
         var str2 = hash;
         var res = str1.concat(str2);
        $(res).addClass('active');
        $(res+'s').addClass('active in');
        
        }
       

    </script>

 @endsection