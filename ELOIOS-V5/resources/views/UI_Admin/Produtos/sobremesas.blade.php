 <h3 ><strong>Todas as Sobremesas</strong></h3>
</br>
                               <!-- CONTEUDO -->

                         
                            <table class="table">
                                <thead>
                                    <tr >
                                        <th>Nome</th>
                                      
                                        <th>Quantidade de Stock</th>
                                        <th>Quantidade Mínima</th>
                                        

                                    </tr>
                                </thead>
                                <tbody>
                                      @forelse($sobremesas as $sobremesa)
                                    <tr class="info">    
                                        <td>{{ $sobremesa-> name }}</td>
                                      
                                         <td>{{ $sobremesa->quantityavailable }}</td>
                                         <td>{{ $sobremesa->quantitymin }}</td>
                                        

                                         <td class=" row direita">
                                                   
                                                    <button type="submit" class="btn btn-default col-xs-4  i"  onclick="openModals({{ $sobremesa }})" title="Adicionar Stock" data-toggle="modal" data-target="#myModal_sobremesa_stock">
                                                        <i class="fa fa-plus" ></i>
                                                    </button>
                                               
                                                <form method="post" action="/admin/sobremesas/{{$sobremesa->id}}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    
                                                    <button type="submit" class="btn btn-danger " title="Eliminar">
                                                        <i class="fa fa-trash" ></i>
                                                    </button>
                                                </form> 
                                                
                                            </td>
                                    </tr>
                                
                                 @empty
                              <div class="alert alert-danger alert-dismissable centrado">
                                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
                                 NÃO EXISTEM SSOBREMESAS  PARA APRESENTAR
                                </div>
      
                               @endforelse
                                </tbody>
                            </table>
                                   
 
         <!-- BOTÕES ADICIONAR E REMOVER ENTRADAS -->
            <div class="text-center">
                <button type="button" class="btn btn-success btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal_sobremesa">
                    <i class="fa fa-plus-circle i"></i>Adicionar Sobremesa
                </button>
                
            </div>
<form class="form-horizontal" method="POST" action="/admin/sobremesas">
{{ csrf_field()}}
            <!-- Modal INSERIR PRODUTO -->
            <div style="width: auto" class="modal fade" id="myModal_sobremesa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_sobremesa">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center" id="myModalLabel_entrada">INSERIR SOBREMESAS</h4>
                        </div>

                        <!-- CORPO DO MODAL -->
                        <div class="modal-body">
                            <div class="row">

                                <div class="col-lg-12 col-md-6">
                                    <div class="form-group">
                                        <label>Nome da sobremesa</label>
                                        <input type="text" class="form-control txt" name="name" required Placholder="Nome">
                                        <p class="help-block">Ex.: Mousse de Chocolate</p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>Qtd. Existente de Stock em Unidades</label>
                                        <input type="text" class="form-control txt" name="qtd_existente" required Placholder="qtd_existente">
                                        <p class="help-block">Ex.: 48</p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>Qtd. Minima de Stock em Unidades</label>
                                         <input type="text" class="form-control txt" name="qtd_min" required Placholder="qtd_min">
                                        <p class="help-block">Ex.: 24</p>
                                    </div>
                                </div>
                            </div>


                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary" runat="server">Inserir Sobremesa</button>
                        </div>

                    </div>
                    </form>
                </div>
            </div>

            

<!-- MODAL ALTERAR STICKsobremesas -->
            <div style="width: auto" class="modal fade" id="myModal_sobremesa_stock" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_sobremesa_stock">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center" id="myModalLabel_entrada">INSERIR STOCK SOBREMESAS</h4>
                        </div>

                        <!-- CORPO DO MODAL -->
                        <div class="modal-body">
                            <div class="row">
                            
                              
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                    <form class="form-horizontal" method="POST" action="/admin/sobremesas/{{$sobremesa->id}}">
                                            {{ csrf_field()}}
                                            {{ method_field('PUT') }}
                                    <input type="text" name="id" hidden id="idsob" required >
                                            
                                        <label>Qtd. a adicionar (em Unidades)</label>
                                        <input type="text" class="form-control txt" name="qtd" required Placholder="qtd_existente">
                                        <p class="help-block">Ex.: 48</p>
                                    </div>
                                </div>
                                
                            </div>


                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary" runat="server">Alterar Sobremesa</button>
                        </div>

                    </div>
                    </form>
                </div>
            </div>
            

<script src='https://code.jquery.com/jquery-2.2.4.js'></script>
	<script>
	

        function openModals(obj){
            
            document.getElementById("idsob").value =obj.id;
           
           

        }  

	</script>