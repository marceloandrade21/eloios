 <h3 ><strong>Todos as Entradas</strong></h3>
</br>
                               <!-- CONTEUDO -->

                         
                            <table class="table">
                                <thead>
                                    <tr >
                                        <th>Nome</th>
                                                     
                                    </tr>
                                </thead>
                                <tbody>
                                      @forelse($entradas as $entrada)
                                    <tr class="info">    
                                        <td>{{ $entrada->name }}</td>       
                                        
                                        
                                         <td class="direita">
                                                <form method="post" action="/admin/entradas/{{$entrada->id}}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    
                                                    <button type="submit" class="btn btn-danger" title="Eliminar">
                                                        <i class="fa fa-trash" ></i>
                                                    </button>
                                                </form> 
                                            </td>
                                    </tr>
                                
                                 @empty
                              <div class="alert alert-danger alert-dismissable centrado">
                                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
                                 NÃO EXISTEM ENTRADAS PARA APRESENTAR
                                </div>
      
                               @endforelse
                                </tbody>
                            </table>

                                                    
         <!-- BOTÕES ADICIONAR E REMOVER ENTRADAS -->
            <div class="text-center">
                <button type="button" class="btn btn-success btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal_entradas">
                    <i class="fa fa-plus-circle i"></i>Adicionar Entradas
                </button>
                
            </div>

            <!-- Modal INSERIR PRODUTO -->
            <div style="width: auto" class="modal fade" id="myModal_entradas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_entradas">
                <div class="modal-dialog" role="document">
                    <div class="modal-content ">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center" id="myModalLabel_entrada">INSERIR ENTRADAS</h4>
                        </div>

                        <!-- CORPO DO MODAL -->
                        <form class="form-horizontal" method="POST" action="/admin/entradas">
                            {{ csrf_field()}}
                            <div class="modal-body ">
                                <div class="row">

                                    <div class="col-xs-8">
                                        <div class="form-group">
                                            <label>Nome do Produto</label>
                                            <input type="text" class="form-control txt" name="name" required>
                                            <p class="help-block">Ex.: Bolinhos de Bacalhau</p>
                                        </div>
                                    </div>
                                    
                                </div>


                            </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <input type="submit" class="btn btn-primary" value="Gravar">
                        </div>
                    </form>
                    </div>
                    
                </div>
            </div>

            
