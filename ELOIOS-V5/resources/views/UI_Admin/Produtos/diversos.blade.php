 <h3 ><strong>Todos os Diversos</strong></h3>
</br>
                               <!-- CONTEUDO -->

                         
                            <table class="table">
                                <thead>
                                    <tr >
                                        <th>Nome</th>
                                      
                                        <th>Quantidade de Stock</th>
                                        <th>Quantidade Mínima</th>
                                        

                                    </tr>
                                </thead>
                                <tbody>
                                      @forelse($diversos as $diverso)
                                    <tr class="info">    
                                        <td>{{ $diverso-> name }}</td>       
                                         <td>{{ $diverso->quantityavailable }}</td>
                                         <td>{{ $diverso->quantitymin }}</td>
                                          
                                         <td class="row direita">
                                      
                                         <button type="submit" class="btn btn-default col-xs-3 i"  onclick="openModal2({{ $diverso }})" title="Remover Stock" data-toggle="modal" data-target="#myModal_diversos_rm_stock">
                                                        <i class="fa fa-minus" ></i>
                                                    </button>
                                            <button type="submit" class="btn btn-default col-xs-3  i"  onclick="openModal({{ $diverso }})" title="Adicionar Stock" data-toggle="modal" data-target="#myModal_diversos_add_stock">
                                                        <i class="fa fa-plus" ></i>
                                                    </button>
                                                <form method="post" action="/admin/diversos/{{$diverso->id}}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    
                                                    <button type="submit" class="btn btn-danger " title="Eliminar">
                                                        <i class="fa fa-trash" ></i>
                                                    </button>
                                                </form> 
                                                
                                            </td>
                                    </tr>
                                
                                 @empty
                              <div class="alert alert-danger alert-dismissable centrado">
                                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
                                 NÃO EXISTEM DIVERSOS PARA APRESENTAR
                                </div>
      
                               @endforelse
                                </tbody>
                            </table>

                                                    
         <!-- BOTÕES ADICIONAR -->
            <div class="text-center">
                <button type="button" class="btn btn-success btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal_diverso">
                    <i class="fa fa-plus-circle i"></i>Adicionar Diverso
                </button>
                
            </div>

            <!-- Modal INSERIR PRODUTO -->
            <div style="width: auto" class="modal fade" id="myModal_diverso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_diverso">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center" id="myModalLabel_entrada">INSERIR DIVERSOS</h4>
                        </div>

                        <!-- CORPO DO MODAL -->
                        <form class="form-horizontal" method="POST" action="/admin/diversos">
                            {{ csrf_field()}}
                            <div class="modal-body">
                                <div class="row">

                                    <div class="col-lg-12 col-md-6">
                                        <div class="form-group">
                                            <label>Nome do Produto</label>
                                            <input type="text" class="form-control txt" name="name" required>
                                            <p class="help-block">Ex.: Garrafa de Oleo</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>Qtd. Existente de Stock em Unidades</label>
                                            <input type="text" class="form-control txt" name="quantityavailable" required>
                                            <p class="help-block">Ex.: 48</p>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>Qtd. Minima de Stock em Unidades</label>
                                            <input type="text" class="form-control txt" name="quantitymin" required>
                                            <p class="help-block">Ex.: 24</p>
                                        </div>
                                    </div>
                                </div>


                            </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <input type="submit" class="btn btn-primary" value="Gravar">
                        </div>
                    </form>
                    </div>
                    
                </div>
            </div>

     <!-- MODAL ADD STOCK  -->
            <div style="width: auto" class="modal fade" id="myModal_diversos_add_stock" tabindex="-1" role="dialog" aria-labelledby="myModal_diversos_add_stock">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center" id="myModalLabel_entrada">INSERIR STOCK DE DIVERSOS</h4>
                        </div>

                        <!-- CORPO DO MODAL -->
                        <div class="modal-body">
                            <div class="row">
                            
    
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                    <form class="form-horizontal" method="POST" action="/admin/diversos/{{$diverso->id}}">
                                            {{ csrf_field()}}
                                            {{ method_field('PUT') }}
                                    <input type="text" name="id" hidden id="iddiverso" required >
                                            
                                        <label>Qtd. a adicionar (em Unidades)</label>
                                        <input type="text" class="form-control txt" name="qtd" required Placholder="qtd_existente">
                                        <p class="help-block">Ex.: 48</p>
                                    </div>
                                </div>
                                
                            </div>


                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary" runat="server">Alterar Diverso</button>
                        </div>

                    </div>
                    </form>
                </div>
            </div>
            

     <!-- MODAL ADD STOCK  -->
            <div style="width: auto" class="modal fade" id="myModal_diversos_rm_stock" tabindex="-1" role="dialog" aria-labelledby="myModal_diversos_rm_stock">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center" id="myModalLabel_entrada">REMOVER STOCK DE DIVERSOS</h4>
                        </div>

                        <!-- CORPO DO MODAL -->
                        <div class="modal-body">
                            <div class="row">
                            
                              
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                    <form class="form-horizontal" method="POST" action="/admin/diversos/rm">
                                            {{ csrf_field()}}
                                            {{ method_field('PUT') }}
                                    <input type="text" name="id" hidden id="iddiverso2" required >
                                            
                                        <label>Qtd. a adicionar (em Unidades)</label>
                                        <input type="text" class="form-control txt" name="qtd" required Placholder="qtd_existente">
                                        <p class="help-block">Ex.: 48</p>
                                    </div>
                                </div>
                                
                            </div>


                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary" runat="server">Alterar Diverso</button>
                        </div>

                    </div>
                    </form>
                </div>
            </div>
    
<script src='https://code.jquery.com/jquery-2.2.4.js'></script>
	<script>
	

        function openModal(obj){
            
            document.getElementById("iddiverso").value =obj.id;
        }  

           function openModal2(obj){
            
            document.getElementById("iddiverso2").value =obj.id;
        }  

	</script>