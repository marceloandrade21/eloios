
 <h3 ><strong>Todas as Bebidas</strong></h3>
</br>
                               <!-- CONTEUDO -->

                         
                            <table class="table">
                                <thead>
                                    <tr >
                                        <th>Nome</th>
                                        <th>Tipo de Bebida</th>
                                        <th>Quantidade de Stock</th>
                                        <th>Quantidade Mínima</th>
                                        

                                    </tr>
                                </thead>
                                <tbody>
                                      @forelse($drinks as $drink)
                                    <tr class="info">    
                                        <td>{{ $drink-> name }}</td>
                                        <td>{{ $drink->type_drink->name }}</td>
                                         <td>{{ $drink->quantityavailable }}</td>
                                         <td>{{ $drink->quantitymin }}</td>
                                         <div class="row col-xs-12"> 
                                         <td class="direita">
                                       
                                                <button type="submit" class="btn btn-default col-xs-4 i"  onclick="openModalb({{ $drink }})" title="Adicionar Stock" data-toggle="modal" data-target="#myModal_bebida_stock">
                                                        <i class="fa fa-plus" ></i>
                                                    </button>
                                                

                                                <form method="post" action="/admin/bebidas/{{$drink->id}}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    
                                                    <button type="submit" class="btn btn-danger" title="Eliminar">
                                                        <i class="fa fa-trash" ></i>
                                                    </button>
                                                </form> 
                                           
                                            </td>
                                             </div>
                                    </tr>
                                
                                 @empty
                              <div class="alert alert-danger alert-dismissable centrado">
                                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
                                 NÃO EXISTEM BEBIDAS PARA APRESENTAR
                                </div>
      
                               @endforelse
                                </tbody>
                            </table>
                             <div class=" centrado">
                                {{ $drinks->links() }}
 
                                </div>




                                
                                    
         <!-- BOTÕES ADICIONAR E REMOVER ENTRADAS -->
            <div class="text-center">
                <button type="button" class="btn btn-success btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal_entrada">
                    <i class="fa fa-plus-circle i"></i>Adicionar Bebida
                </button>
                
            </div>
<form class="form-horizontal" method="POST" action="/admin/bebidas">
{{ csrf_field()}}
            <!-- Modal INSERIR PRODUTO -->
            <div style="width: auto" class="modal fade" id="myModal_entrada" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_entrada">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center" id="myModalLabel_entrada">INSERIR BEBIDA</h4>
                        </div>

                        <!-- CORPO DO MODAL -->
                        <div class="modal-body">
                            <div class="row">

                                <div class="col-lg-12 col-md-6">
                                    <div class="form-group">
                                        <label>Nome da bebida</label>
                                        <input type="text" class="form-control txt" name="name" required Placholder="Nome">
                                        <p class="help-block">Ex.: coca-cola</p>
                                    </div>
                                </div>
                                <div class="col-sm-6">    
                                <label>Tipo de Bebida</label>
                                <select class="form-control" name="drinkType" >
                                        @forelse($tipos as $tipo)
                                                        
                                        <option  value="{{ $tipo->id }}">{{ $tipo->name }}</option>
                                                        

                                    @empty
                                    <option> NÃO EXISTEM TIPOS DE BEBIDA </option>
                                                        @endforelse
                                </select>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>Qtd. Existente de Stock em Unidades</label>
                                        <input type="text" class="form-control txt" name="qtd_existente" required Placholder="qtd_existente">
                                        <p class="help-block">Ex.: 48</p>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>Qtd. Minima de Stock em Unidades</label>
                                         <input type="text" class="form-control txt" name="qtd_min" required Placholder="qtd_min">
                                        <p class="help-block">Ex.: 24</p>
                                    </div>
                                </div>
                            </div>


                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary" runat="server">Inserir Bebida</button>
                        </div>

                    </div>
        </form>
                </div>
            </div>


<!-- MODAL ALTERAR STICK BEBEIDAS -->
            <div style="width: auto" class="modal fade" id="myModal_bebida_stock" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_bebida_stock">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center" id="myModalLabel_entrada">INSERIR STOCK DE BEBEIDAS</h4>

                        <!-- CORPO DO MODAL -->
                        <div class="modal-body">
                            <div class="row">

                              
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                    <form class="form-horizontal" method="POST" action="/admin/bebidas/{{$drink->id}}">
                                            {{ csrf_field()}}
                                            {{ method_field('PUT') }}
                                    <input type="text" name="id" hidden id="idbebida" required >
                                            
                                        <label>Qtd. a adicionar (em Unidades)</label>
                                        <input type="text" class="form-control txt" name="qtd" required Placholder="qtd_existente">
                                        <p class="help-block">Ex.: 48</p>
                                    </div>
                                </div>
                                
                            </div>


                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary" runat="server">Alterar Bebida</button>
                        </div>

                    </div>
                    </form>
                </div>
            </div>
             </div>
            
<script src='https://code.jquery.com/jquery-2.2.4.js'></script>
	<script>
	

        function openModalb(obj){
            document.getElementById("idbebida").value =obj.id;
           
           

        }

	</script>