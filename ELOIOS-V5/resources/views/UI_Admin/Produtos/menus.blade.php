
 <h3 ><strong>MENÚS</strong></h3>
 </br>

            <div class="panel-group" id="accordion">

            @foreach($tipospratos as $tipoprato)
                <!--PRATOS -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#{{$tipoprato->name}}" href="#{{$tipoprato->name}}" class="collapsed">{{$tipoprato->name}}</a>
                        </h4>
                    </div>
                    <div id="{{$tipoprato->name}}" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">


                            <!-- CONTEUDO -->

                            <h5> <strong>Todos os Pratos de {{$tipoprato->name}} </strong></h5>
                            </br>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Estado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pratos as $prato)
                                    <tr class="info">
                                        @if($prato->type_dish_id==$tipoprato->id)
                                            <td>{{$prato->name}}</td>
                                            @if($prato->product->menu==1)
                                            <td>
                                                Em Menu.  <a href="/admin/pratos/alteramenu/{{$prato->product_id}}">Retirar do Menu</a>
                                            </td>
                                            @else
                                            <td>
                                                Fora do Menu. <a href="/admin/pratos/alteramenu/{{$prato->product_id}}">Colocar no Menu</a>
                                            </td>
                                            @endif
                                            <td>
                                            <form method="post" action="/admin/pratos/{{$prato->id}}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    
                                                    <button type="submit" class="btn btn-danger" title="Eliminar">
                                                        <i class="fa fa-trash" ></i>
                                                    </button>
                                                </form> 
                                            </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                   
                                </tbody>
                            </table>

                            <!-- BOTÕES ADICIONAR E REMOVER PRATOS -->
                            <div class="text-center">
                                <button type="button" class="btn btn-success btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal_carne">
                                    <i class="fa fa-plus i"></i>Adicionar Prato
                                </button>
                               
                            </div>

                            
                        </div>
                    </div>
                </div>
            @endforeach
                <!-- Modal INSERIR PRODUTO -->
                            <div style="width: auto" class="modal fade" id="myModal_carne" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_carne">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title text-center" id="myModalLabel_carne"><i class="fa fa-glass i"></i>INSERIR PRATO</h4>
                                        </div>

                                        <!-- CORPO DO MODAL -->
                                        <div class="modal-body">
                                            <div class="row">
                                            <form action="/admin/pratos" method="post">
                                                    {{ csrf_field() }}
                                            
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="form-group">
                                                        <label>Nome do Prato</label>
                                                        <input type="text" name="name" class="form-control">
                                                        <p class="help-block">Ex. Francesinha com ovo</p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="form-group">
                                                        <label>Tipo de Prato </label>

                                                      <select class="form-control" name="dishType">
                                                        @foreach($tipospratos as $tipoprato)
                                                            <option value="{{$tipoprato->id}}">{{$tipoprato->name}}</option>
                                                        @endforeach
                                                      </select>

                                                    </div>
                                                </div>
                                               

                                            </div>
                                        </div>


                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                            <input type="submit" class="btn btn-primary" value="Gravar"/>
                                        </div>
                                         </form>

                                    </div>
                                </div>
                            </div>
            </div>


