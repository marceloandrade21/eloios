@extends('UI_Admin.base')
 
 @section('title')
Gerir Reservas
 @endsection

 @section('css')

 @endsection

 @section('content')
 
    <h3 class="text-center"><strong>Todas as Reservas</strong></h3>
    @if($sucessodel==1)
        <div class="alert alert-success alert-dismissable" id="success-alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Successo!</strong> Reserva eliminada com sucesso.
        </div>
    @endif
     @if($sucessoconf==1)
        <div class="alert alert-success alert-dismissable" id="success-alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Successo!</strong> Reserva editada com sucesso.
        </div>
    @endif


</br>
                               <!-- CONTEUDO -->

                         
                            <table class="table">
                                <thead>
                                    <tr >
                                        <th>Nr Reserva</th>
                                        <th>Nome do Utilizador</th>
                                        <th>Contacto</th>
                                        <th>Data e Hora</th>
                                        <th>Número de Pessoas</th>
                                        <th>Estado da Reserva</th>
                                    </tr>
                                </thead>
                                <tbody>
                                      @forelse($reserves as $reserve)
                                        @if($reserve->status==0)
                                        <tr class="danger">
                                        @else <tr class="info">   
                                        @endif 
                                            <td>{{ $reserve->id }}</td>
                                            <td>{{ $reserve->user->name }}</td>
                                            <td>{{$reserve->user->phone}}</td>
                                            <td>{{$reserve->datehour}}</td>
                                            <td>{{$reserve->nr_persons}}</td>
                                        
                                        @if($reserve->status==0)
                                        <td> <a href="/admin/reservas/confirmar/{{$reserve->id}}">Confirmar Reserva</a></td>
                                        @else
                                        
                                        <td style="color:green"><i class="fa fa-check centrado" ></i> Reserva Confirmada</td>
                                        @endif
                                        
                                         <td class="direita">
                                                <form method="post" action="/admin/reservas/{{$reserve->id}}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    
                                                    <button type="submit" class="btn btn-danger" title="Eliminar">
                                                        <i class="fa fa-trash" ></i>
                                                    </button>
                                                </form> 
                                            </td>
                                    </tr>
                                
                                 @empty
                              <div class="alert alert-danger alert-dismissable centrado">
                                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
                                 NÃO EXISTEM RESERVAS PARA APRESENTAR
                                </div>
      
                               @endforelse
                                </tbody>
                            </table>
 

    <script src='https://code.jquery.com/jquery-2.2.4.js'></script>

<script>
         
$("#success-alert").fadeTo(2000, 1000).slideUp(1000, function(){
    $("#success-alert").alert('close');
});
        $('.main-menu li.active-link').removeClass('active-link');
        $('#reservas').addClass("active-link");
        document.getElementById("introducao").innerHTML = "GERIR RESERVAS";
        document.getElementById("introducao2").innerHTML = "AQUI PODE VISUALIZAR TODAS AS RESERVAS ASSIM COMO CONFIRMA-LAS.";

</script>
 
 @endsection