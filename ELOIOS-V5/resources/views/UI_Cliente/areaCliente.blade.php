
@extends('UI_Cliente.base')
 
 @section('title')
Alterar Dados
 @endsection
 

@section('css')
  <link href="/css_loginregistar.css" rel="stylesheet" />


<style>
        .branco {
            color: white;
        }

        .bordas {
            border-radius: 7px;
        }

        .centrado {
            text-align: center;
        }

            .centrado .calen {
                text-align: center;
            }

         MEDIA QUERYS MOBILE REGISTAR
        @media screen and (min-width:320px) {
            #meioR {
                min-height: 100%;
            }
        }

        @media screen and (max-width:800px) {
            #meioR {
                min-height: 100%;
            }
        }

       


        }
    </style> 
    @endsection


    @section('content')
 <div class="col-sm-8 centrado">
            @if($sucesso==1)
                    <div class="alert alert-success alert-dismissable" id="success-alert" style="margin-top:30px">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>DADOS ALTERADOS COM SUCESSO!</strong> 
                    </div>
            @endif
</div>

    <div class="container ">

        <div class="bg-faded p-4 my-4" id="meioR" >
            <div class="card card-inverse" id="meioR" >
                <div class="card-img-overlay bg-overlay col-sm-12 color" id="meioR" >
                    <h3 class="card-title text-shadow text-white text-uppercase mb-0 text-center">AÉREA DE CLIENTE</h3>
                    <br />
                    <div class=" col-sm-8 centrado">
                    <br />
                        <strong style="color: white;" >ALTERE OS SEUS DADOS </strong>
                        <div class="main">
                            <form  method="POST" action="/utilizador/{{Auth::user()->id}}" class="form-horizontal">
                                {{ method_field('PUT') }}
                                        {{ csrf_field() }}
                                            <div class="form-group">
                                                <input type="text"  name="name" required Value="{{Auth::user()->name}}">
                                            </div>
                                            <div class="form-group">
                                                <input type="text"  name="email" required Value="{{Auth::user()->email}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <input type="text"  name="phone" required  value="{{Auth::user()->phone}}">
                                            </div>
                                            
                                            <br />
                                            <input type="submit" value="Guardar Dados">
                            </form>
                                
                                 <input type="submit" class= "mb-0" data-toggle="modal" data-target="#myModal" value="Mudar Password">  
                                
                                    </input>

                                    <!-- Modal -->
                                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                
                                                <h4 class="modal-title text-center" id="myModalLabel">Alterar Password</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" method="POST" action="/utilizador/alterarpwd">
                                                 {{ method_field('PUT') }}
                                                    {{ csrf_field() }}
                                                    <div class="form-group">
                                                        <input id="password" type="password"  name="oldpwd" required PlaceHolder="Password Antiga">
                                                    </div>
                                                    <div class="form-group">
                                                       <input id="password-confirm" type="password" class="form-control" name="newpwd" required PlaceHolder="Nova Password">
                                                    </div>   

                                                    <div class="form-group">
                                                        <input id="password-confirm" type="password" class="form-control" name="confirmpwd" required PlaceHolder="Confirme  Password">
                                                    </div>           
                                                                            
                                                    </div>
                                                
                                                <div class="modal-footer">
                                                    
                                                    <input type="submit" class="btn btn-primary" Value="Mudar Password"></input>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    </div>

                    </div> 
                </div>
            </div>
        </div>

    </div>
 <script src='https://code.jquery.com/jquery-2.2.4.js'></script>
<script>
$("#success-alert").fadeTo(3000, 500).slideUp(500, function(){
    $("#success-alert").alert('close');
  }); 
</script>

 @endsection