@extends('UI_Cliente.base')
 
 @section('title')
 Index
 @endsection
 
 @section('content')
 <div class="container">
 
       <div class="bg-faded p-4 my-4">
         <!-- Image Carousel -->
         <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
           <ol class="carousel-indicators">
             <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
             <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
             <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
           </ol>
           <div class="carousel-inner" role="listbox">
             <div class="carousel-item active">
               <img class="d-block img-fluid w-100 rola" src="img/main2.png"  alt="">
               <div class="carousel-caption d-none d-md-block">
                
               </div>
             </div>
             <div class="carousel-item">
               <img class="d-block img-fluid  w-100 rola" src="img/slide-2.jpg" alt="">
               <div class="carousel-caption d-none d-md-block">
                 
               </div>
             </div>
             <div class="carousel-item">
               <img class="d-block img-fluid w-100 rola" src="img/slide-3.jpg" alt="">
               <div class="carousel-caption d-none d-md-block">
                 
               </div>
             </div> 
           </div>
       
                 <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
             <span class="carousel-control-prev-icon" aria-hidden="true"></span>
             <span class="sr-only">Anterior</span>
           </a>
           <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
             <span class="carousel-control-next-icon" aria-hidden="true"></span>
             <span class="sr-only">próximo</span>
           </a>
         </div>
              
         <!-- Welcome Message -->
         <div class="text-center mt-4">
           <div class="text-heading text-muted text-lg">BEM-VINDO AO</div>
           <h1 class="my-2">E'LOIOS</h1>
 
         </div>
       </div>
 
       <div class="bg-faded p-4 my-4">
         <hr class="divider">
         <h2 class="text-center text-lg text-uppercase my-0">Venha Experimentar
           <strong>As Nossas Especialidades</strong>
         </h2>
         <hr class="divider">
       
        
           <div class="bg-faded p-4 my-4">
                 <hr class="divider">
                     <h2 class="text-center text-lg text-uppercase my-0">
                       <strong>CARNE</strong>
                     </h2>
                  <hr class="divider">
                 <div class="row">
                   <div class="col-md-4 mb-4 mb-md-0">
                     <div class="card w-100">
                       <img class="card-img-top imagem" src="/img/inicio/carne1.jpg" alt="">
                       <div class="card-body text-center">
                         <h4 class="card-title m-0">Carne de Porco Alentejana
                         </h4>
                       </div>
                     </div>
                   </div>
                   <div class="col-md-4 mb-4 mb-md-0">
                     <div class="card w-100">
                       <img class="card-img-top imagem" src="/img/inicio/carne2.jpg" alt="">
                       <div class="card-body text-center">
                         <h4 class="card-title m-0">Arroz de Cabidela
                         </h4>
                       </div>
                     </div>
                   </div>
                   <div class="col-md-4">
                     <div class="card w-100"> 
                       <img class="card-img-top imagem" src="/img/inicio/carne3.jpg" alt="">
                       <div class="card-body text-center">
                         <h4 class="card-title m-0">Leitão
                         </h4>
                       </div>
                     </div>
                   </div>
                 </div>
 
               <br />
              
 
                <hr class="divider">
                     <h2 class="text-center text-lg text-uppercase my-0">
                       <strong>PEIXE</strong>
                     </h2>
                  <hr class="divider">
                 <div class="row">
                    <div class="col-md-4 mb-4 mb-md-0">
                     <div class="card w-100">
                       <img class="card-img-top imagem" src="/img/inicio/peixei1.jpg" alt="">
                       <div class="card-body text-center">
                         <h4 class="card-title m-0">Bacalhau a Brás
                         </h4>
                       </div>
                     </div>
                   </div>
                   <div class="col-md-4 mb-4 mb-md-0">
                     <div class="card w-100">
                       <img class="card-img-top imagem" src="/img/inicio/peixe2.jpg" alt="">
                       <div class="card-body text-center">
                         <h4 class="card-title m-0">Bacalhau com Natas
                         </h4>
                       </div>
                     </div>
                   </div>
                   <div class="col-md-4">
                     <div class="card w-100">
                       <img class="card-img-top imagem" src="/img/inicio/peixe3.jpg" alt="">
                       <div class="card-body text-center">
                         <h4 class="card-title m-0">Sardinhas
                         </h4>
                       </div>
                     </div>
                   </div>
                 </div>
               <br />
 
                <hr class="divider">
                     <h2 class="text-center text-lg text-uppercase my-0">
                       <strong>Sobremesas</strong>
                     </h2>
                  <hr class="divider">
                 <div class="row">
                   <div class="col-md-4 mb-4 mb-md-0">
                     <div class="card w-100">
                       <img class="card-img-top imagem" src="/img/inicio/sobremesa1.jpg" alt="">
                       <div class="card-body text-center">
                         <h4 class="card-title m-0">Molotofe
                         </h4>
                       </div>
                     </div>
                   </div>
                   <div class="col-md-4 mb-4 mb-md-0">
                     <div class="card w-100">
                       <img class="card-img-top imagem" src="/img/inicio/sobremesa2.jpeg" alt="">
                       <div class="card-body text-center">
                         <h4 class="card-title m-0">Bolo de Bolacha
                         </h4>
                       </div>
                     </div>
                   </div>
                   <div class="col-md-4">
                     <div class="card w-100">
                       <img class="card-img-top imagem" src="/img/inicio/sobremesa3.jpg" alt="">
                       <div class="card-body text-center">
                         <h4 class="card-title m-0">CheeseCake
                           </h4>
                       </div>
                     </div>
                   </div>
                 </div>
 
         </div>
 
 
 
       </div>
      
 
      <div class="bg-faded p-4 my-4 text-center ">
           
              <h2 class="text-center text-lg text-uppercase my-0">
                <strong>Ementa </strong>
              </h2>
            <hr class="divider">

            
              </br>
                <div class=" nav navbar-top-links navbar-right"> 
                  
                   @foreach($tipos as $tipo)
                    <div class="col-lg-3 col-sm-12"> 
                    <ul  style=" list-style-type: none; margin:auto;" >
                             <hr class="divider">
                            <li> <strong class="text-uppercase">  {{ $tipo->name}} </strong></li>
                                    <hr class="divider">
                                    
                                              @foreach($pratos as $prato)
                                                @if($prato->product->menu==1 && $prato->type_dish_id == $tipo->id)
                                                    
                                                    <li class="text-uppercase"> {{$prato->name}}</li>
                                                  
                                                @endif
                                                      
                                              @endforeach
                                        
                                          
                    </ul>
                    </br>
                     </div>               
                    @endforeach
                    
                 </div>
 
      </div>
     </div>
    <script src='https://code.jquery.com/jquery-2.2.4.js' ></script>
     <script>
         $('.menu li.active').removeClass('active');
         $('#inicio').addClass("active");
 
     </script>

    @endsection