
@extends('UI_Cliente.base')
 
 @section('title')
Reservas
 @endsection
 

@section('css') 
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<style>

        .branco {
            color: white;
        }

        .bordas {
            border-radius: 7px;
        }

        .centrado {
            text-align: center;
        }

            .centrado .calen {
                text-align: center;
            }

         MEDIA QUERYS MOBILE REGISTAR
        @media screen and (min-width:320px) {
            #meioR {
                min-height: 100%;
            }
        }

        @media screen and (max-width:800px) {
            #meioR {
                min-height: 1000px;
            }
        }

        @media screen and (min-width:800px) {
            #meiOR {
                min-height: auto;
            }


        }
        #lbl{
            color:red;
        }
    </style> 
    @endsection


    @section('content')
 



    <div class="container">
 <div class="col-sm-12">
            @if($sucesso==1)
                    <div class="alert alert-success alert-dismissable" id="success-alert" style="margin-top:30px">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>Pedido de reserva efetuado com sucesso!</strong> 
                    </div>
            @endif
</div>
        <div class="bg-faded p-4 my-4" id="meioR">
            <div class="card card-inverse" id="meioR">
                <img class="card-img img-fluid w-100" id="meio" src="../img/1.jpg" alt="">
                <div class="card-img-overlay bg-overlay col-sm-12" id="meioR">
                    <h3 class="card-title text-shadow text-white text-uppercase mb-0 text-center">Reserve aqui uma mesa e a sua refeição evitando esperas</h3>
                    <br />

                    <br />
                    <form action="/reservas" method="post">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-sm-6 centrado">
                            <img style="width: 100px;" src="../img/mesa.png" alt="">
                            <br />
                            <br />
                            <strong class="branco">Quantas pessoas são?</strong> 
                            
                            <br />
                          
                            <select id="ddl" name="nr_persons" onchange="verselecionado()" class="form-control" required>
                                <option value="">Selecione o Nr de Pessoas</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="11">+9</option>
                                
                            </select>
                            <br />
                            <label id="lbl"></label>

                        </div>

                        <div class="col-sm-6 centrado">
                            <img style="width: 100px;" src="../img/calendario.png" alt="">
                            <br />
                            <br />
                            <strong class="branco">Para quando deseja a sua reserva?</strong>
                            <br />
                           
                                 <div class="text-center">
                                        <input type="date" id="day" name="date" class="form-control min-today" onchange="verdata()" required>
                                        
                                </div>
                                <script>
                                $(function(){
                                    $('[type="date"].min-today').prop('min', function(){
                                        return new Date().toJSON().split('T')[0];
                                    });
                                });
                                </script>
                                
                        <select id="horas" onchange="verhora()" name="hour" class="form-control invisible" required>
                                <option value="">Selecione a Hora:</option>
                                <option value="12:00:00">12:00</option>
                                <option value="13:00:00">13:00</option>
                                <option value="14:00:00">14:00</option>
                                <option value="15:00:00">15:00</option>
                                <option value="16:00:00">16:00</option>
                                <option value="17:00:00">17:00</option>
                                <option value="18:00:00">18:00</option>
                                <option value="19:00:00">19:00</option>
                                <option value="20:00:00">20:00</option>
                                <option value="21:00:00">21:00</option>
                        </select>
                               
                            <input type="submit" id="envia" value="Reservar" class="form-control invisible" />
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>

<!-- SE NÃO TIVER LOGADO REDIRECIONA PARA LOGIN -->

    </div>


<script src='https://code.jquery.com/jquery-2.2.4.js'></script>

<script>
webshims.setOptions('forms-ext', {types: 'date'});
webshims.polyfill('forms forms-ext');

 $('.menu li.active').removeClass('active');
        $('#reserva').addClass("active");

$("#success-alert").fadeTo(3000, 500).slideUp(500, function(){
    $("#success-alert").alert('close');    
});
    function verdata(){
        $("#horas").removeClass("invisible");
         
        
    }
    function verhora(){
         $("#envia").removeClass("invisible");
    }
        function verselecionado() {

            var value = document.getElementById("ddl").options[document.getElementById("ddl").selectedIndex].value;
            if (value == 11) {
                document.getElementById("lbl").innerHTML = 'Para esse valor de pessoas contacte diretamente o restaurante';
            }
            else {
                document.getElementById("lbl").innerHTML = '';
            }
        }
    </script>


 @endsection