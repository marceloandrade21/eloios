@extends('UI_Cliente.base')
 
 @section('title')
 Sobre
 @endsection
 
 @section('content')
 <div class="container">

      <div class="bg-faded p-4 my-4">
        <hr class="divider">
        <h2 class="text-center text-lg text-uppercase my-0">Sobre
          <strong>E'LOIOS</strong>
        </h2>
        <hr class="divider">
        <hr class="divider">
        
        <div class="row">
          <div class="col-lg-6">
            <img class="img-fluid mb-4 mb-lg-0" src="/img/slide-2.jpg" alt="">
          </div>
          <div class="col-lg-6 text-center">
          <br/>
          <p>Somos um restaurante familiar situado na Rua dos Caldeireiros bem no centro da magnifica cidade do Porto.
          Gostamos acima de tudo propocionar ao cliente a melhor experiência na nossa mítica cidade.</p>
            <p>A nossa especialidade é o Leitão.</p>
            <br/>
            <p> Sabias que estamos espalhados pela internet? Visita-nos!</p>
            <p><a href="https://www.facebook.com/eloiospt/" target="_blank"><i id="social-fb" class="fa fa-facebook-square fa-3x social" ></i></a>
            <a href="https://www.tripadvisor.pt/Restaurant_Review-g2618568-d2063961-Reviews-Eloios-Porto_District_Northern_Portugal.html" target="_blank"><i id="social-fb" class="fa fa-tripadvisor fa-3x social" ></i></a></p>
            
          </div>
        </div>
      </div>

      <div class="bg-faded p-4 my-4">
        <hr class="divider">
        <h2 class="text-center text-lg text-uppercase my-0">A Nossa
          <strong>Equipa</strong>
        </h2>
        <hr class="divider">
        
        <div class="row">
          <div class="col-md-4 mb-4 mb-md-0">
            <div class="card">
              <img class="card-img-top imagem" src="/img/empregado1.jpg" alt="">
              <div class="card-body text-center">
                <h4 class="card-title m-0">Ricardo Nogueira
                  <small class="text-muted">Gerente</small>
                </h4>
              </div>
            </div>
          </div>
          <div class="col-md-4 mb-4 mb-md-0">
            <div class="card ">
              <img class="card-img-top imagem"  src="/img/empregado2.png" alt="">
              <div class="card-body text-center">
                <h4 class="card-title m-0">Marcelo Andrade
                  <small class="text-muted">Chefe de Sala</small>
                </h4>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card ">
              <img class="card-img-top imagem"  src="/img/empregado3.jpg" alt="">
              <div class="card-body text-center">
                <h4 class="card-title m-0">André Azevedo
                  <small class="text-muted">Cozinheiro</small>
                </h4>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>


    <script src='https://code.jquery.com/jquery-2.2.4.js' ></script>
    <script>
        $('.menu li.active').removeClass('active');
        $('#sobre').addClass("active");
 
    </script>   
     @endsection