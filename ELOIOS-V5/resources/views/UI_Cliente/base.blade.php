<html>
<head>
    
    
     <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
     <link rel="icon" href="/icon.ico">

    <title>
     @yield('title')
     </title>

     @yield('css')
     <style>
       .i {
            margin-right: 10px;
        }
     </style>


    <!-- Bootstrap core CSS -->
    <link href="/VENDOR/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/business-casual.css" rel="stylesheet" />
    
    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

      


</head>

<body>
     <div class="tagline-upper text-center text-heading text-shadow text-white mt-5 d-none d-lg-block">E'LOIOS</div>
    <div class="tagline-lower text-center text-expanded text-shadow text-uppercase text-white mb-5 d-none d-lg-block">Rua Dos Caldeireiros| BAIXA-PORTO | 229383267</div>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-faded py-lg-4">
	
      <div class="container">
        <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul id="menu" class="navbar-nav mx-auto">
              <li id="inicio" class="nav-item px-lg-4">
                <a class="nav-link text-uppercase text-expanded" href="/" >Início             </a>
              </li>
              <li id="sobre" class="nav-item px-lg-4">
                <a class="nav-link text-uppercase text-expanded" href="/sobre">Sobre Nós</a>
              </li>
            
              <li id="contactos" class="nav-item px-lg-4">
                <a class="nav-link text-uppercase text-expanded" href="/contactos">Contactos</a>
              </li>
              @if (Auth::user())
              <li id="reservas" class="nav-item px-lg-4">
                <a class="nav-link text-uppercase text-expanded" href="/reservas">Reservas</a>
              </li>
                @endif
            
                @if (Auth::user())
            <li class="nav-item px-lg-4 dropdown ">
                                  <a href="/" class="dropdown-toggle nav-link text-uppercase " data-toggle="dropdown"  >
                                    <i class="fa fa-user i" style="color:grey;"></i> {{ Auth::user()->name }} 
                                  </a>

                                  <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a class="nav-link text-center" href="/utilizador" > Alterar Dados</a>
                                      </li>
                                    @if(Auth::user()->utype_id==1)
                                    <li><a class="nav-link text-center" href="/home">Administração</a></li>
                                    @endif
                                     @if(Auth::user()->utype_id==2)
                                    <li><a class="nav-link text-center" href="/verreservas">Ver Reservas</a></li>
                                    @endif
                                      <li>
                                          <a class="nav-link text-center" href="{{ route('logout') }}"
                                              onclick="event.preventDefault();
                                                      document.getElementById('logout-form').submit();">
                                              Terminar Sessão
                                          </a>

                                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                              {{ csrf_field() }}
                                          </form>
                                      </li>
                                      
                                  </ul>
            </li>
            @else
            <li class="nav-item px-lg-4  " style="text-align:right;">
                                  <a href="/login" class=" nav-link text-uppercase "   >
                                    <i class="fa fa-user i" style="color:grey;"></i>Área de Cliente 
                                  </a>

                                
            </li>
            @endif
          </ul>
       
        </div>
      </div>
    </nav>

    
    @yield('content')


     <footer class="bg-faded text-center py-5">
      <div class="container">
        <p class="m-0"> <div class="text-heading text-muted text-lg">
        Copyright &copy;  <strong> André Azevedo | Marcelo Andrade | Ricardo Nogueira </strong>
        </div></p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="/VENDOR/jquery/jquery.min.js"></script>
    <script src="/VENDOR/popper/popper.min.js"></script>
    <script src="/VENDOR/bootstrap/js/bootstrap.min.js"></script>
     

</body>
</html>