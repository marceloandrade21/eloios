@extends('UI_Cliente.base')
 
 @section('title')
Contatcs
 @endsection
 
@section('css')
 <style> 
        .centrado {
           margin-left: auto;
           margin-right:auto;
        }
    </style>
@endsection
@section('content')
<div class="container">
      <div class="bg-faded p-4 my-4">
        <hr class="divider">
        <h2 class="text-center text-lg text-uppercase my-0">
          <strong>Localização</strong>
        </h2>
        <hr class="divider">
        <div class="row">
          <div class="col-lg-8">
            <div class="embed-responsive embed-responsive-16by9 map-container mb-4 mb-lg-0">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3004.5129509980347!2d-8.616091884576178!3d41.145157179287075!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd2464e237685689%3A0x46927716df02e403!2sRua+dos+Caldeireiros%2C+Porto!5e0!3m2!1spt-PT!2spt!4v1505153018871" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
          </div>
          <div class="col-lg-4">
            <h5 class="mb-0">Telefone:</h5>
            <div class="mb-4">229383267</div>
            <h5 class="mb-0">Email:</h5>
            <div class="mb-4">
              <a href="mailto:name@example.com">name@example.com</a>
            </div>
            <h5 class="mb-0">Morada:</h5>
            <div class="mb-4">
              Rua Dos Caldeireiros
              <br>
              4050- Baixa-Porto
            </div>
          </div>
        </div>
      </div>

      <div class="bg-faded p-4 my-4">
       <div class="col-sm-12">
            @if($sucesso==1)
                    <div class="alert alert-success alert-dismissable" id="success-alert" style="margin-top:30px">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>Sugestão efetuada com sucesso!</strong> 
                    </div>
            @endif
       </div>
        <hr class="divider">
        <h2 class="text-center text-xs text-uppercase my-0">Deixe-nos a sua opinião
        </h2>
        <h2 class="text-center text-lg text-uppercase my-0">
          <strong>Sugestões/Reclamações</strong>
        </h2>
        <hr class="divider">
        <form method="POST" action="/contactos">
          {{ csrf_field() }}
            <div class="row">
              <div class="form-group col-lg-4">
                <label class="text-heading">Nome</label>
                <input type="text" class="form-control" name='name'required>
              </div>
              <div class="form-group col-lg-4">
                <label class="text-heading">Email </label>
                <input type="email" class="form-control" name='email'required>
              </div>
              <div class="form-group col-lg-4">
                <label class="text-heading">Número de Telefone</label>
                <input type="tel" class="form-control" name='phone' required>
              </div>
              <div class="clearfix"></div>
              <div class="form-group col-lg-12">
                <label class="text-heading">Mensagem</label>
                <textarea class="form-control" rows="6" name='message' required></textarea>
              </div>
              <div class="form-group col-lg-12">
                <input type="submit" class="btn btn-secondary " value="Enviar"></input>
              </div>
            </div>
        </form>
      </div>

</div>
  
    <script src='https://code.jquery.com/jquery-2.2.4.js' ></script>
    <script>
        $('.menu li.active').removeClass('active');
        $('#contactos').addClass("active");
 
    </script>
 @endsection