
@extends('UI_Cliente.base')
 
 @section('title')
Reservas
 @endsection
 

@section('css') 
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<style>

        .branco {
            color: white;
        }

        .bordas {
            border-radius: 7px;
        }

        .centrado {
            text-align: center;
        }

            .centrado .calen {
                text-align: center;
            }

         MEDIA QUERYS MOBILE REGISTAR
        @media screen and (min-width:320px) {
            #meioR {
                min-height: 100%;
            }
        }

        @media screen and (max-width:800px) {
            #meioR {
                min-height: 1000px;
            }
        }

        @media screen and (min-width:800px) {
            #meiOR {
                min-height: auto;
            }


        }
        #lbl{
            color:red;
        }
    </style> 
    @endsection


    @section('content')
 



    <div class="container">
 <div class="col-sm-12">
            @if($sucesso==1)
                    <div class="alert alert-success alert-dismissable" id="success-alert" style="margin-top:30px">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>Reserva cancelada com sucesso!</strong> 
                    </div>
            @endif
</div>
        <div class="bg-faded p-4 my-4" id="meioR">
            <div class="card card-inverse" id="meioR">
                <img class="card-img img-fluid w-100" id="meio" src="../img/1.jpg" alt="">
                <div class="card-img-overlay bg-overlay col-sm-12" id="meioR">
                    <h3 class="card-title text-shadow text-white text-uppercase mb-0 text-center">Veja aqui todas as suas Reservas</h3></br>
                    
                            <table class="table" style="color:white;">
                                <thead>
                                    <tr >
                                        <th>Nr Reserva</th>
                                        <th>Data e Hora</th>
                                        <th>Número de Pessoas</th>
                                        <th>Estado da Reserva</th>
                                    </tr>
                                </thead>
                                <tbody>
                                      @forelse($reserves as $reserve)
                                        @if($reserve->status==0)
                                        <tr class="danger">
                                        @else <tr class="info">   
                                        @endif 
                                            <td>{{ $reserve->id }}</td>
                                            <td>{{$reserve->datehour}}</td>
                                            <td>{{$reserve->nr_persons}}</td>
                                        
                                        @if($reserve->status==0)
                                        <td> Reserva por Confirmar</td>
                                        @else
                                        
                                        <td style="color:green"><i class="fa fa-check centrado" ></i> Reserva Confirmada</td>
                                        @endif
                                        
                                         <td class="direita">
                                                <form method="post" action="/reservas/{{$reserve->id}}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    
                                                    <button type="submit" class="btn btn-danger" title="Eliminar">
                                                        <i class="fa fa-trash" ></i>
                                                    </button>
                                                </form> 
                                            </td>
                                    </tr>
                                
                                 @empty
                                 </br>
                              <div class="alert alert-danger alert-dismissable centrado">
                                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
                                 NÃO EXISTEM RESERVAS PARA APRESENTAR
                                </div>
      
                               @endforelse
                                </tbody>
                            </table>
                  
                </div>
            </div>
        </div>

<!-- SE NÃO TIVER LOGADO REDIRECIONA PARA LOGIN -->

    </div>


<script src='https://code.jquery.com/jquery-2.2.4.js'></script>

<script>

    </script>


 @endsection