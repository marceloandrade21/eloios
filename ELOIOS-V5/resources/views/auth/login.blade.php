@extends('UI_Cliente.base')
@section('css')
  <link href="css_loginregistar.css" rel="stylesheet" />
@endsection
@section('title')
Iniciar Sessão
@endsection

@section('content')
<div class="container">
    <div class="container new ">

        <div class="bg-faded p-4 my-4 " id="meioR">
            <div class="card card-inverse" id="meioR">
                <div class="card-img-overlay bg-overlay color">
<h3 class="card-title text-shadow text-white text-uppercase mb-0 text-center">ÁREA DE CLIENTE</h3>
    </br>
    </br>
    </br>
                    <div class="col-sm-8 centrado">
                        <strong style="color: white;" >Iniciar Sessão</strong>
                        <div class="main">
                            <form class="form-horizontal" method="POST" action="/login">
                                        {{ csrf_field() }}
                                        
                                            <div class="form-group">
                                                <input type="text"  name="email" required Placeholder="E-mail">
                                            </div>

                                            <div class="form-group">
                                                <input id="password" type="password"  name="password" required PlaceHolder="Password">
                                            </div>
                                               
                                            <br />
                                         
                                                        <div class="col-xs-12">
                                                            <input type="submit" class="botaos" value="Entrar" style="width:100%;">
                                                        </div>
                                        
                                </form>
                                                        <div class="col-xs-12">
                                                            <form action="/register">
                                                                <input type="submit" class="botaos" value="Registar" style="width:100%;"/> 
                                                            </form>
                                                        </div>
                                                    
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
@endsection
