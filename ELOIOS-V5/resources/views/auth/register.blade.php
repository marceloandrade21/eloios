@extends('UI_Cliente.base')
@section('css')
  <link href="css_loginregistar.css" rel="stylesheet" />
@endsection
@section('title')
REGISTAR
@endsection

@section('content')
<div class="container">
    <div class="container new ">

        <div class="bg-faded p-4 my-4 " id="meioR">
            <div class="card card-inverse" id="meioR">
                <div class="card-img-overlay bg-overlay color">
<h3 class="card-title text-shadow text-white text-uppercase mb-0 text-center">ÁREA DE CLIENTE</h3>

                    <div class="col-sm-8 centrado">
                        {{--  <strong style="color: white;" >REGISTE-SE  </strong>  --}}
                        <div class="main">
                            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                                        {{ csrf_field() }}
                                            <div class="form-group">
                                                <input type="text"  name="name" required PlaceHolder="Nome">
                                            </div>
                                            <div class="form-group">
                                                <input type="text"  name="email" required Placeholder="E-mail">
                                            </div>

                                            <div class="form-group">
                                                <input id="password" type="password"  name="password" required PlaceHolder="Password">
                                            </div>
                                                <div class="form-group">
                                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required PlaceHolder="Confirma a Password">
                                                </div>

                                            <div class="form-group">
                                                <input type="text"  name="phone" required  Placeholder="Telemóvel">
                                            </div>
                                                   
                                            <input type="submit" value="Register" style="width:100%;">
                                            
                                            
                            </form>
                     
                        </div>

                    </div>

                    


                </div>
            </div>

        </div>
    </div>
</div>
@endsection
