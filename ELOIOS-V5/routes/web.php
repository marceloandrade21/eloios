<?php
use App\Dish;
use App\Type_Dish;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $pratos = Dish::all();
    $tipos= Type_Dish::all();
    return view('UI_Cliente.index')->with(compact('pratos','tipos'));
});

Route::get('/sobre', function () {
    return view('UI_Cliente.about');
});

Route::get('/admin/admin', function () {
    $sucesso=0;
    $tipos= Type_Dish::all();
    return view('UI_Admin.Admin')->with(compact('sucesso','tipos'));;
});

Route::get('/admin/cenasparapagina', function () {
    return view('UI_Admin.cenasparapagina');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// ROTAS ADMIN
Route::prefix('admin')->namespace('Admin')->group(function () {
    
    // USERS
    //BLOQUEAR USER
    Route::get('/utilizadores/ban/{id}','UserController@bloquear');
    Route::get('/funcionarios','UserController@ListaFuncionarios');
    Route::resource('/utilizadores', 'UserController');
    
    // RCLAMAÇÕES
    Route::get('/reclamacoes/estado/{id}','ComplaintController@status');
    Route::resource('/reclamacoes', 'ComplaintController');
    // RESERVAS
    Route::get('/reservas/confirmar/{id}','ReserveController@confirmar');
    Route::resource('/reservas', 'ReserveController');

    // PEDIDO
    Route::resource('/pedidos', 'RequestController');

     // LISTA
     Route::resource('/lista', 'ListaController');

    // MESA
    Route::resource('/mesas', 'TableController');

    // PRODUTOS
    Route::resource('/produtos', 'ProductController');

    // PRATO
    // TIPO DE PRATOS
    Route::resource('/tipopratos','DishTypeController');
    //PRATOS
    Route::get('/pratos/alteramenu/{id}','DishController@AlterarMenu');
    Route::resource('/pratos', 'DishController');

    // DIVERSOS
    Route::put('/diversos/rm','VariousController@rmStock');
    Route::resource('/diversos', 'VariousController');

    // SOBREMESAS
    Route::resource('/sobremesas', 'DessertController');

    // BEBIDAS
    Route::resource('/bebidas', 'DrinkController');
    // TIPOS DE BEBIDA
    Route::resource('/tipobebidas','DrinkTypeController');

    //DIGESTIVOS
    Route::resource('/digestivos', 'DigestiveController');

    //ENTRAVAS
    Route::resource('/entradas', 'AppetizerController');
});
    

Route::namespace('Utilizador')->group(function () {
    
    // USERS
    Route::put('/utilizador/alterarpwd','UserController@AlterarPwd');
    Route::resource('/utilizador', 'UserController');


    // PRODUTOS
    Route::resource('/produtos', 'ProductController');

    // RESERVAS
    Route::get('/verreservas','ReserveController@reservesbyuser');
    Route::resource('/reservas', 'ReserveController');
   // Contacts
      Route::resource('/contactos', 'ContactController');
});
      