<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

// Route::match(['get', 'post'], 'register', function(){
//     return redirect('/');
// });

Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/order/openOrders', 'OrderController@getOpenOrders');
//
// Route::get('/order/closeOrders', 'OrderController@getCloseOrders');
//
// Route::put('/order/{id}/changeStatus', 'OrderController@changeStatus');
//
// Route::resource('/order', 'OrderController');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// ROTAS FUNCIONARIO
Route::prefix('funcionarios')->namespace('Funcionario')->group(function () {

    // CRIAR PEDIDO
    Route::post('/criarpedido', 'TableController@criarPedido');

    // MOSTRAR MESAS
    Route::get('/mesas', 'TableController@index');

    // MOSTRAR ENTRADAS
    Route::get('/{id}/entradas', 'AppetizerController@index');

    // MOSTRAR SOBREMESAS
    Route::get('/{id}/sobremesas', 'DessertController@index');

    // MOSTRAR PRATOS
    Route::post('/{id}/pratos', 'DishController@showDishByType');

    // MOSTRAR TIPO DE PRATOS
    Route::get('/{id}/tipopratos', 'DishTypeController@index');

    // MOSTRAR DIVERSOS
    Route::get('/{id}/diversos', 'VariedController@index');

    // MOSTRAR BEBIDAS
    Route::post('/{id}/bebidas', ['as' => 'bebidasbybtype', 'uses' => 'DrinkController@showDrinkByType']);

    // MOSTRAR TIPO DE BEBIDAS
    Route::get('/{id}/tipobebidas', 'DrinkTypeController@index');

    // MOSTRAR CATEGORIAS
    Route::get('/{id}/categorias', 'CatController@index');

    // MOSTRAR DIGESTIVOS
    Route::get('/{id}/digestivos', 'DigestiveController@index');

    // ADICIONAR ENTRADA AO PEDIDO
    Route::post('/{id}/addentradas', 'AppetizerController@addAppetizer');

    // ADICIONAR SOBREMESA AO PEDIDO
    Route::post('/{id}/addsobremesas', 'DessertController@addDessert');

    // ADICIONAR PRATOS AO PEDIDO
    Route::post('/{id}/addpratos', 'DishController@addDish');

    // ADICIONAR DIVERSOS AO PEDIDO
    Route::post('/{id}/adddiversos', 'VariedController@addVaried');

    // ADICIONAR BEBIDAS AO PEDIDO
    Route::post('/{id}/addbebidas', 'DrinkController@addDrink');

    // ADICIONAR DIGESTIVOS AO PEDIDO
    Route::post('/{id}/adddigestivos', 'DigestiveController@addDigestive');

    // MOSTRAR PEDIDOS ABERTOS
    Route::get('/pedidosabertos', 'RequestController@openRequests');

    // MOSTRAR PEDIDOS NA COZINHA
    Route::get('/pedidoscozinha', 'RequestController@kitchenRequests');

    // MOSTRAR PEDIDOS FECHADOS
    Route::get('/pedidosfechados', 'RequestController@closeRequests');

    // MOSTRAR LINHAS DE PEDIDO
    Route::get('/pedidos/{id}', 'RequestController@showRequestDetails');

    // ELIMINAR LINHA PEDIDO JÁ FINALIZADO
    Route::post('/linhapedido/{id}', 'RequestController@deleteRequestLine');

    // FINALIZAR PEDIDO
    Route::post('/{id}/finalizar', 'RequestController@sendToKitchen');

    // ENCERRAR PEDIDO
    Route::get('/finishrequest/{id}', 'RequestController@finishRequest');
});
