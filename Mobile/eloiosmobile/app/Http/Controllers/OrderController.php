<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Order;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $orders = Order::all();
        // return view('order.showall')
        //     ->with(compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $order = new Order();
        return view('order.create')
            ->with(compact('order'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = new Order();
        $order->name = $request->get('name');
        $order->save();
        return redirect('/order/openOrders');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        return view('order.showOne')
            ->with(compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);
        return view('order.edit')
            ->with(compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $order->name = $request->get('name');
        $order->save();

        return redirect('/order/openOrders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getOpenOrders(){
        $openOrders = DB::table('orders')
                        ->where('is_active', '=', 1)
                        ->orderby('')
                        ->get();

        return view('order.showOpen')
            ->with(compact('openOrders'));
    }

    public function getCloseOrders(){
        $closeOrders = DB::table('orders')
                        ->where('is_active', '=', 0)
                        ->get();

        return view('order.showClose')
            ->with(compact('closeOrders'));
    }

    public function changeStatus($id){
        $order = Order::find($id);
        $order->is_active = 0;
        $order->save();

        return redirect('/order/openOrders');
    }
}

