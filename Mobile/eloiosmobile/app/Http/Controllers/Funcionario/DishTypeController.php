<?php

namespace App\Http\Controllers\Funcionario;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\DishType;
use App\Request as Pedido;
use App\Product;

class DishTypeController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
   public function index($id)
   {
       //Obter todos os appetizers
       $dishtypes = DishType::all();
       //Obter pedido
       $pedido = Pedido::find($id);
       $pedidoID = $pedido->id;
       //Retorno
       return view('UI.pratos.showTipo', compact('dishtypes', 'pedidoID'));
   }
}
