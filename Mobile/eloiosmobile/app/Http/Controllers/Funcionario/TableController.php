<?php

namespace App\Http\Controllers\Funcionario;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Table;
use App\Request as Pedido;

class TableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tables = Table::all();
        $errorTables = 0;
        return view('UI.mesas.show', compact('tables', 'errorTables'));
    }

    public function criarPedido(Request $request)
    {
        $pedido = new Pedido();
        $pedido->status = 1;
        $pedido->datehour = Carbon::now();
        $pedido->user_id = $request->user;
        $pedido->tables_name = $request->tables;
        $tables = $request->tables;
        $flag = 0;
        if($tables != ""){
          foreach(explode(',', $tables) as $table){
            $mesa = Table::where('name', '=', $table)->first();
            $mesa->status = 0;
            $mesa->save();
            if($flag == 0){
              $pedido->table_id = $table;
              $pedido->save();
            }
          }
          return view('UI.categorias.show', compact('pedido'));
        }else {
          $tables = Table::all();
          $errorTables = 1;
          return view('UI.mesas.show', compact('errorTables', 'tables'));
        }
    }
}
