<?php

namespace App\Http\Controllers\Funcionario;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Varied;
use App\Request as Pedido;
use App\ProductRequest;
use App\Product;

class VariedController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index($id)
  {
    //Obter todos as sobremesas
    $various = Varied::all();
    //Obter pedido
    $pedido = Pedido::find($id);
    $pedidoID = $pedido->id;
    //Obter linhas de pedido
    $requestLines = ProductRequest::where('request_id', '=', $pedidoID)->get();
    //Mais que a quantidade inicializada a 0
    $quantOver = 0;
    $nomeProd = '';
    //Retorno
    return view('UI.diversos.show', compact('various', 'pedidoID', 'requestLines', 'quantOver', 'nomeProd'));
  }

  public function addVaried(Request $request, $id)
  {
    //Obter pedido
    $pedido = Pedido::find($id);
    //Obter id pedido
    $pedidoID = $pedido->id;
    //Obter entrada
    $product = Product::find($request->name);
    //Obter todas as entradas
    $various = Varied::all();
    //Flag
    $check = 0;
    //Controlo de Quantidade
    $quantOver = 0;
    //Obter numero de linhas de pedido
    $requestLines = ProductRequest::where('request_id', '=', $pedido->id)->get();
    //Percorer todas as linhas de pedido
    foreach($requestLines as $requestLine){
      if($requestLine->product_id == $product->id){ //<-- Se encontrar correspondencia
        $updateLine = ProductRequest::find($requestLine->id);
        $varied = Varied::where('product_id', '=', $product->id)->first();
        if($updateLine->quantity + 1 > $varied->quantityavailable){
          $nomeProd = $product->name;
          $quantOver = 1;
          $check = 1;
          break;
        }else{
          $updateLine->quantity = $updateLine->quantity + 1;
          $updateLine->save();
          $check = 1;
        }
      }
    }
    if($check == 0){ //<-- Se não encontrar correspondencia adiciona um novo
      $newLine = new ProductRequest();
      $newLine->request_id = $pedido->id;
      $newLine->product_id = $product->id;
      $newLine->description = '';
      $newLine->quantity = 1;
      $newLine->save();
    }
    //Atualizar as linhas do pedido
    $requestLines = ProductRequest::where('request_id', '=', $pedido->id)->get();
    //Retorno
    return view('UI.diversos.show', compact('various', 'pedidoID', 'requestLines', 'quantOver', 'nomeProd'));
  }
}
