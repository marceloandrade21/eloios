<?php

namespace App\Http\Controllers\Funcionario;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Request as Pedido;

class CatController extends Controller
{
    public function index($id){
      $pedido = Pedido::find($id);
      $pedido->status = 1;
      $pedido->save();
      return view('UI.categorias.show', compact('pedido'));
    }
}
