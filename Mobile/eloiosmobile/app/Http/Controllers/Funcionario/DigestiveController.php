<?php

namespace App\Http\Controllers\Funcionario;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Digestive;
use App\Request as Pedido;
use App\ProductRequest;
use App\Product;

class DigestiveController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index($id)
  {
      //Obter todos os appetizers
      $digestives = Digestive::all();
      //Obter pedido
      $pedido = Pedido::find($id);
      $pedidoID = $pedido->id;
      //Obter linhas de pedido
      $requestLines = ProductRequest::where('request_id', '=', $pedidoID)->get();
      //Retorno
      return view('UI.digestivos.show', compact('digestives', 'pedidoID', 'requestLines'));
  }

  public function addDigestive(Request $request, $id)
  {
    //Obter pedido
    $pedido = Pedido::find($id);
    //Obter id pedido
    $pedidoID = $pedido->id;
    //Obter entrada
    $product = Product::find($request->name);
    //Obter todas as entradas
    $digestives = Digestive::all();
    //Flag
    $check = 0;
    //Obter numero de linhas de pedido
    $requestLines = ProductRequest::where('request_id', '=', $pedido->id)->get();
    //Percorer todas as linhas de pedido
    foreach($requestLines as $requestLine){
      if($requestLine->product_id == $product->id){ //<-- Se encontrar correspondencia
        $updateLine = ProductRequest::find($requestLine->id);
        $updateLine->quantity = $updateLine->quantity + 1;
        $updateLine->save();
        $check = 1;
      }
    }
    if($check == 0){ //<-- Se não encontrar correspondencia adiciona um novo
      $newLine = new ProductRequest();
      $newLine->request_id = $pedido->id;
      $newLine->product_id = $product->id;
      $newLine->description = "";
      $newLine->quantity = 1;
      $newLine->save();
    }
    //Atualizar as linhas do pedido
    $requestLines = ProductRequest::where('request_id', '=', $pedido->id)->get();
    //Retorno
    return view('UI.digestivos.show', compact('digestives', 'pedidoID', 'requestLines'));
  }
}
