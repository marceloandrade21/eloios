<?php

namespace App\Http\Controllers\Funcionario;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Request as Pedido;
use App\ProductRequest;
use App\Appetizer;
use App\Dessert;
use App\Digestive;
use App\Dish;
use App\DishType;
use App\Drink;
use App\DrinkType;
use App\Varied;
use App\Table;

class RequestController extends Controller
{
  public function sendToKitchen($id){
    //Obter pedido
    $request = Pedido::find($id);
    //Obter linhas daquele pedido
    $requestLines = ProductRequest::where('request_id', '=', $id)->get();
    //Por cada linha retirar a quantidade
    foreach($requestLines as $requestLine){
      if($requestLine->status == 0){
        //Meter linha como ter passado pela cozinha
        $requestLine->status = 1;
        //Verificar o tipo de produto
        //Sobremesa
        $dessert = Dessert::where('product_id', '=', $requestLine->product_id)->first();
        //Bebida
        $drink = Drink::where('product_id', '=', $requestLine->product_id)->first();
        //Diverso
        $varied = Varied::where('product_id', '=', $requestLine->product_id)->first();
        if($dessert){
          $dessert->quantityavailable = $dessert->quantityavailable - $requestLine->quantity;
          $dessert->save();
        }else{
          if($varied){
            $varied->quantityavailable = $varied->quantityavailable - $requestLine->quantity;
            $varied->save();
          }else{
            if($drink){
              $drink->quantityavailable = $drink->quantityavailable - $requestLine->quantity;
              $drink->save();
            }
          }
        }
      }
    }
    //Alterar estado do pedido
    $request->status = 0;
    //Guardar
    $request->save();
    //Retorno
    return redirect('/'); //<-- por agora
  }

  public function openRequests(){
    //Obter pedidos abertos
    $requests = Pedido::where('status', '=', 1)->get();
    //Retorno
    return view('UI.pedidos.showOpen', compact('requests'));
  }

  public function kitchenRequests(){
    //Obter pedidos abertos
    $requests = Pedido::where('status', '=', 0)->get();
    //Retorno
    return view('UI.pedidos.showKitchen', compact('requests'));
  }

  public function closeRequests(){
    //Obter pedidos abertos
    $requests = Pedido::where('status', '=', null)->get();
    //Retorno
    return view('UI.pedidos.closeRequests', compact('requests'));
  }

  public function showRequestDetails($id){
    //Obter pedido
    $request = Pedido::find($id);
    //Obter linhas de pedido
    $requestLines = ProductRequest::where('request_id', '=', $id)->get();
    //Retorno
    return view('UI.pedidos.showDetails', compact('requestLines', 'request'));
  }

  public function deleteRequestLine($id){
    //Obter linha de pedido
    $requestLine = ProductRequest::find($id);
    //Verificar o tipo de produto
    //Sobremesa
    $dessert = Dessert::where('product_id', '=', $requestLine->product_id)->first();
    //Bebida
    $drink = Drink::where('product_id', '=', $requestLine->product_id)->first();
    //Diverso
    $varied = Varied::where('product_id', '=', $requestLine->product_id)->first();
    if($dessert){
      $dessert->quantityavailable = $dessert->quantityavailable + $requestLine->quantity;
      $dessert->save();
      $requestLine->delete();
    }else{
      if($varied){
        $varied->quantityavailable = $varied->quantityavailable + $requestLine->quantity;
        $varied->save();
        $requestLine->delete();
      }else{
        if($drink){
          $drink->quantityavailable = $drink->quantityavailable + $requestLine->quantity;
          $drink->save();
          $requestLine->delete();
        }else{
          $requestLine->delete();
        }
      }
    }
    //Retorno
    return redirect(url()->previous());
  }

  public function finishRequest($id){
    //Obter pedido
    $request = Pedido::find($id);
    //Obter mesas
    $tables = $request->tables_name;
    //Alterar estado das mesas
    foreach(explode(',', $tables) as $table){
      $mesa = Table::where('name', '=', $table)->first();
      $mesa->status = 1;
      $mesa->save();
    }
    //Alterar estado do pedido
    $request->status = null;
    //Gravar
    $request->save();
    //Retorno
    return redirect('/');
  }

  public function addDrinkQuantLine($id, Request $request){
    $requestLine = ProductRequest::find($id);
    $quantOver = 0;
    $drink = Drink::where('id', '=', $request->prod)->first();
    if($request->qtd + 1 <= $drink->quantityavailable){
      $requestLine->quantity = $request->qtd + 1;
      $requestLine->save();
    }else{
      $quantOver = 1;
    }
    //Retorno
    $nomeProd = $drink->name;
    $drinkType = DrinkType::where('id', '=', $drink->type_drink_id)->first();
    $typeName = $drinkType->name;
    $pedido = Pedido::where('id', '=', $requestLine->request_id)->first();
    $pedidoID = $pedido->id;
    $requestLines = ProductRequest::where('request_id', '=', $pedido->id);
    $drinks = Drink::where('type_drink_id', '=', $drink->type_drink_id)->get();
    return redirect()->route('bebidasbybtype', ['request'=>['name'=>$drink->type_drink_id], 'id'=>$pedidoID]);
    // return view('UI.bebidas.show', compact('drinks', 'pedidoID', 'requestLines', 'quantOver', 'nomeProd', 'typeName'));
  }

  public function rmDrinkQuantLine($id, Request $request){
    $requestLine = ProductRequest::find($id);
    $quantOver = 0;
    $drink = Drink::where('id', '=', $request->prod)->first();
    
  }
}
