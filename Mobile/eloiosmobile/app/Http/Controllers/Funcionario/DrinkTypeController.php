<?php

namespace App\Http\Controllers\Funcionario;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\DrinkType;
use App\Request as Pedido;
use App\Product;

class DrinkTypeController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
   public function index($id)
   {
       //Obter todos os appetizers
       $drinktypes = DrinkType::all();
       //Obter pedido
       $pedido = Pedido::find($id);
       $pedidoID = $pedido->id;
       //Retorno
       return view('UI.bebidas.showTipo', compact('drinktypes', 'pedidoID'));
   }
}
