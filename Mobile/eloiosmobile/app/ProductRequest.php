<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductRequest extends Model
{
    protected $table = "product_request";
    
    public function request(){
        return $this->belongsTo(Request::Class);
    }
    public function product(){
        return $this->belongsTo(Product::Class);
    }
}
