<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  public function product_request(){
      return $this->belongsToMany(Product_Request::Class);
  }
}
