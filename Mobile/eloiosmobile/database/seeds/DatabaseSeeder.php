<?php

use Illuminate\Database\Seeder;
use App\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('utypes')->insert(['name'=>'admin']);
        DB::table('utypes')->insert(['name'=>'normal']);
        DB::table('utypes')->insert(['name'=>'funcionario']);
        DB::table('utypes')->insert(['name'=>'cozinha']);
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@admin.pt',
            'password' => bcrypt('123456a'),
            'status'=>1,
            'utype_id'=>1,
            'phone'=>919119,
        ]);
        DB::table('users')->insert([
            'name' => 'ricardo',
            'email' => 'ricardo@gmail.pt',
            'password' => bcrypt('123qwe'),
            'status'=>1,
            'utype_id'=>3,
            'phone'=>919119,
        ]);

        /* MESAS */

        DB::table('tables')->insert(['name' => '1','status'=>1,]);
        DB::table('tables')->insert(['name' => '2','status'=>1,]);
        DB::table('tables')->insert(['name' => '3','status'=>1,]);
        DB::table('tables')->insert(['name' => '4','status'=>1,]);
        DB::table('tables')->insert(['name' => '5','status'=>1,]);
        DB::table('tables')->insert(['name' => '6','status'=>1,]);
        DB::table('tables')->insert(['name' => '7','status'=>1,]);
        DB::table('tables')->insert(['name' => '8','status'=>1,]);
        DB::table('tables')->insert(['name' => '9','status'=>1,]);

        /* TIPOS DE BEBIDAS*/

        DB::table('drink_types')->insert(['name' => 'Vinhos',]);
        DB::table('drink_types')->insert(['name' => 'Águas',]);
        DB::table('drink_types')->insert(['name' => 'Sumos',]);
        DB::table('drink_types')->insert(['name' => 'Cerveja',]);

        /* TIPOS DE PRATOS */
        DB::table('dish_types')->insert(['name'=>'Leitão']);
        DB::table('dish_types')->insert(['name'=>'Portugueses']);
        DB::table('dish_types')->insert(['name'=>'Carne']);
        DB::table('dish_types')->insert(['name'=>'Peixe']);

        /* TABELA "MÃE" DE PRODUTOS */
        // BEBIDAS
        DB::table('products')->insert(['name' => 'Vinho Branco','img'=>'','menu'=>1]);//1
        DB::table('products')->insert(['name' => 'Vitalis','img'=>'','menu'=>1]);//2
        DB::table('products')->insert(['name' => '7Up','img'=>'','menu'=>1]);//3
        //ENTRADAS
        DB::table('products')->insert(['name' => 'Rissois de Carne','img'=>'','menu'=>1]);//4
        //SOBREMESAS
        DB::table('products')->insert(['name' => 'Bolo Bolacha','img'=>'','menu'=>1]);//5
        DB::table('products')->insert(['name' => 'Leite Creme','img'=>'','menu'=>1]);//6
        DB::table('products')->insert(['name' => 'Fruta','img'=>'','menu'=>1]);//7
        DB::table('products')->insert(['name' => 'Molotof','img'=>'','menu'=>1]);//8
        //DIGESTIVOS
        DB::table('products')->insert(['name' => 'Bagaço','img'=>'','menu'=>1]);//9
        DB::table('products')->insert(['name' => 'Café','img'=>'','menu'=>1]);//10
        DB::table('products')->insert(['name' => 'Wiskhy','img'=>'','menu'=>1]);//11
        //PRATOS
        DB::table('products')->insert(['name' => 'Sande Leitão','img'=>'','menu'=>1]);//12
        DB::table('products')->insert(['name' => 'Feijoada','img'=>'','menu'=>1]);//13
        DB::table('products')->insert(['name' => 'Prato Leitão','img'=>'','menu'=>1]);//14
        DB::table('products')->insert(['name' => 'Bacalhau a Brás','img'=>'','menu'=>1]);//15
        DB::table('products')->insert(['name' => 'Arroz de Pato','img'=>'','menu'=>1]);//16
        DB::table('products')->insert(['name' => 'Arroz de Cabidela','img'=>'','menu'=>1]);//17
        DB::table('products')->insert(['name' => 'Carne Alentejana','img'=>'','menu'=>1]);//18
        DB::table('products')->insert(['name' => 'Prato de Bifanas','img'=>'','menu'=>1]);//19
        DB::table('products')->insert(['name' => 'Prego no Pão','img'=>'','menu'=>1]);//20
        //DIVERSOS
        
        DB::table('products')->insert(['name' => 'Vinho Tinto','img'=>'','menu'=>1]);//21
        DB::table('products')->insert(['name' => 'Vinho Rosé','img'=>'','menu'=>1]);//22
        DB::table('products')->insert(['name' => 'Água das Pedras','img'=>'','menu'=>1]);//23
        DB::table('products')->insert(['name' => 'Freeze Limão','img'=>'','menu'=>1]);//24
        DB::table('products')->insert(['name' => 'Freeze Frutos Vermelhos','img'=>'','menu'=>1]);//25

        DB::table('products')->insert(['name' => 'Tábua de Queijos','img'=>'','menu'=>1]);//26
        DB::table('products')->insert(['name' => 'Rissois Leitão','img'=>'','menu'=>1]);//27
        DB::table('products')->insert(['name' => 'Rissois Camarão','img'=>'','menu'=>1]);//28
        DB::table('products')->insert(['name' => 'Pão','img'=>'','menu'=>1]);//29
        DB::table('products')->insert(['name' => 'Presunto','img'=>'','menu'=>1]);//30
        DB::table('products')->insert(['name' => 'Melão','img'=>'','menu'=>1]);//31
        DB::table('products')->insert(['name' => 'Paté de Atum','img'=>'','menu'=>1]);//32
        DB::table('products')->insert(['name' => 'Camarões','img'=>'','menu'=>1]);//33

        DB::table('products')->insert(['name' => 'Cerveja Alemã','img'=>'','menu'=>1]);//34
        DB::table('products')->insert(['name' => 'SuperBock','img'=>'','menu'=>1]);//35
        DB::table('products')->insert(['name' => 'Sagres','img'=>'','menu'=>1]);//36
        
       
        

        /* TABELA ESPECÍFICA DOS PRODUTOS */

        DB::table('appetizers')->insert(['product_id'=>4,'name'=>'Rissois de Carne']);
        DB::table('appetizers')->insert(['product_id'=>26,'name'=>'Tábua de Queijos']);
        DB::table('appetizers')->insert(['product_id'=>27,'name'=>'Rissois Leitão']);
        DB::table('appetizers')->insert(['product_id'=>27,'name'=>'Rissois Camarão']);
        DB::table('appetizers')->insert(['product_id'=>29,'name'=>'Pão']);
        DB::table('appetizers')->insert(['product_id'=>30,'name'=>'Presunto']);
        DB::table('appetizers')->insert(['product_id'=>31,'name'=>'Melão']);
        DB::table('appetizers')->insert(['product_id'=>32,'name'=>'Paté de Atum']);
        DB::table('appetizers')->insert(['product_id'=>33,'name'=>'Camarões']);
        
        DB::table('drinks')->insert(['product_id'=>1,'name'=>'Vinho Branco','type_drink_id'=>1,'quantityavailable'=>10,'quantitymin'=>2]);
        DB::table('drinks')->insert(['product_id'=>2,'name'=>'Vitalis','type_drink_id'=>2,'quantityavailable'=>10,'quantitymin'=>2]);
        DB::table('drinks')->insert(['product_id'=>3,'name'=>'7Up','type_drink_id'=>3,'quantityavailable'=>1,'quantitymin'=>2]);
        DB::table('desserts')->insert(['product_id'=>5,'name'=>'Bolo Bolacha','quantityavailable'=>10,'quantitymin'=>5]);
        DB::table('desserts')->insert(['product_id'=>6,'name'=>'Leite Creme','quantityavailable'=>5,'quantitymin'=>5]);
        DB::table('desserts')->insert(['product_id'=>7,'name'=>'Fruta','quantityavailable'=>2,'quantitymin'=>5]);
        DB::table('desserts')->insert(['product_id'=>8,'name'=>'Molotof','quantityavailable'=>10,'quantitymin'=>5]);
        DB::table('digestives')->insert(['product_id'=>9,'name'=>'Bagaço']);
        DB::table('digestives')->insert(['product_id'=>10,'name'=>'Café']);
        DB::table('digestives')->insert(['product_id'=>11,'name'=>'Wiskhy']);
        DB::table('dishes')->insert(['product_id'=>12,'name'=>'Sande Leitão','type_dish_id'=>1]);
        DB::table('dishes')->insert(['product_id'=>14,'name'=>'Prato Leitão','type_dish_id'=>1]);
        DB::table('dishes')->insert(['product_id'=>13,'name'=>'Feijoada','type_dish_id'=>2]);
        DB::table('dishes')->insert(['product_id'=>15,'name'=>'Bacalhau a Brás','type_dish_id'=>2]);
        DB::table('dishes')->insert(['product_id'=>16,'name'=>'Arroz de Pato','type_dish_id'=>2]);
        DB::table('dishes')->insert(['product_id'=>17,'name'=>'Arroz de Cabidela','type_dish_id'=>2]);
        DB::table('dishes')->insert(['product_id'=>18,'name'=>'Carne Alentejana','type_dish_id'=>2]);
        DB::table('dishes')->insert(['product_id'=>19,'name'=>'Prato de Bifanas','type_dish_id'=>2]);
        DB::table('dishes')->insert(['product_id'=>16,'name'=>'Arroz de Pato','type_dish_id'=>3]);
        DB::table('dishes')->insert(['product_id'=>17,'name'=>'Arroz de Cabidela','type_dish_id'=>3]);
        DB::table('dishes')->insert(['product_id'=>18,'name'=>'Carne Alentejana','type_dish_id'=>3]);
        DB::table('dishes')->insert(['product_id'=>19,'name'=>'Prato de Bifanas','type_dish_id'=>3]);
        DB::table('dishes')->insert(['product_id'=>15,'name'=>'Bacalhau a Brás','type_dish_id'=>4]);
        DB::table('drinks')->insert(['product_id'=>21,'name'=>'Vinho Tinto','type_drink_id'=>1,'quantityavailable'=>10,'quantitymin'=>2]);
        DB::table('drinks')->insert(['product_id'=>22,'name'=>'Vinho Rosé','type_drink_id'=>1,'quantityavailable'=>10,'quantitymin'=>2]);
        DB::table('drinks')->insert(['product_id'=>23,'name'=>'Água das Pedras','type_drink_id'=>2,'quantityavailable'=>10,'quantitymin'=>2]);
        DB::table('drinks')->insert(['product_id'=>24,'name'=>'Freeze Limão','type_drink_id'=>2,'quantityavailable'=>10,'quantitymin'=>2]);
        DB::table('drinks')->insert(['product_id'=>25,'name'=>'Freeze Frutos Vermelhos','type_drink_id'=>2,'quantityavailable'=>10,'quantitymin'=>2]);
        DB::table('drinks')->insert(['product_id'=>34,'name'=>'Cerveja Alemã','type_drink_id'=>4,'quantityavailable'=>10,'quantitymin'=>2]);
        DB::table('drinks')->insert(['product_id'=>35,'name'=>'SuperBock','type_drink_id'=>4,'quantityavailable'=>10,'quantitymin'=>2]);
        DB::table('drinks')->insert(['product_id'=>36,'name'=>'Sagres','type_drink_id'=>4,'quantityavailable'=>10,'quantitymin'=>2]);
        
    }
}
