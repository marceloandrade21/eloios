<!DOCTYPE html>
<html>
    <head>
        <!--MEU CSS-->
        <link href="/css/mainCss/css.css" rel="stylesheet" type="text/css"></link>
        <link href="/css/buttonsCss/css.css" rel="stylesheet" type="text/css"></link>
        <link href="/css/toastCss/css.css" rel="stylesheet" type="text/css"></link>
        <!--BOOTSTRAP-->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="/css/custom.css">


    </head>

    <body onload="javascript:toast()">
        <!--OPÇÕES MENU LATERAL-->
        <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        @auth
            <a href="/home">Home</a>
            <a href="/funcionarios/mesas">Novo Pedido</a>
            <a href="/funcionarios/pedidosabertos">Pedidos Abertos</a>
            <a href="/funcionarios/pedidoscozinha">Pedidos Cozinha</a>
            <a href="/funcionarios/pedidosfechados">Pedidos Fechados</a>
            <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                Logout
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        @endauth
        @guest
            <a href="#">Login</a>
        @endguest
        </div>
        <!--NAVBAR-->
        <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Menu</span>
            </div>
        </div>
        </nav>
        <!--ABRIR MENU LATERAL-->


        @yield('content')

        <script>
            /*JAVASCRIPT MENU LATERAL*/
            function openNav() {
                document.getElementById("mySidenav").style.width = "250px";
            }

            function closeNav() {
                document.getElementById("mySidenav").style.width = "0";
            }
            /*************************/
            /*JAVASCRIPT TOAST*/
            function toast() {
                var x = document.getElementById("snackbar")
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
            }
            /*************************/
        </script>
        <script type="text/javascript" src="/js/Myjs.js"></script>
    </body>
</html>
