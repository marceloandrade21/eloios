@extends('principal.main')

@section('content')

<div id="prodsType" class="container text-center">
  <form method="POST" action="/funcionarios/{{$pedidoID}}/bebidas">
    {{ csrf_field() }}
    @foreach($drinktypes as $drinktype)
      <button class="btn btn-primary buttonTypeProd" type="submit" value="{{$drinktype->id}}" name="name">{{$drinktype->name}}</button>
    @endforeach
  </form>
</div>

@endsection
