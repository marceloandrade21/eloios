@extends('principal.main')

@section('content')

@if($quantOver == 1)
  <div class="alert alert-danger text-center" id="success-alert">
    <strong>Não é possivel adicionar mais {{$nomeProd}}</strong>
  </div>
@endif

<div id="prods" class="container text-center">
  <h3 class="title">{{ $typeName }}</h3>
  <form method="POST" action="/funcionarios/{{$pedidoID}}/addbebidas">
    {{ csrf_field() }}
    @foreach($drinks as $drink)
      @if($drink->quantityavailable > $drink->quantitymin)
        <input type="hidden" value="{{$drink->type_drink_id}}" name="drinktype"></input>
        <button class="btn btn-primary buttonProd" type="submit" value="{{$drink->product_id}}" name="name">{{$drink->name}}</button>
      @else
        @if($drink->quantityavailable > 0 && $drink->quantityavailable <= $drink->quantitymin)
          <input type="hidden" value="{{$drink->type_drink_id}}" name="drinktype"></input>
          <button class="btn btn-warning buttonProd" type="submit" value="{{$drink->product_id}}" name="name">{{$drink->name}}</button>
        @endif
      @endif
    @endforeach
  </form>
  <p>
    <a class="btn btn-primary buttonSub" href="/funcionarios/{{$pedidoID}}/categorias">Ok</a>
  </p>
</div>
<div id="prodsList" class="container">  
  <table class="table tableProds">
    <tr class="header">
      <th class="right">Nome Produto</th>
      <th>Quantidade</th>
    </tr>
      @foreach($requestLines as $requestLine)
      <tr>
        <td class="right">{{$requestLine->product->name}}</td>
        <td>{{$requestLine->quantity}}</td>
      </tr>
      @endforeach
  </table> 
</div>

<script type="text/javascript">
  $("#success-alert").fadeTo(1000, 500).slideUp(500, function(){
    $("#success-alert").alert('close');

  });
</script>

@endsection
