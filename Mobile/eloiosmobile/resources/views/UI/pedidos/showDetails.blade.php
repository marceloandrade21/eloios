@extends('principal.main')

@section('content')
<div id="prodsList" class="container text-center">
  <p><h3 class="title">Detalhes do pedido</h3></p>
  <table class="table tableProds">
    <tr class="header">
      <th class="right">Nome Produto</th>
      <th>Quantidade</th>
      @if($request->status === 1 || $request->status === 0)
        <th class="left">Ações</th>
      @endif
    </tr>
    @foreach($requestLines as $requestLine)
      <tr class="stripe">
        <td class="right" valign="center">{{$requestLine->product->name}}</td>
        <td valign="center">{{$requestLine->quantity}}</td>
        @if($request->status === 1 || $request->status === 0)
          <td class="left" valign="center">
            <form class="" action="/funcionarios/linhapedido/{{$requestLine->id}}" method="POST">
              {{ csrf_field() }}
              <button class="btn btn-danger tableButtons" type="submit">Eliminar</button>
            </form>
          </td>
        @endif
      </tr>
    @endforeach
  </table>
</div>
@if($request->status === 1 || $request->status === 0)
  <div id="addProds" class="container text-center">
    <a class="btn btn-primary buttonAdd bottom" href="/funcionarios/{{$request->id}}/categorias">Adicionar Produtos</a>
  </div>
@endif

@endsection
