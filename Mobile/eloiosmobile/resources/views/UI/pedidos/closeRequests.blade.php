@extends('principal.main')

@section('content')
<div id="prodsList" class="container text-center">
  <p><h3 class="title">Pedidos terminados</h3></p>
  <table class="table tableProds bottom">
    <tr class="header">
      <th class="right">Mesa</th>
      <th class="right">Data/Hora</th>
      <th>Ações</th>
    </tr>
    @foreach($requests as $request)
      <tr class="stripe">
        <td class="right">{{$request->table_id}}</td>
        <td class="right">{{$request->datehour}}</td>
        <td>
          <a class="btn btn-primary tableButtons" href="/funcionarios/pedidos/{{$request->id}}">Ver pedido</a>
        </td>
      </tr>
    @endforeach
  </table>
</div>

@endsection
