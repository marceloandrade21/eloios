@extends('principal.main')

@section('content')
<div id="prodsList" class="container text-center">
  <p><h3 class="title">Pedidos na cozinha</h3></p>
  <table class="table tableProds bottom">
    <tr class="header">
      <th class="right">Mesa</th>
      <th>Ações</th>
    </tr>
    @foreach($requests as $request)
      <tr class="stripe">
        <td class="right">{{$request->table_id}}</td>
        <td>
          <a class="btn btn-primary tableButtons" href="/funcionarios/{{$request->id}}/categorias">Adicionar Produtos</a>
          <a class="btn btn-primary tableButtons" href="/funcionarios/pedidos/{{$request->id}}">Ver pedido</a>
        </td>
      </tr>
    @endforeach
  </table>
</div>
@endsection
