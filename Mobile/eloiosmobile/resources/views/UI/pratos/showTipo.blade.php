@extends('principal.main')

@section('content')

<div id="prodsType" class="container text-center">
  <h3 class="title">Tipo de Pratos</h3>
  <form method="POST" action="/funcionarios/{{$pedidoID}}/pratos">
    {{ csrf_field() }}
    @foreach($dishtypes as $dishtype)
      <p><button class="btn btn-primary buttonTypeProd" type="submit" value="{{$dishtype->id}}" name="name">{{$dishtype->name}}</button></p>
    @endforeach
  </form>
</div>

@endsection
