@extends('principal.main')

@section('content')

<div id="prods" class="container text-center">
  <h3 class="title">Pratos {{ $typeName }}</h3>
  <form method="POST" action="/funcionarios/{{$pedidoID}}/addpratos">
    {{ csrf_field() }}
    @foreach($dishes as $dish)
      <input type="hidden" value="{{$dish->type_dish_id}}" name="dishtype"></input>
      <button class="btn btn-primary buttonProd" type="submit" value="{{$dish->product_id}}" name="name">{{$dish->name}}</button>
    @endforeach
  </form>
  <p>
    <a class="btn btn-primary buttonSub" href="/funcionarios/{{$pedidoID}}/categorias">Ok</a>
  </p>
</div>
<div id="prodsList" class="container">
  <table class="table tableProds">
    <tr class="header">
      <th class="right">Nome Produto</th>
      <th>Quantidade</th>
    </tr>
      @foreach($requestLines as $requestLine)
      <tr>
        <td class="right">{{$requestLine->product->name}}</td>
        <td>{{$requestLine->quantity}}</td>
      </tr>
      @endforeach
  </table>
</div>
@endsection
