@extends('principal.main')

@section('content')

  <div class="container text-center" id="inicio">
    <h3>Mesas Disponíveis</h3>
    @if($errorTables == 1)
    <div class="alert alert-danger" id="success-alert">
      <strong>Nenhuma mesa selecionada</strong>
    </div>
    @endif
    <form method="POST" action="/funcionarios/criarpedido">
      {{ csrf_field() }}
      @foreach($tables as $table)
        @if($table->status == 1)
          <a id="btn{{$table->name}}" class="btn btn-primary buttonsTables" onclick="selectTable('btn{{$table->name}}',this)">{{$table->name}}</a>
        @endif
      @endforeach
      <br>
      <h3 class="secTitle">Mesas Selecionadas</h3>
      <div class="showTables">
        <input type="hidden" name="user" value="{{Auth::user()->id}}"/>
        <input id="txt" name="tables" type="text" value="" readonly/><br>
        <a class="btn btn-primary buttonsBottom" onclick="reset()">Limpar</a>
        <button type="submit" class="btn btn-primary buttonsBottom" name="next">Seguinte</button>
      </div>
    </form>
  </div>
  <script type="text/javascript">
    $("#success-alert").fadeTo(1000, 500).slideUp(500, function(){
      $("#success-alert").alert('close');
    });
  </script>
@endsection
