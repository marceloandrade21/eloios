@extends('principal.main')

@section('content')

@if($quantOver == 1)
  <div class="alert alert-danger" id="success-alert">
    <strong>Não é possivel adicionar mais {{$nomeProd}}</strong>
  </div>
@endif

<div>
  <form method="POST" action="/funcionarios/{{$pedidoID}}/adddiversos">
    {{ csrf_field() }}
    @foreach($various as $varied)
      @if($varied->quantityavailable > $varied->quantitymin)
        <button class="btn btn-primary" type="submit" value="{{$varied->product_id}}" name="name">{{$varied->name}}</button>
      @else
        @if($varied->quantityavailable > 0 && $varied->quantityavailable <= $varied->quantitymin)
          <button class="btn btn-warning" type="submit" value="{{$varied->product_id}}" name="name">{{$varied->name}}</button>
        @endif
      @endif
    @endforeach
  </form>
  <p>
    <a class="btn btn-primary" href="/funcionarios/{{$pedidoID}}/categorias">Ok</a>
  </p>
</div>
<div>
  <table class="table table-striped">
    <tr>
      <th>Nome Produto</th>
      <th>Quantidade</th>
    </tr>
      @foreach($requestLines as $requestLine)
      <tr>
        <td>{{$requestLine->product->name}}</td>
        <td>{{$requestLine->quantity}}</td>
      </tr>
      @endforeach
  </table>
</div>

<script type="text/javascript">
  $("#success-alert").fadeTo(1000, 500).slideUp(500, function(){
    $("#success-alert").alert('close');

  });
</script>
@endsection
