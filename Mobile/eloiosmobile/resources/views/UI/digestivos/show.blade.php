@extends('principal.main')

@section('content')

<div id="prods" class="container text-center">
  <form method="POST" action="/funcionarios/{{$pedidoID}}/adddigestivos">
    {{ csrf_field() }}
    @foreach($digestives as $digestive)
      <button class="btn btn-primary buttonProd" type="submit" value="{{$digestive->product_id}}" name="name">{{$digestive->name}}</button>
    @endforeach
  </form>
  <p>
    <a class="btn btn-primary buttonSub" href="/funcionarios/{{$pedidoID}}/categorias">Ok</a>
  </p>
</div>
<div id="prodsList" class="container">
  <table class="table tableProds">
    <tr class="header">
      <th class="right">Nome Produto</th>
      <th>Quantidade</th>
    </tr>
      @foreach($requestLines as $requestLine)
      <tr>
        <td class="right">{{$requestLine->product->name}}</td>
        <td>{{$requestLine->quantity}}</td>
      </tr>
      @endforeach
  </table>
</div>
@endsection
