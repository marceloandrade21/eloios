@extends('principal.main')

@section('content')

  @if(1 == 0)
    <div class="container text-center">
      <h3>Nenhum item adicionado ao pedido</h3>
    </div>
  @endif
  <div id="cats" class="container text-center">
    <p><a href="/funcionarios/{{$pedido->id}}/entradas" class="btn btn-primary buttonsCat">Entradas</a></p>
    <p><a href="/funcionarios/{{$pedido->id}}/tipopratos" class="btn btn-primary buttonsCat">Pratos</a></p>
    <p><a href="/funcionarios/{{$pedido->id}}/tipobebidas" class="btn btn-primary buttonsCat">Bebidas</a></p>
    <p><a href="/funcionarios/{{$pedido->id}}/sobremesas" class="btn btn-primary buttonsCat">Sobremesas</a></p>
    <p><a href="/funcionarios/{{$pedido->id}}/digestivos" class="btn btn-primary buttonsCat">Digestivos</a></p>

    <form class="" action="/funcionarios/{{$pedido->id}}/finalizar" method="POST">
      {{ csrf_field() }}
      <button class="btn btn-danger buttonEnd" type="submit" name="button">Finalizar Pedido</button>
    </form>
  </div>

@endsection
