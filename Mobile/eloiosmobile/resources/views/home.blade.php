@extends('principal.main')


@section('content')
<div class="container text-center" id="home">
  <a href="/funcionarios/mesas" class="btn btn-primary buttonStart">Criar Pedido</a>
  <a href="/funcionarios/pedidosabertos" class="btn btn-primary buttonStart">Mostrar pedidos abertos</a>
  <a href="/funcionarios/pedidoscozinha" class="btn btn-primary buttonStart">Mostrar pedidos cozinha</a>
  <a href="/funcionarios/pedidosfechados" class="btn btn-primary buttonStart">Mostrar pedidos fechados</a>
</div>
@endsection
