use master;
drop database eloios;
-- ***************
create database eloios;
use eloios;

--1 criar tabela tipo utilizadores

create table Tipo_User(
ID int identity(1,1) not null,
NOME varchar(20) not null,
constraint PK_TIPOUSER primary key (ID)
);

insert into Tipo_User(NOME) values ('ADMIN'),('CLIENTE'),('COZINHA'),('EMPREGADO');
select * from Tipo_User;
 
  
--2 criar tabela de utilizadores
create table Users(
ID int identity(1,1) not null,
NOME varchar(200) not null,
EMAIL varchar(100) not null,
PWD varchar(20) not null,
TELEMOVEL varchar(15) not null,
ESTADO int not null default 1,
TIPO int not null, 
constraint PK_USERS primary key (ID),
constraint FK_TIPOUSER foreign key (TIPO) references Tipo_User(ID)
);

insert into Users values ('ADMIN','ADMINISTRACAO@EMAIL.PT','ADMIN','911111',1,1)
insert into Users values ('MARCELO','MARCELO@MARCELO.PT','MARCELO','22222',1,2)

--LISTAR TODOS OS UTILIZADORES E O SEU TIPO DE USER
select Users.NOME,EMAIL,Tipo_User.NOME as TipoUtilizador from Users,Tipo_User where Users.TIPO=Tipo_User.ID

--3 criar tabela de RESERVA
create table Reserva(
IDUSER int not null,
NR_PESSOAS int not null,
DATAHORA datetime not null,
constraint PK_RESERVA primary key (IDUSER,DATAHORA),
constraint FK_RESERVAUSER foreign key (IDUSER) references Users(ID)
);

insert into Reserva values (2,10,GETDATE());

select Users.NOME, NR_PESSOAS, DATAHORA from Reserva,Users where Users.ID=Reserva.IDUSER

--4 criar tabela de MESAS
create table Mesa(
ID int identity(1,1) not null,
NOME varchar(5) not null,
ESTADO bit default 0,
constraint PK_MESA primary key (ID)
);

insert into Mesa(NOME) values ('1'),('2'),('3'),('4'),('5'),('6'),('7'),('8'),('9'),('10');

select * from Mesa;

--5 criar tabela de PEDIDOS
create table Pedido(
ID int identity(1,1) not null,
IDMESA int not null,
ESTADO bit default 1,
DATAHORA datetime not null,
IDUSER int not null,
constraint PK_PEDIDO primary key (ID),
constraint FK_PEDIDOUSER foreign key (IDUSER) references Users(ID),
constraint FK_PEDIDOMESA foreign key (IDMESA) references Mesa(ID)
);


--6 criar tabela PRODUCT
create table Producto(
ID int identity(1,1) not null,
NOME varchar(50) not null,
IMAGEM varchar(500),
MENU bit not null default 1,
constraint PK_PRODUCTO primary key (ID)
);

--7 criar tabela Linha_Pedido
create table Linha_Pedido(
IDPEDIDO int not null,
IDPRODUCTO int not null,
QUANTIDADE int not null,
DESCRICAO varchar(500),
constraint PK_LINHAPEDIDO primary key (IDPEDIDO,IDPRODUCTO),
constraint FK_LINHAPEDIDO foreign key (IDPEDIDO) references Pedido(ID),
constraint FK_LINHAPRODUTO foreign key (IDPRODUCTO) references Producto(ID),
);

--8 criar tabela TIPO_BEBIDA
create table Tipo_Bebida(
ID int identity(1,1) not null,
NOME varchar(50) not null,
constraint PK_TIPOBEBIDA primary key (ID)
);

insert into Tipo_Bebida values ('Sumos'),('Vinho'),('�guas')

--9 criar tabela BEBIDA
create table Bebida(
IDPRODUCTO int not null,
QUANTSTOCK int not null,
QUANTMIN int not null,
TIPOBEBIDA int not null,
constraint PK_BEBIDA primary key (IDPRODUCTO),
constraint FK_PRODUTOBEBIDA foreign key (IDPRODUCTO) references Producto(ID),
constraint FK_TIPOBEBIDA foreign key (TIPOBEBIDA) references Tipo_Bebida(ID),
);

insert into Producto(Nome,MENU) values ('Fanta',0)
insert into Bebida values (1,10,5,1)

--10 criar tabela ENTRADAS
create table Entradas(
IDPRODUCTO int not null,
NOME varchar(50) not null,
constraint PK_ENTRADAS primary key (IDPRODUCTO),
constraint FK_PRODUTOENTRADAS foreign key (IDPRODUCTO) references Producto(ID),
);

--11 criar tabela DIGESTIVOS
create table Digestivos(
IDPRODUCTO int not null,
NOME varchar(50) not null,
constraint PK_DIGESTIVOS primary key (IDPRODUCTO),
constraint FK_PRODUTODIGESTIVOS foreign key (IDPRODUCTO) references Producto(ID),
);

--11 criar tabela SOBREMESAS
create table Sobremesas(
IDPRODUCTO int not null,
NOME varchar(50) not null,
QUANTSTOCK int not null,
QUANTMIN int not null,
constraint PK_SOBREMESAS primary key (IDPRODUCTO),
constraint FK_PRODUTOSOBREMESAS foreign key (IDPRODUCTO) references Producto(ID),
);

--12 criar tabela TIPOPRATO
create table Tipo_Prato(
ID int identity(1,1) not null,
NOME varchar(50) not null,
constraint PK_TIPOPRATO primary key (ID)
);

--13 criar tabela PRATO
create table Prato(
IDPRODUCTO int not null,
NOME varchar(50) not null,
TIPOPRATO int not null,
constraint PK_PRATO primary key (IDPRODUCTO),
constraint FK_PRODUTOPRATO foreign key (IDPRODUCTO) references Producto(ID),
constraint FK_TIPOPRATO foreign key (TIPOPRATO) references Tipo_Prato(ID)
);

-- CRIAR TABELA DIVERSOS
create table Diversos(
IDPRODUCTO int not null,
QUANTSTOCK int not null,
QUANTMIN int not null,
constraint PK_DIVEROS primary key (IDPRODUCTO),
constraint FK_PRODUTODIVERSOS foreign key (IDPRODUCTO) references Producto(ID)
);

-- STORAGE PROCEDURES --
-- 1 Obter todos os Utilizadores
create procedure USP_ObterTodosOsUtilizadores
AS 
	select Users.NOME,EMAIL,Tipo_User.NOME as TipoUtilizador from Users,Tipo_User where Users.TIPO=Tipo_User.ID
go
exec USP_ObterTodosOsUtilizadores

--2 Obter UTILIZADOR atrav�s de email
create procedure USP_ObterTodosOsUtilizadoresByEmail @Email varchar(100)
AS 
select Users.NOME,EMAIL,Tipo_User.NOME as TipoUtilizador from Users,Tipo_User where Users.TIPO=Tipo_User.ID and Users.EMAIL = @Email
GO
exec USP_ObterTodosOsUtilizadoresByEmail 1

--3 INSERIR UTILIZADORES
-- drop procedure USP_InserirUtilizador

create procedure USP_InserirUtilizador @Nome varchar(200),@Email varchar(100),@PWD varchar(20),@Telemovel varchar(15),@TIPO int
	AS
		if((select count(ID) from Users where EMAIL = @Email) = 0)
		BEGIN
			IF(@Email LIKE '%@%.%' and @Email not like '%..%' and @Email not like '%@%@%')
			BEGIN
				insert into Users(NOME,EMAIL,PWD,TELEMOVEL,TIPO) values (@Nome,@Email,@PWD,@Telemovel,@TIPO);
				select ID from Users where Email=@Email;
			END
			else 
				return -1
		END 
		else 
			return -1
	GO
 
Declare @res int  
exec @res = USP_InserirUtilizador 'Ricardo Nogueira','ri1cardo11@ola11.pt','teste','912345678',2
print @res

select * from Users


--4 VERIFICAR USER A FAZER LOGIN
-- drop procedure USP_VerificarUtilizador

create procedure USP_VerificarUtilizador @Email varchar(100)
AS
	if exists (select ID from Users where EMAIL = @Email)
		BEGIN
			If ((select ESTADO from Users where EMAIL = @Email) = 1 ) 
				select ID,PWD from Users where EMAIL = @Email
			else
				return -2  -- retorno de -2 � a indicar que o utilizador est� banido
		END 
	else
		return -1 --retorno de -1 � a indicar que o utilizador n�o existe ou Palavra passe errada
GO

Declare @res int  
exec @res = USP_VerificarUtilizador 'ADMINISTRACAO@EMAIL.PT'
print @res

--5 BANIR UTILIZADOR 
create procedure USP_BanirUtilizador @Email VARCHAR(100)
AS
	UPDATE Users set ESTADO=0 where EMAIL= @Email
GO

--6 RETIRAR BAN UTILIZADOR 
create procedure USP_RetirarBan @Email VARCHAR(100)
AS
	UPDATE Users set ESTADO=1 where EMAIL= @Email
GO

--7 ABRIR PEDIDO PARA A MESA X 
create procedure USP_AbrirPedido @IDMesa int, @IDUser int 
AS
	insert into Pedido(IDMESA,DATAHORA,IDUSER) values (@IDMesa,GetDate(),@IDUser)
GO

USP_AbrirPedido 2,1
select * from Pedido

--8 ALTERAR ESTADO DO PEDIDO DA MESA 1 - POR ENTREGAR 0 - ENTREGUE NULL - ENCERRADO
create procedure USP_AlterarEstadoPedido @IDPedido int ,@Estado int
AS
	update Pedido set ESTADO = @Estado where ID = @IDPedido
GO

USP_AlterarEstadoPedido 1,0
select * from Pedido

--8.1 OBTER TODOS OS PEDIDOS 
create procedure USP_ObterTodosOsPedidos
AS
	select * from Pedido
GO

--8.2 OBTER TODOS OS PEDIDOS BY O SEU ESTADO 
create procedure USP_ObterTodosOsPedidosByEstado @Estado int
AS
	select * from Pedido where ESTADO = @Estado
GO
USP_ObterTodosOsPedidosByEstado NULL -- FECHADOS
USP_ObterTodosOsPedidosByEstado 1 --EM PROCESSAMENTO
USP_ObterTodosOsPedidosByEstado 0 --ENTREGUES NA MESA

--9 ADICIONAR LINHA DE PEDIDO AO PEDIDO drop procedure USP_AdicionarLinhaPedido
create procedure USP_AdicionarLinhaPedido @IDPedido int, @IDProducto  int , @Quant int, @DESCRICAO varchar(500) = ''
AS
	-- O PEDIDO EST� A 1- PROCESSAR OU 0 - ENTREGUE A MESA ? SEN�O RETORNA -1
	if((select ESTADO from Pedido where ID = @IDPedido) = 1 or (select ESTADO from Pedido where ID = @IDPedido) = 0 )
		BEGIN
				-- O PRODUTO � UM PRATO OU ENTRADA ? SE FOR TEMOS DE AVISAR A COZINHA
				If ((select count(*) from  Prato  where IDPRODUCTO =  @IDProducto) > 0 or (select count(*) from  Entradas  where IDPRODUCTO =  @IDProducto) > 0)
					BEGIN 
						insert into Linha_Pedido values (@IDPedido,@IDProducto,@Quant,@DESCRICAO)
						-- O PEDIDO ESTAR� A 0 - ENTREGUE? SE TIVER VAMOS AQUI AVISAR A COZINHA ! 
						if((select ESTADO from Pedido where ID = @IDPedido) = 0)
							update Pedido set ESTADO=1 where ID = @IDPedido
					END 
				-- N�O � UM PRATO OU ENTRADA SE FOR UMA BEBIDA OU SOBREMESA VAMOS TER DE VERIFICAR O STOCK E RETIRAR O QUE USARMOS 
				-- SE FOR BEBIDA
				if((select count(*) from BEBIDA where IDPRODUCTO = @IDProducto) = 1)
					BEGIN
						--EXISTE A MESMA QUANTIDADE OU MAIS DA BEBIDA QUE EU PE�O ?
						if((select QUANTSTOCK from Bebida where IDPRODUCTO = @IDProducto) >= @Quant)
							BEGIN
								insert into Linha_Pedido values (@IDPedido,@IDProducto,@Quant,@DESCRICAO)
								update Bebida set QUANTSTOCK = QUANTSTOCK - @Quant where IDPRODUCTO = @IDProducto
							END
						else return -2 -- RETORNA -2 SE A QUANTIDADE N�O FOR SUFICIENTE ! 
					END
				-- SE FOR SOBREMESA
				if((select count(*) from Sobremesas where IDPRODUCTO = @IDProducto) = 1)
					BEGIN
						--EXISTE A MESMA QUANTIDADE OU MAIS DA SOBREMESA QUE EU PE�O ?
						if((select QUANTSTOCK from Sobremesas where IDPRODUCTO = @IDProducto) >= @Quant)
							BEGIN
								insert into Linha_Pedido values (@IDPedido,@IDProducto,@Quant,@DESCRICAO)
								update Sobremesas set QUANTSTOCK = QUANTSTOCK - @Quant where IDPRODUCTO = @IDProducto
							END
						else return -2 -- RETORNA -2 SE A QUANTIDADE N�O FOR SUFICIENTE ! 
					END
				if((select count(*) from Digestivos where IDPRODUCTO = @IDProducto) =1) -- DIGESTIVOS
					BEGIN
						insert into Linha_Pedido values (@IDPedido,@IDProducto,@Quant,@DESCRICAO)
						return 0
					END
		END
	else 
		return -1 --PEDIDO J� EST� ENCERRADO N�O PODE ADICIONAR COISAS
GO


Declare @res int  
exec @res = USP_AdicionarLinhaPedido 1,5,1
print @res

select * from Linha_Pedido
select M.Nome as Mesa,P.NOME,QUANTIDADE from Linha_Pedido L,Producto P,Mesa M,Pedido PE where L.IDPRODUCTO=P.ID and L.IDPEDIDO=PE.ID and PE.IDMESA=M.ID

-- EDITAR LINHA DE PEDIDO drop procedure USP_AlterarQuantLinhaPedido
create procedure USP_AlterarQuantLinhaPedido @IDPedido int, @IDProducto int, @Quant int
AS
	
		IF EXISTS (select * from Pedido where ID = @IDPedido)
		BEGIN
				-- O PEDIDO EST� A 1- PROCESSAR OU 0 - ENTREGUE A MESA ? SEN�O RETORNA -1
			if((select ESTADO from Pedido where ID = @IDPedido) = 1 or (select ESTADO from Pedido where ID = @IDPedido) = 0 )
				BEGIN
						-- O PRODUTO � UM PRATO OU ENTRADA ? SE FOR TEMOS DE AVISAR A COZINHA
						If ((select count(*) from  Prato  where IDPRODUCTO =  @IDProducto) > 0 or (select count(*) from  Entradas  where IDPRODUCTO =  @IDProducto) > 0)
							BEGIN 
								update Linha_Pedido set QUANTIDADE = @Quant where IDPEDIDO = @IDPedido and IDPRODUCTO=@IDProducto;
								-- O PEDIDO ESTAR� A 0 - ENTREGUE? SE TIVER VAMOS AQUI AVISAR A COZINHA ! 
								if((select ESTADO from Pedido where ID = @IDPedido) = 0)
									update Pedido set ESTADO=1 where ID = @IDPedido;
								return 0;-- retorna sucesso
							END 
						-- N�O � UM PRATO OU ENTRADA SE FOR UMA BEBIDA OU SOBREMESA VAMOS TER DE VERIFICAR O STOCK E RETIRAR O QUE USARMOS 
						-- SE FOR BEBIDA
						if((select count(*) from BEBIDA where IDPRODUCTO = @IDProducto) = 1)
							BEGIN
								--EXISTE A MESMA QUANTIDADE OU MAIS DA BEBIDA QUE EU PE�O ?
								if((select QUANTSTOCK from Bebida where IDPRODUCTO = @IDProducto) >= @Quant)
									BEGIN
										declare @ant int;
										set @ant = (select QUANTIDADE from Linha_Pedido where IDPEDIDO = @IDPedido and IDPRODUCTO=@IDProducto);
										update Bebida set QUANTSTOCK = QUANTSTOCK + @ant where IDPRODUCTO = @IDProducto;
										update Linha_Pedido set QUANTIDADE = @Quant where IDPEDIDO = @IDPedido and IDPRODUCTO=@IDProducto;
										update Bebida set QUANTSTOCK = QUANTSTOCK - @Quant where IDPRODUCTO = @IDProducto;
										return 0; -- retorna sucesso
									END
								else return -2; -- RETORNA -2 SE A QUANTIDADE N�O FOR SUFICIENTE ! 
							END
						-- SE FOR SOBREMESA
						if((select count(*) from Sobremesas where IDPRODUCTO = @IDProducto) = 1)
							BEGIN
								--EXISTE A MESMA QUANTIDADE OU MAIS DA SOBREMESA QUE EU PE�O ?
								if((select QUANTSTOCK from Sobremesas where IDPRODUCTO = @IDProducto) >= @Quant)
									BEGIN
										declare @ant1 int;
										set @ant1 = (select QUANTIDADE from Linha_Pedido where IDPEDIDO = @IDPedido and IDPRODUCTO=@IDProducto);
										update Sobremesas set QUANTSTOCK = QUANTSTOCK + @ant1 where IDPRODUCTO = @IDProducto;
										update Linha_Pedido set QUANTIDADE = @Quant where IDPEDIDO = @IDPedido and IDPRODUCTO=@IDProducto;
										update Sobremesas set QUANTSTOCK = QUANTSTOCK - @Quant where IDPRODUCTO = @IDProducto;
										return 0; -- retorna sucesso
									END
								else return -2; -- RETORNA -2 SE A QUANTIDADE N�O FOR SUFICIENTE ! 
							END
						if((select count(*) from Digestivos where IDPRODUCTO = @IDProducto) =1) -- DIGESTIVOS
									BEGIN
										update Linha_Pedido set QUANTIDADE = @Quant where IDPEDIDO = @IDPedido and IDPRODUCTO=@IDProducto;
										return 0
									END
				END
			else 
				return -1; --PEDIDO J� EST� ENCERRADO N�O PODE ADICIONAR COISAS!
		END 
		ELSE 
			return -1; -- N�O EXISTE
GO

select * from Linha_Pedido

declare @res int
exec @res = USP_AlterarQuantLinhaPedido 1,5,2
print @res

-- REMOVER LINHA DE PEDIDO 
create procedure USP_RemoverLinhaPedido @IDPEDIDO int, @IDProduto int
AS
	if((select count(*) from Linha_Pedido where IDPEDIDO=@IDPEDIDO and IDPRODUCTO=@IDProduto)=1)
		BEGIN 
			delete from Linha_Pedido where IDPEDIDO=@IDPEDIDO and IDPRODUCTO=@IDProduto;
			return 0;
		END
	else 
		return -1 -- LINHA N�O EXISTE
GO
USP_RemoverLinhaPedido 1,1

select * from Linha_Pedido

select * from Bebida

-- DETALHE DE TODAS AS LINHAS DE PEDIDO DO PEDIDO @IDPEDIDO
create procedure USP_ObterTodasAsLinhasDoPedido @IDPedido int
AS
	
GO

select P.Nome,L.QUANTIDADE,L.DESCRICAO from Linha_Pedido L ,Producto P where L.IDPRODUCTO=P.ID and IDPEDIDO = 1

--9.1 OBTER TODOS OS PRODUTOS
create procedure USP_ObterTodosOsProdutos
AS
select * from Producto
GO

--9 OBTER TODOS OS PRODUTOS EM MENU
create procedure USP_ObterTodosOsProdutosEmMenu
AS
select ID,NOME,IMAGEM from Producto where MENU like 1
GO

--9.1.1 OBTER PRODUTOS SOBREMESAS
create procedure USP_ObterProdutosSobremesa
AS
	select P.ID,P.Nome,P.IMAGEM,s.QUANTSTOCK,s.QUANTMIN from Producto P,Sobremesas S where P.ID=S.IDPRODUCTO and QUANTSTOCK > 0 --! MOSTRAR ALERTA DE POUCA QTD
GO

--9.1.2 OBTER PRODUTOS DIGESTIVOS
create procedure USP_ObterProdutosDigestivo
AS
	select P.ID,P.Nome,P.IMAGEM	 FROM Producto P, Digestivos D where P.ID=D.IDPRODUCTO
GO

-- ALTERAR QTD SOBREMESA 
create procedure USP_AlterarQuantSobremesa @IDProduto int , @Quant int
AS 
	UPDATE Sobremesas set QUANTSTOCK =  @Quant where IDPRODUCTO = @IDProduto
GO

-- ALTERAR QTD BEBIDAS 
create procedure USP_AlterarQuantBebida @IDProduto int , @Quant int
AS 
	UPDATE Bebida set QUANTSTOCK =  @Quant where IDPRODUCTO = @IDProduto
GO

USP_AlterarQuantBebida 1,10

--9.1.3 OBTER  TIPOS BEBIDA
create procedure USP_ObterTiposBebida
AS
 select * from Tipo_Bebida
GO

USP_ObterTiposBebida

--9.1.4 OBTER BEBIDAS BY TIPO BEBIDA
create procedure USP_ObterBebidasByTipoBebida @IDTipoBebida int
AS
	select P.ID, P.Nome,P.IMAGEM,B.QUANTSTOCK,B.QUANTMIN from Producto P, Bebida B, Tipo_Bebida TB where P.ID=B.IDPRODUCTO and TB.ID=B.TIPOBEBIDA and TB.ID=@IDTipoBebida
GO

USP_ObterBebidasByTipoBebida 1

--9.1.5 OBTER TODAS AS ENTRADAS
create procedure USP_ObterProdutosEntradas
AS
	select P.ID,P.Nome,P.Imagem from Producto P, Entradas E where P.ID=E.IDPRODUCTO
GO

--9.1.6 OBTER TIPOS DE PRATOS
create procedure USP_ObterTiposPrato
AS
	select * from Tipo_Prato
GO

--9.1.7 OBTER TODOS OS PRATOS BY TIPO PRATO
create procedure USP_ObterProdutoPratoByTipoPrato @IDTipoPrato int
AS
	select P.ID, P.Nome,P.IMAGEM from Producto P, Prato PR, Tipo_Prato TP where P.ID=PR.IDPRODUCTO and TP.ID=PR.TIPOPRATO and TP.ID=@IDTipoPrato
GO

USP_ObterProdutoPratoByTipoPrato 1


--10 INSERIR PRODUTO SOBREMESAS
create procedure USP_InserirProdutoSobremesas @NOME varchar(50),@IMAGEM varchar(500),@QUANTSTOCK int, @QUANTMIN int
AS
	insert into Producto(NOME,IMAGEM) values (@NOME,@IMAGEM)
	Declare @ID int;
	set @ID =  SCOPE_IDENTITY();
	insert into  Sobremesas values (@ID,@NOME,@QUANTSTOCK,@QUANTMIN);

GO
USP_InserirProdutoSobremesas 'Natas',NULL,10,5  
select * from Sobremesas
select * from Producto

--11 INSERIR PRODUTO DIGESTIVOS
create procedure USP_InserirProdutoDigestivos @Nome varchar(50),@Imagem varchar(500)
AS
	insert into Producto(NOME,IMAGEM) values (@NOME,@IMAGEM)
	Declare @ID int;
	set @ID =  SCOPE_IDENTITY();
	insert into  Digestivos values (@ID,@NOME);
GO
USP_InserirProdutoDigestivos 'Xiripiti',null
select * from Digestivos

--12 INSERIR PRODUTO ENTRADAS
create procedure USP_InserirProdutoEntradas @Nome varchar(50),@Imagem varchar(500)
AS
	insert into Producto(NOME,IMAGEM) values (@NOME,@IMAGEM)
	Declare @ID int;
	set @ID =  SCOPE_IDENTITY();
	insert into Entradas values (@ID,@NOME);
GO
USP_InserirProdutoEntradas 'Riss�is',null
select * from Entradas

--13 INSERIR TIPOS DE PRATOS
create procedure USP_InserirTiposDePrato @Nome varchar(50)
AS
	insert into Tipo_Prato(NOME) values (@Nome)
GO
USP_InserirTiposDePrato 'Leit�o'

--14 INSERIR PRODUTO PRATO CONSOANTE O TIPO DE PRATO drop procedure USP_InserirProdutoPrato
create procedure USP_InserirProdutoPrato @Nome varchar(50),@Imagem varchar(500),@IDTipoPrato int
AS
	insert into Producto(NOME,IMAGEM) values (@Nome,@Imagem)
	Declare @ID int;
	set @ID = SCOPE_IDENTITY();
	insert into Prato values (@ID,@Nome,@IDTipoPrato)
GO
select * from Tipo_Prato
USP_InserirProdutoPrato SANDE,'',1
select * from Prato
select * from Producto



--14 INSERIR TIPOS DE BEBIDA
create procedure USP_InserirTiposDeBebida @Nome varchar(50)
AS
	insert into Tipo_Bebida(NOME) values (@Nome)
GO

--15 INSERIR PRODUTO BEBIDA CONSOANTE O TIPO DE BEBIDA
create procedure USP_InserirProdutoBebida @Nome varchar(50),@Imagem varchar(500),@QuantStock int,@QuantMin int, @IDTipoBebida int
AS
	insert into Producto(NOME,IMAGEM) values (@Nome,@Imagem)
	Declare @ID int;
	set @ID = SCOPE_IDENTITY();
	insert into Bebida values (@ID,@QuantStock,@QuantMin,@IDTipoBebida)
Go

USP_InserirProdutoBebida 'Coca-Cola',NULL,10,5,1


--16 INSERIR PRODUTO DIVERSOS
create procedure USP_InserirProdutoDiversos @Nome varchar(50),@Imagem varchar(500),@QuantStock int,@QuantMin int
AS
	insert into Producto(NOME,IMAGEM,MENU) values (@Nome,@Imagem,0)
	Declare @ID int;
	set @ID = SCOPE_IDENTITY();
	insert into Diversos values (@ID,@QuantStock,@QuantMin);
GO

USP_InserirProdutoDiversos '�leo',NULL,10,10

select ID,NOME,QUANTSTOCK,QUANTMIN from Producto,Diversos where Producto.ID=Diversos.IDPRODUCTO

--17 ALTERAR PRODUTO DIVERSOS
create procedure USP_AlterarProdutoDiversos @IDProduto int, @Quant int
AS
	update Diversos set QUANTSTOCK = @Quant where IDPRODUCTO = @IDProduto
GO

USP_AlterarProdutoDiversos 2,5
select * from Diversos