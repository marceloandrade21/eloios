<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $sucesso=0;
    return view('login',compact('sucesso'));
});
Route::post('/cozinha/login','RequestController@login');
Route::post('/cozinha/finaliza/{id}','RequestController@fim');
Route::resource('/cozinha','RequestController');
