<?php

use Illuminate\Database\Seeder;
use App\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('utypes')->insert(['name'=>'admin']);
        DB::table('utypes')->insert(['name'=>'normal']);
        DB::table('utypes')->insert(['name'=>'funcionario']);
        DB::table('utypes')->insert(['name'=>'cozinha']);
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@admin.pt',
            'password' => bcrypt('123456a'),
            'status'=>1,
            'utype_id'=>1,
            'phone'=>919119,
        ]);
        DB::table('users')->insert([
            'name' => 'ricardo',
            'email' => 'ricardo@gmail.pt',
            'password' => bcrypt('123qwe'),
            'status'=>1,
            'utype_id'=>3,
            'phone'=>919119,
        ]);

        /* MESAS */

        DB::table('tables')->insert(['name' => '1','status'=>1,]);
        DB::table('tables')->insert(['name' => '2','status'=>1,]);
        DB::table('tables')->insert(['name' => '3','status'=>1,]);
        DB::table('tables')->insert(['name' => '4','status'=>1,]);
        DB::table('tables')->insert(['name' => '5','status'=>1,]);
        DB::table('tables')->insert(['name' => '6','status'=>1,]);
        DB::table('tables')->insert(['name' => '7','status'=>1,]);
        DB::table('tables')->insert(['name' => '8','status'=>1,]);
        DB::table('tables')->insert(['name' => '9','status'=>1,]);

        /* TIPOS DE BEBIDAS*/

        DB::table('drink_types')->insert(['name' => 'Vinhos',]);
        DB::table('drink_types')->insert(['name' => 'Água',]);
        DB::table('drink_types')->insert(['name' => 'Sumos',]);

        /* TIPOS DE PRATOS */
        DB::table('dish_types')->insert(['name'=>'Leitão']);
        DB::table('dish_types')->insert(['name'=>'Português']);

        /* TABELA "MÃE" DE PRODUTOS */
        // BEBIDAS
        DB::table('products')->insert(['name' => 'Vinho Branco','img'=>'','menu'=>1]);//1
        DB::table('products')->insert(['name' => 'Vitalis','img'=>'','menu'=>1]);//2
        //ENTRADAS
        DB::table('products')->insert(['name' => 'Rissois','img'=>'','menu'=>1]);//3
        //SOBREMESAS
        DB::table('products')->insert(['name' => 'Bolo Bolacha','img'=>'','menu'=>1]);//4
        DB::table('products')->insert(['name' => 'Leite Creme','img'=>'','menu'=>1]);//5
        DB::table('products')->insert(['name' => 'Fruta','img'=>'','menu'=>1]);//6
        DB::table('products')->insert(['name' => 'Molotof','img'=>'','menu'=>1]);//7
        //DIGESTIVOS
        DB::table('products')->insert(['name' => 'Bagaço','img'=>'','menu'=>1]);//8
        DB::table('products')->insert(['name' => 'Café','img'=>'','menu'=>1]);//9
        DB::table('products')->insert(['name' => 'Wiskhy','img'=>'','menu'=>1]);//10
        //PRATOS
        DB::table('products')->insert(['name' => 'Sande Leitão','img'=>'','menu'=>1]);//11
        DB::table('products')->insert(['name' => 'Feijoada','img'=>'','menu'=>1]);//12
        //DIVERSOS
        DB::table('products')->insert(['name' => 'Diversos 1', 'img' => '', 'menu'=>1]);//13
        DB::table('products')->insert(['name' => 'Diversos 2', 'img' => '', 'menu'=>1]);//14
        DB::table('products')->insert(['name' => 'Diversos 3', 'img' => '', 'menu'=>1]);//15



        /* TABELA ESPECÍFICA DOS PRODUTOS */

        DB::table('appetizers')->insert(['product_id'=>3,'name'=>'Rissois']);
        DB::table('drinks')->insert(['product_id'=>1,'name'=>'Vinho Branco','type_drink_id'=>1,'quantityavailable'=>10,'quantitymin'=>2]);
        DB::table('drinks')->insert(['product_id'=>2,'name'=>'Vitalis','type_drink_id'=>2,'quantityavailable'=>10,'quantitymin'=>2]);
        DB::table('desserts')->insert(['product_id'=>4,'name'=>'Bolo Bolacha','quantityavailable'=>10,'quantitymin'=>5]);
        DB::table('desserts')->insert(['product_id'=>5,'name'=>'Leite Creme','quantityavailable'=>5,'quantitymin'=>5]);
        DB::table('desserts')->insert(['product_id'=>6,'name'=>'Fruta','quantityavailable'=>2,'quantitymin'=>5]);
        DB::table('desserts')->insert(['product_id'=>7,'name'=>'Molotof','quantityavailable'=>10,'quantitymin'=>5]);
        DB::table('digestives')->insert(['product_id'=>8,'name'=>'Bagaço']);
        DB::table('digestives')->insert(['product_id'=>9,'name'=>'Café']);
        DB::table('digestives')->insert(['product_id'=>10,'name'=>'Wiskhy']);
        DB::table('dishes')->insert(['product_id'=>11,'name'=>'Sande Leitão','type_dish_id'=>1]);
        DB::table('dishes')->insert(['product_id'=>12,'name'=>'Feijoada','type_dish_id'=>2]);
        DB::table('various')->insert(['product_id'=>13, 'name' => 'Diversos 1', 'quantityavailable'=>10,'quantitymin'=>5]);
        DB::table('various')->insert(['product_id'=>14, 'name' => 'Diversos 2', 'quantityavailable'=>5,'quantitymin'=>5]);
        DB::table('various')->insert(['product_id'=>15, 'name' => 'Diversos 3', 'quantityavailable'=>2,'quantitymin'=>5]);



        DB::table('requests')->insert(['user_id'=>1,'table_id'=>1,'status'=>0,'datehour'=>now()]);
        DB::table('product_request')->insert(['request_id'=>1,'product_id'=>1,'description'=>"Fresco",'quantity'=>"1"]);

    }
}
