<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_Request extends Model
{
    protected $table="product_request";

    public function request(){
        return $this->belongsTo(Request::Class);
    }
    public function product(){
        return $this->belongsTo(Product::Class);
    }
}
