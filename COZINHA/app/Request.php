<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    public function user(){
        return $this->belongsTo(User::Class);
    }
    public function table(){
        return $this->belongsTo(Table::Class);
    }
    public function product_request(){
        return $this->hasMany(Product_Request::Class);
    }

}
