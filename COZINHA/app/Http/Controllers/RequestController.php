<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Request as Pedido;
use App\Product_Request;
use App\Dessert;
use App\Appetizer;
use App\Dish;

class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requests = Pedido::where('status','=',0)->get();
        $product_requests = Product_Request::all();
        $sucesso=0;
        return view('cozinha',compact('requests','product_requests','sucesso'));

    }
    /* FINALIZA O PEDIDO , ESTE DESAPARECE DA COZINHA MAS PODEM APARECER NOVAMENTE
    SE ADICIONAREM MAIS LINHAS AO PEDIDO */

    public function fim($id){
        $request = Pedido::find($id);
        $requestLines = Product_Request::where('request_id','=',$id)->get();
        foreach($requestLines as $requestLine){
            $requestLine->status=1;
            $requestLine->save();
        }
        $request->status = 1;
        $request->save();
        $requests = Pedido::where('status','=',0)->get();
        $product_requests = Product_Request::all();
        $sucesso =1;
        return view('cozinha',compact('requests','product_requests','sucesso'));
        
    }
    public function login(Request $request){
        $pin = $request->get('pin');
        if($pin == 1234){
            $requests = Pedido::where('status','=',0)->get();
            $product_requests = Product_Request::all();
            $sucesso=0;
            return redirect('/cozinha');
        }else {
            $sucesso=-1;
            return view('login',compact('sucesso'));

        }
    }
}
