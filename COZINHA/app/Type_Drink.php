<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type_Drink extends Model
{
    protected $table = "drink_types";
    public function drinks()
    {
        return $this->hasMany(Drink::class);
    }
}
