<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drink extends Model
{
    public function type_drink()
    {
        return $this->belongsTo(Type_Drink::class);
    }
}
