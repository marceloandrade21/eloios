<!DOCTYPE html>
<html lang="en">
<head>
  <title>ELOI'OS COZINHA</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="icon" href="/icon.ico">
  <link rel="stylesheet" href="/css/custom.css">
  @yield('head')
  

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 
</head>
<body>
  
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 text-center">
            <img src="/logo.png" alt="logotipo">
        </div>
    </div>

    @yield('content')
</div>

</body>
</html>
