@extends('main')

@section('head')

@endsection
@section('content')
	

    <div class="row">
    <div class="col-sm-3">
    </div>
        <div class="col-sm-6">
            @if($sucesso==1)
                    <div class="alert alert-success alert-dismissable" id="success-alert" style="margin-top:30px">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong>Sucesso!</strong> Pedido Entregue.
                    </div>
            @endif
        </div>
    <div class="col-sm-3">
    </div>
    </div>
    <div class="row">
    @foreach($requests as $request)
        
        <div class=" col-sm-4 pedido text-center" onclick="selecionado(this)" id="pedido{{$request->id}}">
                <div class="frente">
                     <p><b>Pedido Nr: {{ $request->id}}</b> Mesa: {{$request->table->name}}</p>
                    <div class="strike">
                        <span>Produtos</span>
                    </div>
                    <b>
                    @foreach($product_requests as $line)
                            @if($line->request_id == $request->id)
                                @if($line->status==0)
                                <p> {{$line->product->name}} - Qtd : {{ $line->quantity}}</p>
                                @else
                                 <strike><p> {{$line->product->name}} - Qtd : {{ $line->quantity}}</p></strike>
                                @endif
                            @endif
                    @endforeach
                    </b>
                </div>
                <div class="tras hidden">
                    <div >
                        <div>
                         <p><b>Pedido Nr: {{ $request->id}}</b> Mesa: {{$request->table->name}}</p>
                        <form method="post" action="/cozinha/finaliza/{{$request->id}}"> {{csrf_field()}}
                             <input type="hidden" value="{{$request->id}}"/>
                             <input type="submit" class="btn btn-success" value="Finalizar"/>
                        </form>
                        </div>
                        <div >
                            <button type="button" onclick="voltar()" class="btn btn-info">Voltar</button>
                        </div>
                    </div>
                </div>
        </div>
    @endforeach
</div>

<script>

$("#success-alert").fadeTo(1000, 500).slideUp(500, function(){
    $("#success-alert").alert('close');
     window.location.replace("/cozinha");
});
    function alter(){
       
    }
   function selecionado(element){
            $(element).find(".frente").addClass("hidden");
            $(element).find(".tras").removeClass("hidden");
   }
   function voltar(){
       window.location.replace("/cozinha");
   }
   setTimeout(function(){
    window.location.reload(1);}, 15000);
</script>
@endsection
