@extends('main')

@section('head')
<link rel="stylesheet" href="/css/login.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 phone">
            <div class="row1 text-center">
            @if($sucesso ==-1)
                <b style="color:red"> * PIN INCORRETO  </b>
            @endif
                <div class="col-md-12">
                <form method="post" action="/cozinha/login" id="login"> {{csrf_field()}}
                    <input type="password" name="pin" id="pin" class="form-control tel" value="" inputmode="numeric" minlength="4" maxlength="4" size="4" readonly/>
                </form>
                    <div class="num-pad">
                    <div class="span4">
                        <div class="num">
                            <div class="txt">
                                1
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="num">
                            <div class="txt">
                                2 
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="num">
                            <div class="txt">
                                3
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="num">
                            <div class="txt">
                                4 
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="num">
                            <div class="txt">
                                5 
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="num">
                            <div class="txt">
                                6 
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="num">
                            <div class="txt">
                                7 
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="num">
                            <div class="txt">
                                8 
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="num">
                            <div class="txt">
                                9 
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                    </div>
                    <div class="span4">
                        <div class="num">
                            <div class="txt">
                                0 
                            </div>
                        </div>
                    </div>
                     <div class="span4">
                        <div class="num" id="back">
                                <div class="txt">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                </div>
                        </div>
                    </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function () {

    $('#back').click(function(){
         var pin = $('#pin');
         $(pin).val("");
    });

    $('.num').click(function () {
        var num = $(this);
        var text = $.trim(num.find('.txt').clone().children().remove().end().text());
        var pin = $('#pin');
        
        if(pin.val().length<=2){
             $(pin).val(pin.val() + text);
        }else{
             $(pin).val(pin.val() + text);
            $( "#login" ).submit();
        }
       
    });

});
</script>
@endsection